package de.culture4life.luca.poeditor.network

import com.google.gson.GsonBuilder
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

object PoEditorApiFactory {

    private const val urlPoEditorApi = "https://api.poeditor.com/v2/"

    fun create(logger: (message: String) -> Unit): PoEditorApi {
        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor { message -> logger(message) }
                .setLevel(HttpLoggingInterceptor.Level.BASIC))
            .build()

        val gsonFactory = GsonConverterFactory.create(
            GsonBuilder().create()
        )

        val retrofit = Retrofit.Builder()
            .baseUrl(urlPoEditorApi.toHttpUrl())
            .client(okHttpClient)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(gsonFactory)
            .build()

        return retrofit.create(PoEditorApi::class.java)
    }
}