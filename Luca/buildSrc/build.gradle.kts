plugins {
    `kotlin-dsl`
}

gradlePlugin {
    plugins {
        create("luca-poeditor") {
            id = "de.cultur4life.luca.poeditor"
            implementationClass = "de.culture4life.luca.poeditor.PoEditorPlugin"
        }
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.squareup.retrofit2:converter-gson:2.9.0")
    implementation("com.squareup.retrofit2:converter-scalars:2.1.0")
    implementation("com.squareup.retrofit2:retrofit:2.9.0")

    implementation("com.squareup.okhttp3:logging-interceptor:4.9.3")
    implementation("com.squareup.okhttp3:okhttp:4.9.3")

    testImplementation("junit:junit:4.13.2")
}