const axios = require('axios')
const fs = require('fs')
const outputFile = "./stale_branches.txt"
const gitlabAccessToken = process.env.LUCA_GITLAB_ACCESS_TOKEN
const gitlabProjectId = "35039811"

async function deleteBranch(branchName) {
    console.log(`Deleting branch ${branchName}`)
    const encodedName = encodeURIComponent(branchName)
    await axios.delete(`https://gitlab.com/api/v4/projects/${gitlabProjectId}/repository/branches/${encodedName}?private_token=${gitlabAccessToken}`)
}

async function deleteAllStaleBranches(staleBranches) {
    if (staleBranches.length == 0) return
    const line = staleBranches.shift()
    const branchName = line.split('Name: ')[1]
    await deleteBranch(branchName)
    return deleteAllStaleBranches(staleBranches)
}

async function main() {
    const staleBranchesContent = fs.readFileSync(outputFile, 'utf-8');
    const lines = staleBranchesContent.split("\n")
    await deleteAllStaleBranches(lines)
}

(() => {
    console.log("Starting to delete stale branches...")
    main()
        .then(() => console.log("Finished deleting stale branches!"))
        .catch((error) => console.log(error))

})()
