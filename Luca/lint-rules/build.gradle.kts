plugins {
    id("java-library")
    id("org.jetbrains.kotlin.jvm")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.7.0")

    val lintVersion = "30.2.0-beta04"

    // Should be major android gradle plugin version + 23 (so for example 7.2.0 would be 30.2.0)
    // Documentation: http://googlesamples.github.io/android-custom-lint-rules/api-guide.md#example:samplelintcheckgithubproject/lintversion?
    compileOnly("com.android.tools.lint:lint-api:$lintVersion")
    compileOnly("com.android.tools.lint:lint-checks:$lintVersion")

    testImplementation("junit:junit:4.13.2")
    testImplementation("com.android.tools.lint:lint:$lintVersion")
    testImplementation("com.android.tools.lint:lint-tests:$lintVersion")
}
