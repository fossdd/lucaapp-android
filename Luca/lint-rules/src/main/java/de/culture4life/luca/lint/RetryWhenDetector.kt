package de.culture4life.luca.lint

import com.android.tools.lint.detector.api.*
import com.intellij.psi.PsiMethod
import org.jetbrains.uast.UCallExpression

class RetryWhenDetector : Detector(), SourceCodeScanner {

    override fun getApplicableMethodNames() = listOf("retryWhen")

    override fun visitMethodCall(context: JavaContext, node: UCallExpression, method: PsiMethod) {
        context.report(
            ISSUE,
            context.getCallLocation(node, includeReceiver = true, includeArguments = true),
            "Direct usage of `retryWhen`"
        )
    }

    companion object Issues {
        val ISSUE = Issue.create(
            id = "RetryWhen",
            briefDescription = "Direct usage of `retryWhen`",
            explanation = """
                `retryWhen` should not be used directly as it does not have a max. retry amount
                Please use one of the following Util classes to get this functionality: 
                `CompletableUtil`, `SingleUtil`, `ObservableUtil`
            """.trimIndent(),
            category = Category.CORRECTNESS,
            severity = Severity.INFORMATIONAL,
            androidSpecific = true,
            implementation = Implementation(
                RetryWhenDetector::class.java,
                Scope.JAVA_FILE_SCOPE
            )
        )
    }

}
