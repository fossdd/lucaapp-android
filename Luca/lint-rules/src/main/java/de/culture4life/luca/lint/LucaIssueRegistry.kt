package de.culture4life.luca.lint

import com.android.tools.lint.client.api.IssueRegistry
import com.android.tools.lint.client.api.Vendor
import com.android.tools.lint.detector.api.CURRENT_API
import com.android.tools.lint.detector.api.Issue

class LucaIssueRegistry : IssueRegistry() {
    override val issues: List<Issue> = listOf(RetryWhenDetector.ISSUE)

    override val api: Int
        get() = CURRENT_API

    override val vendor: Vendor = Vendor("Luca")
}
