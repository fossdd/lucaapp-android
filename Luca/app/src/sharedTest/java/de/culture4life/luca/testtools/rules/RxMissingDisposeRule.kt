package de.culture4life.luca.testtools.rules

import de.culture4life.luca.testtools.disposiblewatcher.RxDisposableWatcher

class RxMissingDisposeRule : BaseHookingTestRule() {

    // Use it to get a clue where the disposable was leak. Use "0" for don't stop.
    //  See test error which disposable is leaked.
    private val stopAtDisposableCounter = 0

    private val isEnabled = true
    private val watcher = RxDisposableWatcher()

    override fun beforeTest() {
        if (isEnabled) {
            watcher.init(stopAtDisposableCounter)
        }
    }

    override fun afterTest() {
        if (isEnabled) {
            // Throw if any leak detected.
            val disposableCounters = watcher.probe().map { it.disposableCounter }.sorted().joinToString()
            if (disposableCounters.isNotBlank()) {
                val message = "leak detected at disposable counter(s) '$disposableCounters', follow stacktrace to use the counter value"
                throw IllegalStateException(message).also { exception ->
                    exception.stackTrace = arrayOf(
                        // point where this exception is thrown
                        exception.stackTrace[0],
                        // point where you can use the disposable counter to find source of leak
                        StackTraceElement(javaClass.name, "stopAtDisposableCounter", "${javaClass.name}.kt", 9)
                    )
                }
            }
        }
    }
}
