package de.culture4life.luca.testtools.samples

import de.culture4life.luca.network.pojo.HealthDepartment

class HealthDepartmentSamples {

    companion object {
        fun healthDepartment(zipCode: String = CovidDocumentSamples.ErikaMustermann().registrationData().postalCode!!): HealthDepartment {
            val keyIssuerResponseData = DailyKeySamples.WithoutExpiration().keyIssuerResponseData
            return HealthDepartment().apply {
                id = keyIssuerResponseData.id
                name = "Gesundheitsamt Dev"
                zipCodes = listOf(zipCode)
                encryptionKeyJwt = keyIssuerResponseData.encryptionKeyJwt
                signingKeyJwt = keyIssuerResponseData.signingKeyJwt
            }
        }

        fun healthDepartmentAsList(
            zipCode: String = CovidDocumentSamples.ErikaMustermann().registrationData().postalCode!!
        ): List<HealthDepartment> {
            return listOf(healthDepartment(zipCode))
        }
    }
}
