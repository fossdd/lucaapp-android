package de.culture4life.luca.testtools.preconditions

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import io.reactivex.rxjava3.core.Single

class ContactTracingPreconditions {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()
    private val checkInManager by lazy { application.getInitializedManager(application.checkInManager).blockingGet() }

    fun givenContactTracingEnabled(enabled: Boolean = true) {
        checkInManager.setCachedTracingPossible(Single.just(enabled))
    }
}
