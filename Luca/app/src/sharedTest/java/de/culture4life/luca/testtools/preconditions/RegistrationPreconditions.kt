package de.culture4life.luca.testtools.preconditions

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.registration.RegistrationData
import de.culture4life.luca.registration.RegistrationManager
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import io.reactivex.rxjava3.core.Completable

class RegistrationPreconditions {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()
    private val preferencesManager by lazy { application.getInitializedManager(application.preferencesManager).blockingGet() }
    private val registrationManager by lazy { application.getInitializedManager(application.registrationManager).blockingGet() }

    fun givenRegisteredUser(person: CovidDocumentSamples.Person = CovidDocumentSamples.ErikaMustermann()) {
        givenRegisteredUser(person.registrationData())
    }

    fun givenRegisteredUser(data: RegistrationData) {
        Completable.mergeArray(
            registrationManager.persistRegistrationData(data),
            preferencesManager.persist(RegistrationManager.ONBOARDING_COMPLETED_KEY, true),
            preferencesManager.persist(RegistrationManager.USER_ID_KEY, data.id.toString()),
        ).blockingAwait()
    }

    fun updateRegisteredUser(update: (data: RegistrationData) -> RegistrationData) {
        application.registrationManager.getRegistrationData()
            .map { update(it) }
            .flatMapCompletable { registrationManager.persistRegistrationData(it) }
            .blockingAwait()
    }
}
