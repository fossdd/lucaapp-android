package de.culture4life.luca.testtools.pages

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.RequestErrorDialog
import io.github.kakaocup.kakao.text.KButton

class PaymentPage {
    val requestErrorDialog = RequestErrorDialog()
    val paymentIntroActionButton = KButton { withId(R.id.paymentIntroActionButton) }
}
