package de.culture4life.luca.testtools.samples

import de.culture4life.luca.network.pojo.payment.*
import de.culture4life.luca.util.TimeUtil
import de.culture4life.luca.util.toCurrencyAmountBigDecimal
import de.culture4life.luca.util.toUnixTimestamp
import java.util.*

class PaymentSamples {

    interface Payment {
        val id: String
        val locationId: String
        val locationName: String
        val invoiceAmount: Int
        val tipAmount: Int
        val tipPercentage: Int
        val totalAmount: Int
        val timestamp: Long
        val table: String?
        val paymentVerifier: String
        val checkoutUrl: String
        val partialAmountsSupported: Boolean

        val openPaymentRequestResponseData: OpenPaymentRequestResponseData
            get() {
                return OpenPaymentRequestResponseData(
                    id = id,
                    locationId = locationId,
                    invoiceAmount = invoiceAmount.toCurrencyAmountBigDecimal(),
                    openAmount = null,
                    status = OpenPaymentRequestResponseData.Status.OPEN,
                    partialAmountsSupported = partialAmountsSupported
                )
            }

        val operatorCheckoutRequestData: CreateOperatorCheckoutRequestData
            get() {
                return CreateOperatorCheckoutRequestData(
                    table = table,
                    tipAmount = tipAmount.toCurrencyAmountBigDecimal(),
                )
            }

        val customerCheckoutRequestData: CreateCustomerCheckoutRequestData
            get() {
                return CreateCustomerCheckoutRequestData(
                    locationId = locationId,
                    table = table,
                    invoiceAmount = invoiceAmount.toCurrencyAmountBigDecimal(),
                    tipAmount = tipAmount.toCurrencyAmountBigDecimal(),
                )
            }

        fun checkoutPaymentResponseData(
            status: CheckoutResponseData.Payment.Status = CheckoutResponseData.Payment.Status.CLOSED
        ): CheckoutResponseData.Payment {
            return CheckoutResponseData.Payment(
                id = id,
                locationName = locationName,
                invoiceAmount = invoiceAmount.toCurrencyAmountBigDecimal(),
                discountAmounts = null,
                tipAmount = tipAmount.toCurrencyAmountBigDecimal(),
                totalAmount = totalAmount.toCurrencyAmountBigDecimal(),
                timestamp = timestamp,
                table = table,
                status = status,
                paymentVerifier = paymentVerifier
            )
        }

        fun checkoutResponseData(status: CheckoutResponseData.Payment.Status = CheckoutResponseData.Payment.Status.CLOSED): CheckoutResponseData {
            return CheckoutResponseData(
                checkoutId = id,
                locationId = locationId,
                table = table,
                payment = checkoutPaymentResponseData(status)
            )
        }

        val createCheckoutResponseData: CreateCheckoutResponseData
            get() {
                return CreateCheckoutResponseData(
                    id = id,
                    providerCheckoutId = UUID.randomUUID().toString(),
                    providerCheckoutUrl = checkoutUrl
                )
            }
    }

    data class LocationPayment(
        val location: LocationSamples,
        override val invoiceAmount: Int = 5000,
        override val tipAmount: Int = 750,
        override val totalAmount: Int = 5750,
        override val tipPercentage: Int = 15,
        override val partialAmountsSupported: Boolean = true
    ) : Payment {
        override val id: String = UUID.randomUUID().toString()
        override val locationId: String = location.locationId
        override val locationName: String = location.locationResponseData.locationDisplayName ?: "Restaurant Light Blue"
        override val timestamp: Long = TimeUtil.getCurrentMillis().toUnixTimestamp()
        override val table: String? = location.table
        override val paymentVerifier: String = "ABC"
        override val checkoutUrl: String = UrlSamples.webApp
    }
}
