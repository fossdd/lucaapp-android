package de.culture4life.luca.testtools.pages

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction
import de.culture4life.luca.util.toCurrencyAmountString
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class PaymentResultPage(isEmbeddedInDialog: Boolean = true) {

    val successLayout = KView { withId(R.id.successLayout) }
    val sumTextView = KTextView { withId(R.id.sumTextView) }
    val locationNameTextView = KTextView { withId(R.id.locationNameTextView) }
    val amountValueTextView = KTextView { withId(R.id.amountValueTextView) }
    val originAmountValueTextView = KTextView { withId(R.id.originAmountValueTextView) }
    val tipValueTextView = KTextView { withId(R.id.tipValueTextView) }
    val codeValueTextView = KTextView { withId(R.id.codeValueTextView) }
    val nextButton = KButton { withId(R.id.nextButton) }
    val raffleButton = KButton { withId(R.id.raffleButton) }

    val errorLayout = KView { withId(R.id.errorLayout) }
    val retryButton = KButton { withId(R.id.retryButton) }

    init {
        SafeDialogInteraction.apply(this, isEmbeddedInDialog)
    }

    fun isDisplayingPayment(payment: PaymentData) {
        val context = ApplicationProvider.getApplicationContext<LucaApplication>()
        locationNameTextView.hasText(payment.locationName)
        sumTextView.hasText(context.getString(R.string.euro_amount, payment.totalAmount.toCurrencyAmountString()))
        tipValueTextView.hasText(context.getString(R.string.euro_amount, payment.tipAmount.toCurrencyAmountString()))
        codeValueTextView.hasText(payment.paymentVerifier)

        amountValueTextView.hasText(context.getString(R.string.euro_amount, payment.invoiceAmount.toCurrencyAmountString()))
        if (payment.isDiscounted) {
            amountValueTextView.hasTextColor(R.color.payment_promotion)
            originAmountValueTextView.isVisible()
            originAmountValueTextView.hasText(context.getString(R.string.euro_amount, payment.originalInvoiceAmount.toCurrencyAmountString()))
        } else {
            amountValueTextView.hasTextColor(R.color.white)
            originAmountValueTextView.isGone()
        }
    }
}
