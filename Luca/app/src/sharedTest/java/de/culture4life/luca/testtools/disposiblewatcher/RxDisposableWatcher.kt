package de.culture4life.luca.testtools.disposiblewatcher

import io.reactivex.rxjava3.core.*
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.internal.observers.*
import io.reactivex.rxjava3.internal.operators.maybe.MaybeCallbackObserver
import io.reactivex.rxjava3.internal.subscribers.LambdaSubscriber
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import org.reactivestreams.Subscriber
import java.lang.ref.WeakReference

// origin idea from https://github.com/andreyfomenkov/rx-disposable-watcher
class RxDisposableWatcher {

    private val records = HashMap<Int, WeakDisposable>()
    private val counter = HashMap<Int, Int>()

    private var disposableCounter = 0
    private var stopAtDisposableCounter = 0

    private val observableFunction = BiFunction { _: Observable<Any>, observer: Observer<Any> ->
        if (observer is LambdaObserver) {
            addDisposableRecord(observer)
        }
        observer
    }

    private val singleFunction = BiFunction { _: Single<Any>, observer: SingleObserver<Any> ->
        when (observer) {
            is ConsumerSingleObserver -> addDisposableRecord(observer)
            is BiConsumerSingleObserver -> addDisposableRecord(observer)
        }
        observer
    }

    private val completableFunction = BiFunction { _: Completable, observer: CompletableObserver ->
        when (observer) {
            is EmptyCompletableObserver -> addDisposableRecord(observer)
            is CallbackCompletableObserver -> addDisposableRecord(observer)
        }
        observer
    }

    private val maybeFunction = BiFunction { _: Maybe<Any>, observer: MaybeObserver<Any> ->
        if (observer is MaybeCallbackObserver) {
            addDisposableRecord(observer)
        }
        observer
    }

    private val flowableFunction = BiFunction { _: Flowable<Any>, observer: Subscriber<Any> ->
        if (observer is LambdaSubscriber) {
            addDisposableRecord(observer)
        }
        observer
    }

    fun init(stopAtDisposableCounter: Int) {
        this.stopAtDisposableCounter = stopAtDisposableCounter

        RxJavaPlugins.setOnObservableSubscribe(observableFunction)
        RxJavaPlugins.setOnSingleSubscribe(singleFunction)
        RxJavaPlugins.setOnCompletableSubscribe(completableFunction)
        RxJavaPlugins.setOnMaybeSubscribe(maybeFunction)
        RxJavaPlugins.setOnFlowableSubscribe(flowableFunction)
    }

    @Synchronized
    private fun addDisposableRecord(disposable: Disposable) {
        disposableCounter++

        if (disposableCounter == stopAtDisposableCounter) {
            throw Exception("stopped for possible leak source")
        }

        val hashCode = disposable.hashCode()
        records[hashCode] = WeakDisposable(disposable)
        counter[hashCode] = disposableCounter
    }

    @Synchronized
    fun probe(): List<ProbeEntry> {
        val probes = mutableListOf<ProbeEntry>()

        records.entries.forEach { (hashCode, record) ->
            val counter = counter[hashCode]!!
            val disposable = record.get()

            if (disposable?.isDisposed == false) {
                probes += ProbeEntry(disposable, counter)
            }
        }
        return probes
    }
}

data class ProbeEntry(val disposable: Disposable, val disposableCounter: Int)

private typealias WeakDisposable = WeakReference<Disposable>
