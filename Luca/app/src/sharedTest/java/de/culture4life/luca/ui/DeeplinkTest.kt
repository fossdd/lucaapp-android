package de.culture4life.luca.ui

import android.net.Uri
import de.culture4life.luca.R
import de.culture4life.luca.testtools.LucaActivityTest
import de.culture4life.luca.testtools.pages.CheckInPage
import de.culture4life.luca.testtools.pages.MyLucaPage
import de.culture4life.luca.testtools.preconditions.*
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.testtools.rules.LucaActivityScenarioRule
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.testtools.samples.PaymentSamples
import de.culture4life.luca.testtools.samples.UrlSamples
import de.culture4life.luca.ui.splash.SplashActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DeeplinkTest : LucaActivityTest<SplashActivity>(LucaActivityScenarioRule.create()) {

    @get:Rule
    val fixedTimeRule = FixedTimeRule()

    private val registrationPreconditions = RegistrationPreconditions()
    private val dailyKeyPreconditions = DailyKeyPreconditions()

    @Before
    fun setup() {
        TermsPreconditions().givenTermsAccepted()
        OnboardingPreconditions().givenOnboardingSeen()
        WhatIsNewPreconditions().givenAllNewsSeen()

        mockServerPreconditions.givenTimeSync()
    }

    @Test
    fun checkIn() {
        val location = LocationSamples.Restaurant()
        val deepLink = location.deeplink
        registrationPreconditions.givenRegisteredUser()
        dailyKeyPreconditions.givenStoredDailyKey()
        mockServerPreconditions.givenLocation(location)

        activityScenarioRule.launchDeepLink(deepLink)

        CheckInPage().run { isDisplayed() }
    }

    @Test
    fun document() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccFastNegative()
        val deeplink = document.asDeepLink()
        registrationPreconditions.givenRegisteredUser(document.person)
        mockServerPreconditions.givenRedeemDocument()

        getInitializedManager(application.consentManager)
        activityScenarioRule.launchDeepLink(deeplink)

        MyLucaPage().run {
            consentsDialog.acceptButton.click()
            documentList.childAt<MyLucaPage.DocumentItem>(0) {
                assertIsExpectedItemType()
                title.containsText(application.getString(R.string.certificate_type_test_fast) + ": " + application.getString(R.string.certificate_test_outcome_negative))
            }
        }
    }

    @Test
    fun paymentResult() {
        // What should be tested:
        //  The payment flow opens an external WebApp. When we click the "final" button then the external WebApp is closed and our app will
        //  proceed with the payment flow (showing the result).
        // Currently we found no way to simulate the warm start together with multiple activities (SplashActivity as Deeplink entry and MainActivity
        //  which should be "resumed").

        val payment = PaymentSamples.LocationPayment(LocationSamples.RestaurantWithTable())
        val deepLink = Uri.parse(UrlSamples.checkoutResult(payment.id))

        registrationPreconditions.givenRegisteredUser()

        // simulate payment WebApp finish by "return to app" button
        activityScenarioRule.launchDeepLink(deepLink)

        // What we can test:
        //  Until we found a solution for "warm start together with multiple activities" we can just assume it accepts the link and will "not" navigate.
        CheckInPage().isDisplayed()
    }
}
