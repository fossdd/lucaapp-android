package de.culture4life.luca.testtools.pages.dialogs

import androidx.annotation.StringRes
import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultOkCancelDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction

class RequestErrorDialog(
    @StringRes
    private val title: Int = R.string.error_request_failed_title
) : DefaultOkCancelDialog() {

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        baseDialog.title.containsText(context.getString(title))
        baseDialog.positiveButton.containsText(context.getString(R.string.action_retry))
    }
}
