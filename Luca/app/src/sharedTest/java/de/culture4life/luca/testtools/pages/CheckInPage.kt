package de.culture4life.luca.testtools.pages

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.ContactInfoMissingDialog
import de.culture4life.luca.testtools.pages.elements.QrCodeScannerElement
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class CheckInPage {

    val title = KTextView { withId(R.id.actionBarTitleTextView) }

    val contactInfoMissingDialog = ContactInfoMissingDialog()
    val scanner = QrCodeScannerElement()
    val showQrCodeButton = KButton { withId(R.id.showQrCodeButton) }
    val createPrivateMeetingButton = KButton { withId(R.id.createMeetingButton) }

    fun isDisplayed() {
        with(title) {
            hasText(R.string.navigation_check_in)
            isDisplayed()
        }
    }
}
