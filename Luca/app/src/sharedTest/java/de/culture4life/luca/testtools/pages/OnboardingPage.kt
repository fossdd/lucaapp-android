package de.culture4life.luca.testtools.pages

import de.culture4life.luca.R
import io.github.kakaocup.kakao.check.KCheckBox
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class OnboardingPage {
    val title = KTextView { withId(R.id.onboardingTitleTextView) }
    val termsCheckBox = KCheckBox { withId(R.id.termsCheckBox) }
    val privacyPolicyCheckBox = KCheckBox { withId(R.id.privacyPolicyCheckBox) }
    val finishButton = KButton { withId(R.id.primaryActionButton) }
}
