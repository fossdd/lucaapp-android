package de.culture4life.luca.ui.payment.children

import android.content.Intent
import androidx.core.os.bundleOf
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.VerificationModes
import androidx.test.espresso.intent.matcher.IntentMatchers
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.testtools.LucaFlowFragmentTest
import de.culture4life.luca.testtools.mocks.IntentMocks
import de.culture4life.luca.testtools.pages.PaymentWepAppPage
import de.culture4life.luca.testtools.preconditions.MockServerPreconditions
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.testtools.samples.PaymentSamples
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import org.junit.Test

class PaymentWebAppFragmentTest :
    LucaFlowFragmentTest<PaymentWebAppFragment, PaymentFlowViewModel>(PaymentWebAppFragment::class.java, PaymentFlowViewModel::class.java) {

    @Test
    fun resumesFromWebApp() {
        val checkoutLocation = LocationSamples.RestaurantWithTable()
        val payment = PaymentSamples.LocationPayment(checkoutLocation)

        mockServerPreconditions.run {
            givenPaymentSignIn()
            givenPaymentCreateCheckout(payment)
            givenPaymentCheckout(payment)
        }

        IntentMocks.givenWebAppResponse(payment.checkoutUrl, scenario = fragmentScenarioRule)

        launchScenario(bundleOf(PaymentFlowViewModel.ARGUMENT_PAYMENT_LOCATION_DATA_KEY to checkoutLocation.locationData))

        mockServerPreconditions.run {
            assert(MockServerPreconditions.Route.PaymentSignIn)
            assert(MockServerPreconditions.Route.PaymentCreateCheckout)
            assert(MockServerPreconditions.Route.PaymentGetCheckout, mapOf("checkoutId" to payment.id))
        }

        if (!LucaApplication.isRunningUnitTests()) {
            Intents.intended(IntentMatchers.hasAction(Intent.ACTION_MAIN), VerificationModes.times(1))
        }
        IntentMocks.assertWebAppIntent(payment.checkoutUrl)

        Intents.assertNoUnverifiedIntents() // shouldn't start WebApp in a loop

        PaymentWepAppPage(false)
            .isDisplayed()
    }
}
