package de.culture4life.luca.testtools.pages.dialogs

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction
import io.github.kakaocup.kakao.common.utilities.getResourceString
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class CheckInBottomSheet : DefaultDialog {

    val title = KTextView { withId(R.id.checkInHeaderTextView) }
    val info = KTextView { withId(R.id.checkInDescriptionTextView) }
    val checkIn = KButton { withId(R.id.actionButton) }
    val cancel = KButton { withId(R.id.cancelButton) }

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        title.isDisplayed()
        title.hasText(R.string.venue_check_in_confirmation_title)

        info.isDisplayed()
        val infoText = getResourceString(R.string.venue_check_in_confirmation_description)
        val infoTextParts = infoText.split("%1\$s")
        infoTextParts.forEach { info.containsText(it) }

        checkIn.isDisplayed()
        checkIn.hasText(R.string.action_check_in)

        cancel.isDisplayed()
        cancel.hasText(R.string.action_cancel)
    }
}
