package de.culture4life.luca.ui.payment.children

import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.testtools.LucaFlowFragmentTest
import de.culture4life.luca.testtools.actions.scrollToAndPerform
import de.culture4life.luca.testtools.pages.PaymentRafflePage
import de.culture4life.luca.testtools.pages.PaymentResultPage
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.testtools.samples.LucaPaySamples
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import org.junit.Ignore
import org.junit.Test

class PaymentResultFragmentTest : LucaFlowFragmentTest<PaymentResultFragment, PaymentFlowViewModel>(
    PaymentResultFragment::class.java,
    PaymentFlowViewModel::class.java
) {

    private val page = PaymentResultPage(isEmbeddedInDialog = false)
    private val locationData = LocationSamples.Restaurant().locationData
    private val successfulPayment by lazy { LucaPaySamples.ErikaMustermann().payments.first() }
    private val failedPayment by lazy { successfulPayment.copy(status = PaymentData.Status.ERROR) }

    @Test
    fun showSuccessLayoutWithCorrectData() {
        launchScenario()
        sharedViewModel.paymentResult.postValue(successfulPayment)
        page.run {
            errorLayout.isGone()
            successLayout.isDisplayed()
            isDisplayingPayment(successfulPayment)
        }
    }

    @Test
    fun showSuccessLayoutWithDiscount() {
        launchScenario()

        val successfulPaymentWithoutDiscount = successfulPayment
        val successfulPaymentWithDiscount = successfulPayment.copy(
            invoiceAmount = successfulPayment.invoiceAmount / 2,
            discountAmount = successfulPayment.invoiceAmount / 2,
            originalInvoiceAmount = successfulPayment.invoiceAmount
        )

        updatePayment(successfulPaymentWithoutDiscount)
        page.isDisplayingPayment(successfulPaymentWithoutDiscount)

        updatePayment(successfulPaymentWithDiscount)
        page.isDisplayingPayment(successfulPaymentWithDiscount)

        // reset discount campaign (like ended just now)
        updatePayment(successfulPaymentWithoutDiscount)
        // expect discount ui changes are reverted
        page.isDisplayingPayment(successfulPaymentWithoutDiscount)
    }

    @Test
    fun showErrorLayout() {
        launchScenario()
        sharedViewModel.paymentResult.postValue(failedPayment)
        page.run {
            errorLayout.isDisplayed()
            successLayout.isGone()
        }
    }

    @Ignore("Flaky")
    @Test
    fun showRaffleScreen() {
        val raffleCampaign = LucaPaySamples.ActiveCampaign(
            campaignName = PaymentManager.CAMPAIGN_PAY_RAFFLE,
            locationId = locationData.locationId
        )
        mockServerPreconditions.givenCampaigns(raffleCampaign.locationId, raffleCampaign)
        launchScenario()
        sharedViewModel.paymentLocationData = locationData
        sharedViewModel.paymentResult.postValue(successfulPayment)
        page.raffleButton.scrollToAndPerform { click() }
        PaymentRafflePage().isDisplayed()
    }

    @Ignore("Side-effect causes showRaffleScreen to fail")
    @Test
    fun doNotShowRaffleScreen() {
        val raffleCampaign = LucaPaySamples.InActiveCampaign(
            campaignName = PaymentManager.CAMPAIGN_PAY_RAFFLE,
            locationId = locationData.locationId
        )
        mockServerPreconditions.givenCampaigns(raffleCampaign.locationId, raffleCampaign)
        launchScenario()
        sharedViewModel.paymentLocationData = locationData
        sharedViewModel.paymentResult.postValue(successfulPayment)
        page.run {
            raffleButton.isNotDisplayed()
        }
    }

    @Test
    fun onRecreate() {
        launchScenario()
        val payment = LucaPaySamples.ErikaMustermann().payments.first()
        sharedViewModel.paymentResult.postValue(payment)
        page.isDisplayingPayment(payment)
        recreateScenario()
        page.isDisplayingPayment(payment)
    }

    private fun updatePayment(payment: PaymentData) {
        sharedViewModel.apply {
            paymentResult.postValue(payment)
        }
    }
}
