package de.culture4life.luca.testtools.preconditions

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.ui.onboarding.OnboardingActivity

class OnboardingPreconditions {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()
    private val preferencesManager by lazy { application.getInitializedManager(application.preferencesManager).blockingGet() }

    fun givenOnboardingSeen() {
        preferencesManager.persist(OnboardingActivity.WELCOME_SCREEN_SEEN_KEY, true).blockingAwait()
    }
}
