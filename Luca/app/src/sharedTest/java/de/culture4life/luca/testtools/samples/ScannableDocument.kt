package de.culture4life.luca.testtools.samples

interface ScannableDocument {

    val qrCodeContent: String
}
