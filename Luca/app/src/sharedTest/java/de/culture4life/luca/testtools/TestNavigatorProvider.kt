package de.culture4life.luca.testtools

import androidx.navigation.NavDestination
import androidx.navigation.NavGraphNavigator
import androidx.navigation.Navigator
import androidx.navigation.NavigatorProvider
import androidx.navigation.fragment.FragmentNavigator

/**
 * Basically just a copy of [androidx.navigation.testing.TestNavigatorProvider].
 *
 * We expect the situation: fun createDestination() = [FragmentNavigator.Destination]
 * But the origin TestNavigatorProvider returns just a [NavDestination].
 * In our BaseFragment we use [FragmentNavigator.Destination] for easy "safe navigation" check.
 */
class TestNavigatorProvider : NavigatorProvider() {

    /**
     * A [Navigator] that only supports creating destinations.
     */
    private val navigator = @Navigator.Name("TestNavigator") object : Navigator<FragmentNavigator.Destination>() {
        override fun createDestination() = FragmentNavigator.Destination(this)
    }

    init {
        addNavigator(NavGraphNavigator(this))
        addNavigator("test", navigator)
    }

    override fun <T : Navigator<out NavDestination>> getNavigator(name: String): T {
        return try {
            super.getNavigator(name)
        } catch (e: IllegalStateException) {
            @Suppress("UNCHECKED_CAST")
            navigator as T
        }
    }
}
