package de.culture4life.luca.testtools.pages

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction
import de.culture4life.luca.util.TextUtil
import de.culture4life.luca.util.toCurrencyAmountString
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class PaymentTipPage(isEmbeddedInDialog: Boolean = true) {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()
    private val root = R.id.paymentTipRootLayout
    private val rootView = KView { withId(root) }

    val title = KTextView { withId(R.id.titleTextView); isDescendantOfA { withId(root) } }
    val info = KTextView { withId(R.id.infoTextView); isDescendantOfA { withId(root) } }

    private val percentage00 = KView { withId(R.id.tipPercentageLayout00); isDescendantOfA { withId(root) } }
    private val percentage10 = KView { withId(R.id.tipPercentageLayout10); isDescendantOfA { withId(root) } }
    private val percentage15 = KView { withId(R.id.tipPercentageLayout15); isDescendantOfA { withId(root) } }
    private val percentage20 = KView { withId(R.id.tipPercentageLayout20); isDescendantOfA { withId(root) } }

    val totalAmount = KTextView { withId(R.id.totalAmountTextView); isDescendantOfA { withId(root) } }
    val primaryActionButton = KButton { withId(R.id.primaryActionButton); isDescendantOfA { withId(root) } }
    val cancelButton = KButton { withId(R.id.cancelButton) }
    val disclaimerTextView = KTextView { withId(R.id.paymentDisclaimerTextView); isDescendantOfA { withId(root) } }

    val invoiceLabelTextView = KTextView { withId(R.id.invoiceLabelTextView) }
    val originInvoiceAmountTextView = KTextView { withId(R.id.originAmountTextView) }
    val invoiceAmountTextView = KTextView { withId(R.id.invoiceAmountTextView) }
    val campaignInfoImageView = KTextView { withId(R.id.campaignInfoImageView) }
    val campaignInfoTextView = KTextView { withId(R.id.campaignInfoTextView) }

    init {
        SafeDialogInteraction.apply(this, isEmbeddedInDialog)
    }

    fun isSelectedPercentage(percentage: Int) {
        val choice = percentageView(percentage)
        choice.isSelected()

        val choices = mutableListOf(percentage00, percentage10, percentage15, percentage20)
        choices.remove(choice)
        choices.forEach {
            it.isNotSelected()
        }
    }

    fun selectPercentage(percentage: Int) {
        percentageView(percentage).click()
    }

    private fun percentageView(percentage: Int): KView {
        return when (percentage) {
            0 -> percentage00
            10 -> percentage10
            15 -> percentage15
            20 -> percentage20
            else -> throw UnsupportedOperationException()
        }
    }

    fun isDisplayed() {
        rootView.isDisplayed()
        title.hasText(R.string.pay_tip_headline)
        info.hasText(R.string.pay_tip_text)
        primaryActionButton.isVisible()
        cancelButton.isVisible()
        disclaimerTextView.hasText(R.string.rapyd_disclaimer)
    }

    fun assertTipAmountDisplayed(payment: PaymentAmounts) {
        val totalAmountEuroString = application.getString(R.string.euro_amount, payment.totalAmount.toCurrencyAmountString())
        val continueWithAmountString = TextUtil.getPlaceholderString(
            application,
            R.string.pay_continue_button,
            "amount" to totalAmountEuroString
        )

        totalAmount.hasText(totalAmountEuroString)
        primaryActionButton.hasText(continueWithAmountString)

        invoiceLabelTextView.isGone()
        invoiceAmountTextView.isGone()
        originInvoiceAmountTextView.isGone()
        campaignInfoTextView.isGone()
    }

    fun assertAmountDisplayedWithDiscount(payment: PaymentAmounts) {
        val totalAmountEuroString = application.getString(R.string.euro_amount, payment.totalAmount.toCurrencyAmountString())
        val invoiceAmountEuroString = application.getString(R.string.euro_amount, payment.invoiceAmount.toCurrencyAmountString())
        val remainingInvoiceEuroAmountString = application.getString(R.string.euro_amount, payment.remainingInvoiceAmount.toCurrencyAmountString())
        val campaignInfoString = TextUtil.getPlaceholderString(
            application,
            R.string.pay_details_discount_hint,
            "discountPercentage" to "${payment.discountPercentage}",
            "maximumDiscountAmount" to payment.maximumDiscountAmount.toCurrencyAmountString()
        )
        val continueWithAmountString = TextUtil.getPlaceholderString(
            application,
            R.string.pay_continue_button,
            "amount" to totalAmountEuroString
        )

        totalAmount.hasText(totalAmountEuroString)
        primaryActionButton.hasText(continueWithAmountString)

        invoiceLabelTextView.isVisible()
        invoiceLabelTextView.hasText(R.string.pay_invoice_amount)
        invoiceAmountTextView.isVisible()
        invoiceAmountTextView.hasTextColor(R.color.payment_promotion)
        invoiceAmountTextView.hasText(remainingInvoiceEuroAmountString)
        originInvoiceAmountTextView.isVisible()
        originInvoiceAmountTextView.hasText(invoiceAmountEuroString)
        campaignInfoImageView.isVisible()
        campaignInfoTextView.isVisible()
        campaignInfoTextView.hasText(campaignInfoString)
    }
}
