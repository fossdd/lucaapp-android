package de.culture4life.luca.ui.discovery

import android.content.Intent
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.VerificationModes
import androidx.test.espresso.intent.matcher.IntentMatchers
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.mocks.IntentMocks
import de.culture4life.luca.testtools.pages.DiscoveryPage
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import org.junit.Test

class DiscoveryFragmentTest : LucaFragmentTest<DiscoveryFragment>(LucaFragmentScenarioRule.create()) {

    private val discoveryUrl = "${application.getString(R.string.API_BASE_URL)}/discovery"

    @Test
    fun buttonOpensUrl() {
        // Given
        IntentMocks.givenWebAppResponse(discoveryUrl, scenario = fragmentScenarioRule)

        // When
        launchScenario(R.id.nav_graph_discovery)
        DiscoveryPage().discoverButton.click()

        // Then
        if (LucaApplication.isRunningInstrumentationTests()) {
            Intents.intended(IntentMatchers.hasAction(Intent.ACTION_MAIN), VerificationModes.times(1))
        }
        IntentMocks.assertWebAppIntent(discoveryUrl)
        Intents.assertNoUnverifiedIntents()
    }

}
