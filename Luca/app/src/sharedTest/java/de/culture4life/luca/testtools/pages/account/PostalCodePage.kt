package de.culture4life.luca.testtools.pages.account

import de.culture4life.luca.R
import io.github.kakaocup.kakao.text.KTextView

class PostalCodePage {

    val title = KTextView { withId(R.id.actionBarTitleTextView) }

    fun isDisplayed() {
        title.run {
            isDisplayed()
            hasText(R.string.account_tab_item_postal_code)
        }
    }
}
