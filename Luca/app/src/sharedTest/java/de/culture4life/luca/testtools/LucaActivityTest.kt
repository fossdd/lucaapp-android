package de.culture4life.luca.testtools

import androidx.appcompat.app.AppCompatActivity
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.culture4life.luca.testtools.rules.LucaActivityScenarioRule
import org.junit.Rule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
abstract class LucaActivityTest<ACTIVITY : AppCompatActivity>(
    @get:Rule
    val activityScenarioRule: LucaActivityScenarioRule<ACTIVITY>
) : LucaScenarioTest()
