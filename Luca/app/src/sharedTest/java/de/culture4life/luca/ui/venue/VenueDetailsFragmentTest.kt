package de.culture4life.luca.ui.venue

import android.os.Bundle
import androidx.core.os.bundleOf
import de.culture4life.luca.R
import de.culture4life.luca.network.pojo.LocationResponseData
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.pages.PaymentHistoryPage.PaymentFlow.PaymentFlowScenario
import de.culture4life.luca.testtools.pages.VenueDetailsPage
import de.culture4life.luca.testtools.preconditions.RegistrationPreconditions
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.testtools.samples.*
import de.culture4life.luca.ui.checkin.LocationUrl
import de.culture4life.luca.util.LucaUrlUtil
import org.junit.Ignore
import org.junit.Test
import java.net.HttpURLConnection.HTTP_NOT_FOUND
import java.util.*

class VenueDetailsFragmentTest : LucaFragmentTest<VenueDetailsFragment>(LucaFragmentScenarioRule.create()) {

    private val page = VenueDetailsPage()

    @Test
    fun titleIsAreaNameIfNotNullAndSubTitleIsGroupName() {
        // Given
        val location = LocationSamples.Restaurant().apply {
            locationResponseData = LocationResponseData(
                locationId = "123",
                areaName = "AreaName",
                groupName = "GroupName"
            )
        }
        mockServerPreconditions.givenLocation(location)

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to listOf(LocationUrl(LocationUrl.UrlType.MAP, "")),
            )
        )

        // Then
        VenueDetailsPage().run {
            title.isDisplayed()
            title.hasText("AreaName")
            subTitle.isDisplayed()
            subTitle.hasText("GroupName")
        }
    }

    @Test
    fun titleIsAreaNameIfNotNullAndSubTitleIsHiddenIfGroupNameIsNull() {
        // Given
        val location = LocationSamples.Restaurant().apply {
            locationResponseData = LocationResponseData(
                locationId = "123",
                areaName = "AreaName",
                groupName = null
            )
        }
        mockServerPreconditions.givenLocation(location)

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to listOf(LocationUrl(LocationUrl.UrlType.MAP, "")),
            )
        )

        // Then
        VenueDetailsPage().run {
            title.isDisplayed()
            title.hasText("AreaName")
            subTitle.isNotDisplayed()
        }
    }

    @Test
    fun titleIsGroupNameIfAreaNameNullAndSubTitleIsHidden() {
        // Given
        val location = LocationSamples.Restaurant().apply {
            locationResponseData = LocationResponseData(
                locationId = "123",
                areaName = null,
                groupName = "GroupName"
            )
        }
        mockServerPreconditions.givenLocation(location)

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to listOf(LocationUrl(LocationUrl.UrlType.MAP, "")),
            )
        )

        // Then
        VenueDetailsPage().run {
            title.isDisplayed()
            title.hasText("GroupName")
            subTitle.isNotDisplayed()
        }
    }

    @Test
    fun emptyStateOnlyShowsTitle() {
        // Given
        val location = LocationSamples.Restaurant().apply {
            locationResponseData = LocationResponseData(
                locationId = "123",
                areaName = "AreaName",
                groupName = "GroupName"
            )
        }
        mockServerPreconditions.givenLocation(location)

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
            )
        )

        // Then
        VenueDetailsPage().run {
            emptyTitle.isDisplayed()
            emptyTitle.hasText("AreaName")
            title.isNotDisplayed()
            subTitle.isNotDisplayed()
        }
    }

    @Test
    fun tableAndAmountAreShownWhenTableWasProvided() {
        // Given
        val location = LocationSamples.RestaurantWithTable()
        val openPaymentRequest = PaymentSamples.LocationPayment(location)
        val tableId = location.table
        val tableName = "TableName123"

        fixtureTableAndAmountAreShownWhenTableWasProvided(location, openPaymentRequest, tableId, tableName)
    }

    @Test
    fun openPaymentWithoutTable() {
        // Given
        val location = LocationSamples.RestaurantWithoutTables()
        val openPaymentRequest = PaymentSamples.LocationPayment(location)
        with(mockServerPreconditions) {
            givenLocation(location)
            givenPaymentOpenRequest(openPaymentRequest)
        }

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
            )
        )

        // Then
        VenueDetailsPage().run {
            tableIsHidden()
            amountIsDisplayed(openPaymentRequest.invoiceAmount)
        }
    }

    @Test
    fun discountCampaignWithoutOpenPayment() {
        val location = LocationSamples.Restaurant()
        val campaign = LucaPaySamples.Campaign.discount50(location.locationId)
        with(mockServerPreconditions) {
            givenLocation(location)
            mockServerPreconditions.givenCampaigns(location.locationId)
        }

        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
            )
        )
        page.campaignInfoIsHidden()

        updateAvailableCampaignInfo(location, campaign)
        page.campaignInfoIsDisplayed(campaign)

        updateNoCampaignsAvailable(location)
        page.campaignInfoIsHidden()
    }

    @Test
    fun discountCampaignWithOpenPayment() {
        val location = LocationSamples.RestaurantWithTable()
        val openPaymentRequest = PaymentSamples.LocationPayment(location)
        val campaign = LucaPaySamples.Campaign.discount50(location.locationId)
        with(mockServerPreconditions) {
            givenLocation(location)
            givenPaymentOpenRequest(openPaymentRequest)
            mockServerPreconditions.givenCampaigns(location.locationId)
        }

        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
                VenueDetailsFragment.ARGUMENT_ADDITIONAL_DATA_KEY to "{\"tableId\": \"${location.table}\"}"
            )
        )
        page.campaignInfoIsHidden()
        page.amountIsDisplayed(openPaymentRequest.invoiceAmount)

        updateAvailableCampaignInfo(location, campaign)
        page.campaignInfoIsDisplayed(campaign)
        page.amountWithDiscountIsDisplayed(openPaymentRequest.invoiceAmount / 2, openPaymentRequest.invoiceAmount)

        updateNoCampaignsAvailable(location)
        page.campaignInfoIsHidden()
        page.amountIsDisplayed(openPaymentRequest.invoiceAmount)

        updateAvailableCampaignInfo(location, campaign)
        updateNoOpenPaymentAvailable(openPaymentRequest)
        page.campaignInfoIsDisplayed(campaign)
        page.amountIsHidden()
    }

    @Test
    fun amountIsHiddenWhenNoOpenPaymentRequest() {
        // Given
        val location = LocationSamples.Restaurant()
        val tableId = UUID.randomUUID().toString()
        val tableName = "TableName123"

        with(mockServerPreconditions) {
            givenLocation(location)
            givenTableData(tableId, tableName)
        }

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_ADDITIONAL_DATA_KEY to "{\"tableId\": \"$tableId\"}",
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
            )
        )

        // Then
        VenueDetailsPage().run {
            tableIsDisplayed(tableName)
            amountIsHidden()
        }
    }

    @Test
    fun amountIsHiddenWhenOpenPaymentRequestIsGone() {
        // Given
        val location = LocationSamples.RestaurantWithTable()
        val openPaymentRequest = PaymentSamples.LocationPayment(location)
        val tableId = location.table
        val tableName = "TableName123"

        fixtureTableAndAmountAreShownWhenTableWasProvided(location, openPaymentRequest, tableId, tableName)

        mockServerPreconditions.givenPaymentOpenRequest(openPaymentRequest, HTTP_NOT_FOUND)
        waitFor(VenueDetailsViewModel.PAYMENT_REQUEST_POLLING_INTERVAL)

        VenueDetailsPage().run {
            tableIsDisplayed(tableName)
            amountIsHidden()
        }
    }

    @Ignore("Flaky")
    @Test
    fun userInitiatedPayment() {
        val location = LocationSamples.Restaurant()
        val payment = PaymentSamples.LocationPayment(location)
        val paymentFlowScenario = PaymentFlowScenario(this, payment)

        with(mockServerPreconditions) {
            givenLocation(location)
            givenPaymentSignIn()
            givenPaymentSignUp()
            givenPaymentCreateCheckout(payment)
        }

        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
            )
        )

        VenueDetailsPage().run {
            startPaymentButton.click()

            termsOfServiceUpdateInfoDialog.acceptButton.click()
            termsOfServiceUpdateDialog.acceptButton.click()
            lucaPayConsentDialog.acceptButton.click()

            paymentFlow.runPaymentFlow(paymentFlowScenario)
            title.isDisplayed()
        }
    }

    @Test
    fun operatorInitiatedPayment() {
        val location = LocationSamples.RestaurantWithTable()
        val payment = PaymentSamples.LocationPayment(location)
        val paymentFlowScenario = PaymentFlowScenario(this, payment, initiator = PaymentFlowScenario.INITIATOR.OPERATOR)
        val additionalData = LucaUrlUtil.getAdditionalDataFromVenueUrlIfAvailable(location.qrCodeContent).blockingGet()

        with(mockServerPreconditions) {
            givenLocation(location)
            givenPaymentSignIn()
            givenPaymentSignUp()
            givenPaymentOpenRequest(payment)
            givenPaymentOpenRequestCreateCheckout(payment)
        }

        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
                VenueDetailsFragment.ARGUMENT_ADDITIONAL_DATA_KEY to additionalData,
            )
        )

        VenueDetailsPage().run {
            startPaymentButton.click()

            termsOfServiceUpdateInfoDialog.acceptButton.click()
            termsOfServiceUpdateDialog.acceptButton.click()
            lucaPayConsentDialog.acceptButton.click()

            paymentFlow.runPaymentFlow(paymentFlowScenario)
            title.isDisplayed()
        }
    }

    @Test
    fun reportButtonShownWhenAtleastOneUrlShown() {
        // Given
        val location = LocationSamples.RestaurantWithTable()
        mockServerPreconditions.givenLocation(location)

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to listOf(LocationUrl(LocationUrl.UrlType.MAP, UrlSamples.webApp)),
            )
        )

        // Then
        VenueDetailsPage().run {
            reportButton.isDisplayed()
        }
    }

    @Test
    fun reportButtonHiddenWhenNoUrlsShown() {
        // Given
        val location = LocationSamples.RestaurantWithTable()
        mockServerPreconditions.givenLocation(location)

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
            )
        )

        // Then
        VenueDetailsPage().run {
            reportButton.isNotDisplayed()
        }
    }

    @Test
    fun paymentButtonNotShownWhenPaymentDisabled() {
        // Given
        val location = LocationSamples.RestaurantWithoutLucaPay()
        mockServerPreconditions.givenLocation(location)

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
            )
        )

        // Then
        VenueDetailsPage().startPaymentButton.isNotDisplayed()
    }

    @Test
    fun paymentCanBeRestartedWhenCancelled() {
        // Given
        val location = LocationSamples.Restaurant()
        val payment = PaymentSamples.LocationPayment(location)

        with(mockServerPreconditions) {
            givenLocation(location)
            givenPaymentSignIn()
            givenPaymentSignUp()
            givenPaymentCreateCheckout(payment)
        }

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
            )
        )

        // Then
        VenueDetailsPage().run {
            startPaymentButton.click()

            // Accept all terms
            termsOfServiceUpdateInfoDialog.acceptButton.click()
            termsOfServiceUpdateDialog.acceptButton.click()
            lucaPayConsentDialog.acceptButton.click()

            // First time opening of payment flow + cancel
            paymentFlow.amountPage.isDisplayed()
            paymentFlow.cancelButton.click()
            title.isDisplayed()

            // Second time opening payment flow
            startPaymentButton.click()
            paymentFlow.amountPage.isDisplayed()
        }
    }

    private fun launchScenario(bundle: Bundle) {
        RegistrationPreconditions().givenRegisteredUser(CovidDocumentSamples.ErikaMustermann())
        launchScenario(R.id.venueDetailsFragment) { putAll(bundle) }
        initializeUiExtensions()
    }

    private fun fixtureTableAndAmountAreShownWhenTableWasProvided(
        location: LocationSamples.Restaurant,
        openPaymentRequest: PaymentSamples.LocationPayment,
        tableId: String,
        tableName: String
    ) {
        with(mockServerPreconditions) {
            givenLocation(location)
            givenPaymentOpenRequest(openPaymentRequest)
            givenTableData(tableId, tableName)
        }

        // When
        launchScenario(
            bundleOf(
                VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY to location.locationResponseData,
                VenueDetailsFragment.ARGUMENT_ADDITIONAL_DATA_KEY to "{\"tableId\": \"$tableId\"}",
                VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY to emptyList<LocationUrl>(),
            )
        )

        // Then
        VenueDetailsPage().run {
            tableIsDisplayed(tableName)
            amountIsDisplayed(openPaymentRequest.invoiceAmount)
        }
    }

    private fun updateNoCampaignsAvailable(location: LocationSamples.Restaurant) {
        mockServerPreconditions.givenCampaigns(location.locationId)
        waitFor(VenueDetailsViewModel.PAYMENT_REQUEST_POLLING_INTERVAL)
    }

    private fun updateAvailableCampaignInfo(
        location: LocationSamples.Restaurant,
        campaign: LucaPaySamples.Campaign
    ) {
        mockServerPreconditions.givenCampaigns(location.locationId, campaign)
        waitFor(VenueDetailsViewModel.PAYMENT_CAMPAIGN_POLLING_INTERVAL)
    }

    private fun updateNoOpenPaymentAvailable(payment: PaymentSamples.Payment) {
        mockServerPreconditions.givenPaymentOpenRequest(payment, HTTP_NOT_FOUND)
        waitFor(VenueDetailsViewModel.PAYMENT_CAMPAIGN_POLLING_INTERVAL)
    }
}
