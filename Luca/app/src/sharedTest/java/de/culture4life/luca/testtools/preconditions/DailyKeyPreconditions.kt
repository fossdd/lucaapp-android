package de.culture4life.luca.testtools.preconditions

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.testtools.samples.DailyKeySamples

class DailyKeyPreconditions {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()
    private val cryptoManager by lazy { application.getInitializedManager(application.cryptoManager).blockingGet() }

    fun givenStoredDailyKey(dailyKey: DailyKeySamples = DailyKeySamples.WithoutExpiration()) {
        cryptoManager.persistDailyPublicKey(dailyKey.dailyPublicKeyData).blockingAwait()
    }
}
