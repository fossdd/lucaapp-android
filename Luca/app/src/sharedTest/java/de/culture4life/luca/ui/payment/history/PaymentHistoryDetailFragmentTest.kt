package de.culture4life.luca.ui.payment.history

import de.culture4life.luca.R
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.pages.PaymentHistoryDetailPage
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.testtools.samples.LucaPaySamples
import org.joda.time.DateTime
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit

class PaymentHistoryDetailFragmentTest : LucaFragmentTest<PaymentHistoryDetailFragment>(LucaFragmentScenarioRule.create()) {

    private val payment = LucaPaySamples.ErikaMustermann().payments[0]

    @get:Rule
    val fixedTimeRule = FixedTimeRule(
        currentDateTime = DateTime(payment.timestamp + TimeUnit.MINUTES.toMillis(5))
    )

    private fun launchScenario(givenPayment: PaymentData? = null) {
        launchScenario(R.id.paymentHistoryDetailFragment) {
            putSerializable(
                PaymentHistoryDetailFragment.ARGUMENT_PAYMENT_DATA_KEY,
                givenPayment ?: payment
            )
        }
    }

    @Test
    fun showPaymentDataFromTheArguments() {
        val tableName = "Custom table name"
        mockServerPreconditions.givenTableData(tableId = payment.table!!, tableName = tableName)
        launchScenario()
        PaymentHistoryDetailPage().run {
            assertPaymentDisplayed(payment)
            assertTableDisplayed(tableName)
        }
    }

    @Test
    fun hideTableNameIfUnknown() {
        launchScenario()
        PaymentHistoryDetailPage().run {
            assertPaymentDisplayed(payment)
            assertTableNotDisplayed()
        }
    }

    @Test
    fun showDiscount() {
        val paymentWithDiscount = payment.copy(
            invoiceAmount = payment.invoiceAmount / 2,
            discountAmount = payment.invoiceAmount / 2,
            originalInvoiceAmount = payment.invoiceAmount
        )
        launchScenario(paymentWithDiscount)
        PaymentHistoryDetailPage().run {
            assertPaymentDisplayed(paymentWithDiscount)
        }
    }
}
