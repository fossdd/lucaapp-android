package de.culture4life.luca.testtools.pages

import de.culture4life.luca.R
import io.github.kakaocup.kakao.text.KButton

class MainPage {

    val navigation = Navigation()

    class Navigation {
        val myLuca = KButton {
            withText(R.string.navigation_my_luca)
            isDescendantOfA { withId(R.id.bottomNavigationView) }
            isVisible()
        }
        val checkIn = KButton {
            withText(R.string.navigation_check_in)
            isDescendantOfA { withId(R.id.bottomNavigationView) }
            isVisible()
        }
        val lucaPay = KButton {
            withText(R.string.pay_nav_bar)
            isDescendantOfA { withId(R.id.bottomNavigationView) }
            isVisible()
        }
        val account = KButton {
            withText(R.string.navigation_account)
            isDescendantOfA { withId(R.id.bottomNavigationView) }
            isVisible()
        }
    }
}
