package de.culture4life.luca.testtools.samples

import de.culture4life.luca.testtools.rules.FixedTimeRule

object ConfigurationSamples {

    // When the app is at this given DateTime, then the samples here are working like expected.
    //  Means usually all are valid at the given time (if not other mentioned).
    val referenceDateTime = FixedTimeRule.parseDateTime("2021-06-15T15:30:00")
}
