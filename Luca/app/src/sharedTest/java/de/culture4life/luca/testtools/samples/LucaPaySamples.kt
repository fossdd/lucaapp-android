package de.culture4life.luca.testtools.samples

import de.culture4life.luca.network.pojo.payment.CampaignResponseData
import de.culture4life.luca.network.pojo.payment.ConsumerInformationResponseData
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.util.TimeUtil
import de.culture4life.luca.util.toCurrencyAmountBigDecimal
import de.culture4life.luca.util.toUnixTimestamp
import java.util.*
import java.util.concurrent.TimeUnit

class LucaPaySamples {

    interface Consumer {
        val id: String
        val username: String
        val password: String
        val payments: List<PaymentData>
        val payCards: List<ConsumerInformationResponseData.PayCard>
    }

    data class ErikaMustermann(
        override val id: String = UUID.randomUUID().toString(),
        override val username: String = "485f40b1-655f-3ea3-9b85-907e46cc13ec",
        override val password: String = "3f2b6c0e-776b-4446-9f98-fd8a872a7bee",
        override val payments: List<PaymentData> = listOf(
            PaymentData(
                id = UUID.randomUUID().toString(),
                locationName = "Restaurant Light Blue",
                locationId = UUID.randomUUID().toString(),
                invoiceAmount = 19999, // 199,99 €
                tipAmount = 1000, // 10 €
                totalAmount = 20999, // 209,99 €
                timestamp = 1651576979000L,
                table = "23",
                status = PaymentData.Status.CLOSED,
                paymentVerifier = "0ZYDU"
            )
        ),
        override val payCards: List<ConsumerInformationResponseData.PayCard> = listOf(
            ConsumerInformationResponseData.PayCard(
                name = username,
                type = "VISA",
                last4digits = "4242",
                expirationYear = "24",
                expirationMonth = "12"
            )
        )
    ) : Consumer

    class SignIn {
        val accessToken =
            "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2NTRhMDM5Zi03YzkyLTQ5NjMtODU5MC0zYzA0YjRhNTRkYTAiLCJpYXQiOjE2NTExNzM4MzEsImV4cCI6MTY1MTE5MTgzMX0.p3POwPF8aYK4kmEAtabSGhVk5Ufh9mh4VPnB1GWU51eBxqyUmHFXIPA-km1NRKZPi2g9L_PVP0DES3A36mYKwg"
        val expirationTimestamp = TimeUtil.getCurrentMillis() + TimeUnit.DAYS.toMillis(1) // in the future
    }

    interface Campaign {
        val campaignId: String
        val locationId: String
        val campaignName: String
        val creationTimestamp: Long
        val startTimestamp: Long?
        val endTimestamp: Long?
        val discountPercentage: Int?
        val maximumDiscountAmount: Int?

        val responseData: CampaignResponseData
            get() {
                return CampaignResponseData(
                    campaignId = campaignId,
                    creationTimestamp = creationTimestamp.toUnixTimestamp(),
                    paymentCampaign = CampaignResponseData.PaymentCampaign(
                        name = campaignName,
                        startTimestamp = startTimestamp?.toUnixTimestamp(),
                        endTimestamp = endTimestamp?.toUnixTimestamp(),
                        discountPercentage = discountPercentage,
                        maximumDiscountAmount = maximumDiscountAmount?.toCurrencyAmountBigDecimal()
                    )
                )
            }

        companion object {
            fun discount50(locationId: String): Campaign {
                return ActiveCampaign().copy(
                    locationId = locationId,
                    campaignName = "luca-discount-50-off",
                    discountPercentage = 50,
                    maximumDiscountAmount = 10000
                )
            }
        }
    }

    data class ActiveCampaign(
        override val campaignId: String = UUID.randomUUID().toString(),
        override val locationId: String = UUID.randomUUID().toString(),
        override val campaignName: String = "Active Campaign",
        override val creationTimestamp: Long = TimeUtil.getCurrentMillis() - TimeUnit.DAYS.toMillis(2),
        override val startTimestamp: Long? = TimeUtil.getCurrentMillis() - TimeUnit.DAYS.toMillis(1),
        override val endTimestamp: Long? = TimeUtil.getCurrentMillis() + TimeUnit.DAYS.toMillis(1),
        override val discountPercentage: Int? = null,
        override val maximumDiscountAmount: Int? = null
    ) : Campaign

    data class InActiveCampaign(
        override val campaignId: String = UUID.randomUUID().toString(),
        override val locationId: String = UUID.randomUUID().toString(),
        override val campaignName: String = "Inactive Campaign",
        override val creationTimestamp: Long = TimeUtil.getCurrentMillis() - TimeUnit.DAYS.toMillis(3),
        override val startTimestamp: Long? = TimeUtil.getCurrentMillis() - TimeUnit.DAYS.toMillis(2),
        override val endTimestamp: Long? = TimeUtil.getCurrentMillis() - TimeUnit.DAYS.toMillis(1),
        override val discountPercentage: Int? = null,
        override val maximumDiscountAmount: Int? = null
    ) : Campaign
}
