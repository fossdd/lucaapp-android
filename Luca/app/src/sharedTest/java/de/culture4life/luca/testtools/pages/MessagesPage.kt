package de.culture4life.luca.testtools.pages

import androidx.test.espresso.DataInteraction
import de.culture4life.luca.R
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.testtools.pages.dialogs.ConsentDialog
import de.culture4life.luca.ui.messages.MessageListItem
import io.github.kakaocup.kakao.list.KAbsListView
import io.github.kakaocup.kakao.list.KAdapterItem
import io.github.kakaocup.kakao.text.KTextView

class MessagesPage {

    val title = KTextView {
        isDescendantOfA { withId(R.id.messagesRootLayout) }
        withId(R.id.actionBarTitleTextView)
    }
    val messageList = KAbsListView(
        { withId(R.id.messageListView) },
        { itemType(MessagesPage::MessageItem) }
    )
    val termsOfServiceUpdateDialog = ConsentDialog(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_ID)

    fun isDisplayed() {
        title.run {
            isDisplayed()
            hasText(R.string.accessed_data_heading)
        }
    }

    class MessageItem(interaction: DataInteraction) : KAdapterItem<MessageListItem>(interaction) {
        val title = KTextView(interaction) { withId(R.id.itemTitleTextView) }
        val description = KTextView(interaction) { withId(R.id.itemDescriptionTextView) }

        fun assertIsPayRevocationMessage() {
            title.hasText(R.string.pay_revocation_notification_title)
        }

        fun assertIsEnrollmentTokenMessage() {
            title.hasText(R.string.notification_luca_id_enrollment_token_title)
        }

        fun assertIsEnrollmentErrorMessage() {
            title.hasText(R.string.notification_luca_id_enrollment_error_title)
        }

        fun assertIsPostalCodeMessage() {
            title.hasText(R.string.notification_postal_code_matching_title)
        }

        fun assertIsLucaIdVerificationSuccessMessage() {
            title.hasText(R.string.luca_id_verification_success_title_sub)
        }

        fun assertIsTermsUpdateMessage() {
            title.hasText(R.string.notification_terms_update_title)
            description.hasText(R.string.notification_terms_update_description)
        }

        fun assertIsLucaConnectMessage() {
            title.hasText(R.string.notification_luca_connect_supported_title)
        }
    }
}
