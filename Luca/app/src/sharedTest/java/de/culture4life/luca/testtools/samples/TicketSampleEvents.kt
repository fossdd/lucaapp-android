package de.culture4life.luca.testtools.samples

import androidx.test.core.app.ApplicationProvider
import com.nexenio.rxkeystore.RxKeyStore
import de.culture4life.luca.document.provider.eventticket.EventTicketDocument
import de.culture4life.luca.util.CertificateUtil
import de.culture4life.luca.util.TimeUtil
import io.jsonwebtoken.Jwts
import java.security.PrivateKey

abstract class TicketSampleEvents : ScannableDocument {

    abstract val claims: Map<String, Any>

    val jwt: String by lazy { Jwts.builder().addClaims(claims).setHeader(header).signWith(privateKey).compact() }

    override val qrCodeContent: String by lazy { UrlSamples.tickets(jwt) }

    private val header: Map<String, Any> = mapOf("alg" to "ES256")

    private val privateKey: PrivateKey
        get() = CertificateUtil.loadPrivatePEMKey(
            "connfair_private_key.pem",
            ApplicationProvider.getApplicationContext(),
            RxKeyStore.KEY_ALGORITHM_EC
        )

    data class Exhibition(
        val eventName: String = "13. Deutscher Seniorentag 2021",
        val id: String = "935183eb-3d52-4ef8-8ca1-c2ca184a3776",
        override val claims: Map<String, Any> = mapOf(
            "iss" to "Connfair",
            "sub" to id,
            "l" to "Hannover Congress Centrum (HCC)",
            "e" to eventName,
            "c" to EventTicketDocument.Category.EXHIBITION.value,
            "st" to (TimeUtil.getCurrentMillis() / 1000).toInt() - 100,
            "et" to (TimeUtil.getCurrentMillis() / 1000).toInt() + 100,
            "iat" to (TimeUtil.getCurrentMillis() / 1000).toInt() - 200,
            "v" to "e320fdf121951c87",
            "fn" to "Linus",
            "ln" to "Weiss"
        )
    ) : TicketSampleEvents()
}
