package de.culture4life.luca.ui.payment.raffle

import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResultListener
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.actions.forceClick
import de.culture4life.luca.testtools.pages.PaymentRafflePage
import de.culture4life.luca.testtools.preconditions.RegistrationPreconditions
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import de.culture4life.luca.testtools.samples.LucaPaySamples
import de.culture4life.luca.ui.payment.raffle.PaymentRaffleBottomSheetFragment.Companion.PAYMENT_RAFFLE_REQUEST_CODE
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.*

class PaymentRaffleBottomSheetFragmentTest : LucaFragmentTest<PaymentRaffleBottomSheetFragment>(LucaFragmentScenarioRule.create()) {

    private fun launchWithPaymentId(paymentId: String = UUID.randomUUID().toString()) {
        fragmentScenarioRule.launch(
            bundleOf(PaymentRaffleBottomSheetFragment.ARGUMENT_PAYMENT_ID to paymentId)
        )
    }

    @Test
    fun buttonEnabledWhenCheckboxCheckedAndEmailEntered() {
        // When
        launchWithPaymentId()

        // Then
        PaymentRafflePage(isEmbeddedInDialog = false).run {
            primaryActionButton.isDisabled()
            checkbox.setChecked(true)
            primaryActionButton.isDisabled()

            // trailing whitespace simulates applied suggestion from keyboard
            val email = CovidDocumentSamples.ErikaMustermann().registrationData().email!! + " "
            emailInput.replaceText(email)
            primaryActionButton.isEnabled()
            checkbox.setChecked(false)
            primaryActionButton.isDisabled()
        }
    }

    @Test
    fun onSuccessResultIsSetAndFragmentDismissed() {
        // Given
        var result = false
        val samplePayment = LucaPaySamples.ErikaMustermann()
        RegistrationPreconditions().givenRegisteredUser()
        mockServerPreconditions.run {
            givenPaymentSignUp()
            givenPaymentSignIn()
            givenPaymentCampaignSubmission(samplePayment)
        }

        // When
        launchWithPaymentId(samplePayment.payments.first().id)
        fragmentScenarioRule.scenario.onFragment { fragment ->
            fragment.setFragmentResultListener(PAYMENT_RAFFLE_REQUEST_CODE) { _, _ -> result = true }
        }

        // Then
        PaymentRafflePage(isEmbeddedInDialog = false).run {
            checkbox.setChecked(true)
            emailInput.replaceText(CovidDocumentSamples.ErikaMustermann().registrationData().email!!)
            primaryActionButton.view.perform(forceClick())
            title.doesNotExist()
            assertTrue(result)
        }
    }
}
