package de.culture4life.luca.testtools.pages.account

import de.culture4life.luca.R
import de.culture4life.luca.ui.compound.AccountPageActionItemSwitchView
import io.github.kakaocup.kakao.switch.KSwitch
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class PaymentAccountPage {

    val title = KTextView { withId(R.id.actionBarTitleTextView) }
    val revocationCodeButton = KButton { withId(R.id.revocationCodeButton) }
    val toggle = KSwitch {
        withId(R.id.activationToggle)
        isInstanceOf(AccountPageActionItemSwitchView::class.java)
    }

    fun isDisplayed() {
        title.run {
            isDisplayed()
            hasText(R.string.pay_account_title)
        }
    }
}
