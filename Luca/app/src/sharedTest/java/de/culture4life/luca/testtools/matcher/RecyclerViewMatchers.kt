package de.culture4life.luca.testtools.matcher

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

class RecyclerViewMatchers {

    companion object {
        @Suppress("UNCHECKED_CAST")
        fun canScrollUp(): Matcher<View> = CanScrollUpMatcher() as Matcher<View>
    }

    class CanScrollUpMatcher() : TypeSafeMatcher<RecyclerView>() {
        override fun matchesSafely(view: RecyclerView): Boolean {
            return view.canScrollVertically(-1)
        }

        override fun describeTo(description: Description) {
            description.appendText("can scroll up")
        }
    }
}
