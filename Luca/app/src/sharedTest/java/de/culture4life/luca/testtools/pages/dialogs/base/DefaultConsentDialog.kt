package de.culture4life.luca.testtools.pages.dialogs.base

import androidx.annotation.StringRes
import androidx.core.text.HtmlCompat
import androidx.test.platform.app.InstrumentationRegistry
import de.culture4life.luca.R
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

abstract class DefaultConsentDialog : DefaultDialog {

    val title = KTextView { withId(R.id.consentHeaderTextView) }
    val info = KTextView { withId(R.id.consentInfoTextView) }
    val acceptButton = KButton { withId(R.id.acceptButton) }
    val cancelButton = KButton { withId(R.id.cancelButton) }

    protected fun getStringFromHtml(
        @StringRes
        resourceId: Int
    ): String {
        return HtmlCompat.fromHtml(
            InstrumentationRegistry.getInstrumentation().targetContext.getString(resourceId),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        ).toString()
    }
}
