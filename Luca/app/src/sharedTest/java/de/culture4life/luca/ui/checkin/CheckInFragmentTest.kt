package de.culture4life.luca.ui.checkin

import android.os.Bundle
import de.culture4life.luca.R
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.actions.scrollToAndPerform
import de.culture4life.luca.testtools.mocks.IntentMocks
import de.culture4life.luca.testtools.pages.CheckInPage
import de.culture4life.luca.testtools.preconditions.ContactTracingPreconditions
import de.culture4life.luca.testtools.preconditions.DailyKeyPreconditions
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.ui.BaseQrCodeViewModel
import de.culture4life.luca.ui.registration.RegistrationActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CheckInFragmentTest : LucaFragmentTest<CheckInFragment>(LucaFragmentScenarioRule.create()) {

    @get:Rule
    val fixedTimeRule = FixedTimeRule()

    @Before
    fun setup() {
        setupDefaultWebServerMockResponses()
        getInitializedManager(application.consentManager)
        getInitializedManager(application.notificationManager)
    }

    private fun launchScenario(bundleBlock: Bundle.() -> Unit = {}) {
        DailyKeyPreconditions().givenStoredDailyKey()
        launchScenario(R.id.checkInFragment, bundleBlock)
        initializeUiExtensions()
    }

    @Test
    fun showQrCodeWithMissingContactDetails() {
        ContactTracingPreconditions().givenContactTracingEnabled()
        launchScenario()
        CheckInPage().run {
            showQrCodeButton.scrollToAndPerform { click() }
            IntentMocks.givenActivityNavigationResponseOk<RegistrationActivity>()
            contactInfoMissingDialog.okButton.click()
            IntentMocks.assertActivityNavigation<RegistrationActivity>()
        }
    }

    @Test
    fun createPrivateMeetingWithMissingContactDetails() {
        ContactTracingPreconditions().givenContactTracingEnabled()
        launchScenario()
        CheckInPage().run {
            createPrivateMeetingButton.scrollToAndPerform { click() }
            IntentMocks.givenActivityNavigationResponseOk<RegistrationActivity>()
            contactInfoMissingDialog.okButton.click()
            IntentMocks.assertActivityNavigation<RegistrationActivity>()
        }
    }

    @Test
    fun handleCheckInDeeplinkAndShowCheckIn() {
        val location = LocationSamples.Restaurant()
        mockServerPreconditions.givenLocation(location)
        launchScenario { putString(BaseQrCodeViewModel.BARCODE_DATA_KEY, location.qrCodeContent) }
        CheckInPage().run { isDisplayed() }
    }

    private fun setupDefaultWebServerMockResponses() {
        mockServerPreconditions.givenTimeSync()
        mockServerPreconditions.givenSupportedVersion()
    }
}
