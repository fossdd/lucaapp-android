package de.culture4life.luca.testtools.pages

import de.culture4life.luca.R
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.testtools.pages.dialogs.BirthdayNotMatchDialog
import de.culture4life.luca.testtools.pages.dialogs.CheckInNotSupportedDialog
import de.culture4life.luca.testtools.pages.dialogs.ConsentDialog
import de.culture4life.luca.testtools.pages.elements.QrCodeScannerElement
import io.github.kakaocup.kakao.text.KButton

class AddCertificateFlowPage {

    val scanQrCodeButton = KButton { withId(R.id.scanQrCodeButton) }.also { it.inRoot { isDialog() } }
    val qrCodeScannerPage = QrScannerPage()
    val successConfirmButton = KButton { withId(R.id.actionButton) }.also { it.inRoot { isDialog() } }

    class QrScannerPage {

        val consentsDialog = ConsentDialog(ConsentManager.ID_IMPORT_DOCUMENT)
        val checkInNotSupportedDialog = CheckInNotSupportedDialog()
        val birthdayNotMatchDialog = BirthdayNotMatchDialog()
        val cancelButton = KButton { withId(R.id.cancelButton) }
            .also { it.inRoot { isDialog() } }
        val scanner = QrCodeScannerElement()

        fun isDisplayed() {
            // TODO add some checks
        }
    }
}
