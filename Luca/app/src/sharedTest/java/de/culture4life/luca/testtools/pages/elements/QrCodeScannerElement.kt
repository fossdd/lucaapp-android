package de.culture4life.luca.testtools.pages.elements

import de.culture4life.luca.testtools.util.FragmentFinder
import de.culture4life.luca.ui.BaseQrCodeFragment
import timber.log.Timber

class QrCodeScannerElement {
    fun scan(qrCodeContent: String) {
        val scannerFragment = FragmentFinder.find(BaseQrCodeFragment::class.java)
        scannerFragment.viewModel.processBarcode(qrCodeContent)
            .doOnError { Timber.w(it) }
            .onErrorComplete()
            .test()
    }
}
