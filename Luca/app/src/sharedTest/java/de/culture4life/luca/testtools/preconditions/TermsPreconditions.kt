package de.culture4life.luca.testtools.preconditions

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.ui.terms.UpdatedTermsUtil

class TermsPreconditions {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()

    fun givenTermsAccepted() {
        UpdatedTermsUtil.markTermsAsAccepted(application).blockingAwait()
    }
}
