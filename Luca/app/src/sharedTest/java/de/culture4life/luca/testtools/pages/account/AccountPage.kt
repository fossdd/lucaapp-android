package de.culture4life.luca.testtools.pages.account

import de.culture4life.luca.R
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.text.KTextView

class AccountPage {

    val title = KTextView {
        isDescendantOfA { withId(R.id.accountRootLayout) }
        withId(R.id.actionBarTitleTextView)
    }
    val lucaPayItem = KView { withId(R.id.lucaPayItem) }
    val messagesItem = KView { withId(R.id.messagesItem) }
    val postalCodeItem = KView { withId(R.id.postalCodeItem) }
    val lucaConnectItem = KView { withId(R.id.lucaConnectItem) }

    fun isDisplayed() {
        title.run {
            isDisplayed()
            hasText(R.string.navigation_account)
        }
    }
}
