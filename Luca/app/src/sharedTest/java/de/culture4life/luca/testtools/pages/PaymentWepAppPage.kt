package de.culture4life.luca.testtools.pages

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction
import io.github.kakaocup.kakao.progress.KProgressBar

class PaymentWepAppPage(isEmbeddedInDialog: Boolean = true) {

    val progressBar = KProgressBar { withId(R.id.payWebAppProgressBar) }

    init {
        SafeDialogInteraction.apply(this, isEmbeddedInDialog)
    }

    fun isDisplayed() {
        progressBar.isDisplayed()
    }
}
