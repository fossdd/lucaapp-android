package de.culture4life.luca.ui.payment

import de.culture4life.luca.R
import de.culture4life.luca.payment.SignInData
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.pages.PaymentPage
import de.culture4life.luca.testtools.preconditions.ConsentPreconditions
import de.culture4life.luca.testtools.preconditions.MockServerPreconditions
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.testtools.samples.LucaPaySamples
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.net.HttpURLConnection

class PaymentFragmentTest : LucaFragmentTest<PaymentFragment>(LucaFragmentScenarioRule.create()) {

    @get:Rule
    val fixedTimeRule = FixedTimeRule()

    private val consentPreconditions = ConsentPreconditions()

    @Before
    fun setup() {
        setupDefaultWebServerMockResponses()
        getInitializedManager(application.registrationManager)
        getInitializedManager(application.networkManager)
        getInitializedManager(application.cryptoManager)
    }

    private fun launchScenario() {
        launchScenario(R.id.paymentFragment)
    }

    @Test
    fun showPaymentIntroWithMissingSignInData() {
        consentPreconditions.givenConsentLucaPay(true)
        launchScenario()
        PaymentPage().run {
            paymentIntroActionButton.isDisplayed()
        }
    }

    @Test
    fun showPaymentHistoryWithSignInData() {
        consentPreconditions.givenConsentLucaPay(true)
        givenSignInData()
        launchScenario()
        PaymentPage().run {
            paymentIntroActionButton.isGone()
        }
    }

    @Test
    fun showPaymentHistoryAfterSigningInWithSuccess() {
        consentPreconditions.givenConsentLucaPay(true)
        mockServerPreconditions.givenPaymentSignUp()
        mockServerPreconditions.givenPaymentSignIn()
        launchScenario()
        PaymentPage().run {
            paymentIntroActionButton.isDisplayed()
            paymentIntroActionButton.click()
            paymentIntroActionButton.isGone()
        }
    }

    @Test
    fun showErrorWhenSignUpFailed() {
        consentPreconditions.givenConsentLucaPay(true)
        mockServerPreconditions.givenHttpError(MockServerPreconditions.Route.PaymentSignUp, HttpURLConnection.HTTP_NOT_FOUND)
        launchScenario()
        PaymentPage().run {
            paymentIntroActionButton.isDisplayed()
            paymentIntroActionButton.click()
            requestErrorDialog.isDisplayed()
        }
    }

    @Test
    fun navigateToHistoryWhenSignedUp() {
        consentPreconditions.givenConsentLucaPay(true)
        mockServerPreconditions.givenPaymentSignUp()
        mockServerPreconditions.givenPaymentSignIn()
        launchScenario()
        PaymentPage().run {
            paymentIntroActionButton.isDisplayed()
            paymentIntroActionButton.click()
            paymentIntroActionButton.isGone()
            assertNavigationDestination(R.id.paymentHistoryFragment)
        }
    }

    private fun givenSignInData() {
        val paymentManager = application.getInitializedManager(application.paymentManager).blockingGet()
        val signIn = LucaPaySamples.SignIn()
        paymentManager.persistSignInData(
            SignInData(
                accessToken = signIn.accessToken,
                expirationTimestamp = signIn.expirationTimestamp
            )
        ).blockingAwait()
    }

    private fun setupDefaultWebServerMockResponses() {
        with(mockServerPreconditions) {
            givenTimeSync()
            givenSupportedVersion()
        }
    }
}
