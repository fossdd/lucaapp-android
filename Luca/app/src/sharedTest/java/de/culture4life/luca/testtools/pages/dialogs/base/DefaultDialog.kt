package de.culture4life.luca.testtools.pages.dialogs.base

interface DefaultDialog {

    fun isDisplayed()
}
