package de.culture4life.luca.ui.account.lucapay

import de.culture4life.luca.R
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.pages.account.PaymentAccountPage
import de.culture4life.luca.testtools.pages.dialogs.DisablePaymentDialog
import de.culture4life.luca.testtools.preconditions.ConsentPreconditions
import de.culture4life.luca.testtools.preconditions.PaymentPreconditions
import de.culture4life.luca.testtools.preconditions.RegistrationPreconditions
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import org.junit.Test

class LucaPayPreferencesFragmentTest : LucaFragmentTest<LucaPayPreferencesFragment>(LucaFragmentScenarioRule.create()) {

    private fun givenPaymentEnabled() {
        mockServerPreconditions.run {
            givenPaymentSignUp()
            givenPaymentSignIn()
        }
        ConsentPreconditions().givenConsentLucaPay(true)
        RegistrationPreconditions().givenRegisteredUser()
        PaymentPreconditions().givenSignedUpAndSignedIn()
    }

    @Test
    fun alertShownWhenDisablingLucaPay() {
        givenPaymentEnabled()
        launchScenario(R.id.lucaPayFragment)
        PaymentAccountPage().run {
            toggle.isChecked()
            toggle.click()
            DisablePaymentDialog().isDisplayed()
        }
    }
}
