package de.culture4life.luca.testtools

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildFragment
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowViewModel
import org.junit.Rule

abstract class LucaFlowFragmentTest<FRAGMENT : BaseFlowChildFragment<*, *>, VM : BaseFlowViewModel>(
    private val fragmentClass: Class<FRAGMENT>,
    private val sharedViewModelClass: Class<VM>,
    @get:Rule
    val fragmentScenarioRule: LucaFragmentScenarioRule<DummySharedViewModelScopeProvider> = LucaFragmentScenarioRule.create()
) : LucaScenarioTest() {

    lateinit var sharedViewModel: VM

    fun launchScenario(arguments: Bundle? = null) {
        fragmentScenarioRule.launch(
            onCreated = { container ->
                container.createActualChildFragment(fragmentClass)
                sharedViewModel = ViewModelProvider(container.sharedViewModelStoreOwner).get(sharedViewModelClass)
                if (arguments != null) {
                    sharedViewModel.processArguments(arguments).blockingAwait()
                }
            }
        )
    }

    /**
     * close and create new fragment state
     */
    fun restartScenario(arguments: Bundle? = null) {
        fragmentScenarioRule.cleanUpScenario()
        launchScenario(arguments)
    }

    /**
     * close and restore previous fragment state
     */
    fun recreateScenario() {
        fragmentScenarioRule.scenario.recreate()
        waitForIdle()
    }
}
