package de.culture4life.luca.testtools.rules

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.runner.intent.IntentCallback
import androidx.test.runner.intent.IntentMonitorRegistry
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.testtools.FixRobolectricIdlingResource
import java.lang.reflect.Field

class LucaActivityScenarioRule<ACTIVITY : AppCompatActivity> @Deprecated("Use LucaActivityScenarioRule.create() instead.") constructor(
    private val startActivityClass: Class<ACTIVITY>,
    private val supportMultipleActivity: Boolean
) : BaseHookingTestRule() {

    private var _scenario: ActivityScenario<AppCompatActivity>? = null
    val scenario: ActivityScenario<AppCompatActivity>
        get() = _scenario!!

    private val launchNextActivityScenarioCallback = IntentCallback {
        cleanUpScenario()
        _scenario = ActivityScenario.launch(it)
        waitForIdle()
    }

    override fun beforeTest() {
        if (supportMultipleActivity && LucaApplication.isRunningUnitTests()) {
            // Support to launch multiple activities.
            //  Usually robolectric does not support to launch multiple activities. But instrumentations tests do support it.
            //  With this workaround we get multiple activities support for robolectric.
            IntentMonitorRegistry.getInstance().addIntentCallback(launchNextActivityScenarioCallback)
        }
    }

    override fun afterTest() {
        IntentMonitorRegistry.getInstance().removeIntentCallback(launchNextActivityScenarioCallback)
        cleanUpScenario()
        releaseAndroidViewModels()
    }

    fun launch(bundle: Bundle? = null) {
        launch { ActivityScenario.launch(startActivityClass, bundle) }
    }

    /**
     * Start app with given data.
     *
     * Does not use the [startActivityClass]. First activity is evaluated from AndroidManifest.
     */
    fun launchDeepLink(data: Uri) {
        val intent = Intent(Intent.ACTION_VIEW, data).apply {
            // avoid app selection on device
            setPackage(ApplicationProvider.getApplicationContext<LucaApplication>().packageName)
        }

        launch { ActivityScenario.launch<AppCompatActivity>(intent) }
    }

    private fun launch(launcher: () -> ActivityScenario<*>) {
        val startScenario = launcher()
        waitForIdle()
        if (_scenario == null) {
            @Suppress("UNCHECKED_CAST")
            _scenario = startScenario as ActivityScenario<AppCompatActivity>
        } else {
            // usually means the launch process did start next activity immediately
            startScenario.close()
        }
    }

    private fun waitForIdle() {
        FixRobolectricIdlingResource.waitForIdle()
    }

    private fun cleanUpScenario() {
        _scenario?.moveToState(Lifecycle.State.DESTROYED)
        _scenario?.close()
    }

    private fun releaseAndroidViewModels() {
        // ViewModelFactory keeps static reference to ViewModels.
        // https://github.com/robolectric/robolectric/issues/6251
        // Next tests would try to reuse the previous ViewModel which will be instantiated with the previous LucaApplication.
        val instance: Field = ViewModelProvider.AndroidViewModelFactory::class.java.getDeclaredField("sInstance")
        instance.isAccessible = true
        instance.set(null, null)
    }

    companion object {
        inline fun <reified ACTIVITY : AppCompatActivity> create(): LucaActivityScenarioRule<ACTIVITY> {
            @Suppress("DEPRECATION")
            return LucaActivityScenarioRule(ACTIVITY::class.java, true /* make it configurable when needed */)
        }
    }
}
