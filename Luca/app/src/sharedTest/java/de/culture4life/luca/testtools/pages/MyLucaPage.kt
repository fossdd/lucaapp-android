package de.culture4life.luca.testtools.pages

import android.view.View
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import de.culture4life.luca.R
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.testtools.actions.scrollToAndPerform
import de.culture4life.luca.testtools.matcher.CardViewMatchers
import de.culture4life.luca.testtools.pages.dialogs.*
import de.culture4life.luca.testtools.samples.ScannableDocument
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.image.KImageView
import io.github.kakaocup.kakao.recycler.KRecyclerItem
import io.github.kakaocup.kakao.recycler.KRecyclerView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import org.hamcrest.Matcher

class MyLucaPage {

    val title = KTextView { withId(R.id.actionBarTitleTextView) }
    val addDocumentButton = KButton { withId(R.id.primaryActionButton) }
    val addCertificateFlowPage = AddCertificateFlowPage()
    val addIdentityFlow = IdNowEnrollFlowPage()
    val documentList = KRecyclerView(
        { withId(R.id.myLucaRecyclerView) },
        {
            itemType(MyLucaPage::DocumentItem)
            itemType(MyLucaPage::IdentityItem)
            itemType(MyLucaPage::IdentityRequestQueuedItem)
            itemType(MyLucaPage::IdentityRequestedItem)
            itemType(MyLucaPage::CreateIdentityItem)
            itemType(MyLucaPage::NoDocumentsItem)
            itemType(MyLucaPage::EventItem)
        }
    )
    val updateDialog = UpdateDialog()
    val consentsDialog = ConsentDialog(ConsentManager.ID_IMPORT_DOCUMENT)
    val authenticationDialog = UserAuthenticationRequiredLucaIdIdentDialog()
    val authenticationNotActivatedDialog = UserAuthenticationNotActivatedDialog()
    val birthdayNotMatchDialog = BirthdayNotMatchDialog()
    val deleteEnrollmentDialog = DeleteEnrollmentDialog()
    val deleteDocumentDialog = DeleteDocumentDialog()

    fun isDisplayed() {
        with(title) {
            hasText(R.string.my_luca_heading)
            isDisplayed()
        }
    }

    fun navigateToScanner() {
        addDocumentButton.click()
        addCertificateFlowPage.run {
            scanQrCodeButton.scrollToAndPerform { click() }
        }
    }

    fun stepsScanValidDocument(document: ScannableDocument) {
        navigateToScanner()
        addCertificateFlowPage.run {
            qrCodeScannerPage.run {
                scanner.scan(document.qrCodeContent)
                consentsDialog.acceptButton.click()
            }
            successConfirmButton.scrollToAndPerform { click() }
        }
    }

    class EventItem(parent: Matcher<View>) : KRecyclerItem<Any>(parent) {
        val title = KTextView(parent) { withId(R.id.itemTitleTextView) }
        val icon = KImageView(parent) { withId(R.id.icon) }
        val expandedContent = KView(parent) { withId(R.id.collapseLayout) }

        fun assertIsExpectedItemType() {
            view.check(matches(withId(R.id.eventTicketCardView)))
            title.isDisplayed()
            icon.isDisplayed()
        }

        fun assertIsCollapsed() {
            expandedContent.isNotDisplayed()
        }

        fun assertIsExpanded() {
            expandedContent.isDisplayed()
        }
    }

    class DocumentItem(parent: Matcher<View>) : KRecyclerItem<Any>(parent) {
        val title = KTextView(parent) { withId(R.id.itemTitleTextView) }
        val collapseIndicator = KImageView(parent) { withId(R.id.collapseIndicator) }
        val expandedContent = KView(parent) { withId(R.id.collapseLayout) }
        val deleteButton = KView(parent) { withId(R.id.deleteItemButton) }

        fun assertIsExpectedItemType() {
            title.isDisplayed()
            collapseIndicator.isDisplayed()
        }

        fun assertIsCollapsed() {
            expandedContent.isNotDisplayed()
        }

        fun assertIsExpanded() {
            expandedContent.isDisplayed()
        }

        fun assertIsMarkedAsValid() {
            view.check(matches(CardViewMatchers.hasBackgroundColor(R.color.document_outcome_fully_vaccinated)))
        }

        fun assertIsMarkedAsPartially() {
            view.check(matches(CardViewMatchers.hasBackgroundColor(R.color.document_outcome_partially_vaccinated)))
        }

        fun assertIsMarkedAsPositive() {
            view.check(matches(CardViewMatchers.hasBackgroundColor(R.color.document_outcome_positive)))
        }

        fun assertIsMarkedAsNegative() {
            view.check(matches(CardViewMatchers.hasBackgroundColor(R.color.document_outcome_negative)))
        }

        fun assertIsMarkedAsExpired() {
            view.check(matches(CardViewMatchers.hasBackgroundColor(R.color.document_outcome_expired)))
        }
    }

    class CreateIdentityItem(parent: Matcher<View>) : KRecyclerItem<Any>(parent) {
        val title = KTextView(parent) { withId(R.id.titleTextView) }
        val description = KTextView(parent) { withId(R.id.descriptionTextView) }
        fun assertIsExpectedItemType() {
            title.hasText(R.string.luca_id_empty_item_title)
            description.hasText(R.string.luca_id_empty_item_description)
        }
    }

    fun KRecyclerView.assertIsCreateLucaIdItemOnPosition(position: Int) {
        childAt<CreateIdentityItem>(position) { assertIsExpectedItemType() }
    }

    fun KRecyclerView.assertIsIdentityRequestQueuedItemOnPosition(position: Int) {
        childAt<IdentityRequestQueuedItem>(position) { assertIsExpectedItemType() }
    }

    class IdentityRequestQueuedItem(parent: Matcher<View>) : KRecyclerItem<Any>(parent) {
        val title = KTextView(parent) { withId(R.id.titleTextView) }
        val description = KTextView(parent) { withId(R.id.descriptionTextView) }
        fun assertIsExpectedItemType() {
            title.hasText(R.string.luca_id_request_queued_item_title)
            description.hasText(R.string.luca_id_request_queued_item_description)
        }
    }

    class IdentityRequestedItem(parent: Matcher<View>) : KRecyclerItem<Any>(parent) {
        val title = KTextView(parent) { withId(R.id.titleTextView) }
        val description = KTextView(parent) { withId(R.id.descriptionTextView) }
        val token = KTextView(parent) { withId(R.id.tokenTextView) }
        fun assertIsExpectedItemType() {
            title.hasText(R.string.luca_id_requested_item_title)
            description.hasText(R.string.luca_id_requested_item_description)
            token.hasAnyText()
        }
    }

    fun KRecyclerView.assertIsIdentityRequestedItemOnPosition(position: Int) {
        childAt<IdentityRequestedItem>(position) { assertIsExpectedItemType() }
    }

    class IdentityItem(parent: Matcher<View>) : KRecyclerItem<Any>(parent) {
        val name = KTextView(parent) { withId(R.id.name) }
        fun assertIsExpectedItemType() {
            name.hasText(R.string.luca_id_card_name_blurry_placeholder)
        }
    }

    fun KRecyclerView.assertIsIdentityItemOnPosition(position: Int) {
        childAt<IdentityItem>(position) { assertIsExpectedItemType() }
    }

    class NoDocumentsItem(parent: Matcher<View>) : KRecyclerItem<Any>(parent) {
        val title = KTextView(parent) { withId(R.id.emptyTitleTextView) }
        fun assertIsExpectedItemType() {
            title.hasText(R.string.my_luca_empty_title)
        }
    }
}
