package de.culture4life.luca.ui.onboarding

import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import androidx.activity.viewModels
import de.culture4life.luca.databinding.FragmentOnboardingWelcomeBinding
import de.culture4life.luca.ui.BaseActivity
import de.culture4life.luca.ui.registration.RegistrationActivity
import de.culture4life.luca.util.ViewRequiredUtil.showCheckBoxRequiredError

class OnboardingActivity : BaseActivity() {

    private val viewModel: OnboardingViewModel by viewModels()
    private lateinit var welcomeBinding: FragmentOnboardingWelcomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        welcomeBinding = FragmentOnboardingWelcomeBinding.inflate(layoutInflater)
        showWelcomeScreen()
        hideActionBar()
    }

    private fun showWelcomeScreen() {
        setContentView(welcomeBinding.root)
        viewModel.termsCheckBoxErrorLiveData.observe(this) {
            if (it.isNotHandled) {
                it.isHandled = true
                showCheckBoxRequiredError(welcomeBinding.termsCheckBox, welcomeBinding.termsTextView)
            }
        }
        viewModel.privacyPolicyCheckBoxErrorLiveData.observe(this) {
            if (it.isNotHandled) {
                it.isHandled = true
                showCheckBoxRequiredError(welcomeBinding.privacyPolicyCheckBox, welcomeBinding.privacyPolicyTextView)
            }
        }
        viewModel.showRegistrationScreenLiveData.observe(this) {
            if (it.isNotHandled) {
                it.isHandled = true
                showRegistration()
            }
        }

        welcomeBinding.termsTextView.movementMethod = LinkMovementMethod.getInstance()
        welcomeBinding.privacyPolicyTextView.movementMethod = LinkMovementMethod.getInstance()

        welcomeBinding.primaryActionButton.setOnClickListener {
            viewModel.onWelcomeActionButtonClicked(welcomeBinding.termsCheckBox.isChecked, welcomeBinding.privacyPolicyCheckBox.isChecked)
        }
    }

    private fun showRegistration() {
        val intent = Intent(this, RegistrationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    companion object {
        const val WELCOME_SCREEN_SEEN_KEY = "welcome_screen_seen"
    }
}
