package de.culture4life.luca.payment

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Data of the consumer used for the payment sign up containing randomly generated [username],
 * [name] and [password].
 *
 * @see [Sign up consumer](https://app.luca-app.de/pay/api/v1/swagger/#/Consumers/post_consumers_signup)
 */
data class ConsumerData(

    @Expose
    @SerializedName("username")
    val username: String,

    @Expose
    @SerializedName("name")
    val name: String,

    @Expose
    @SerializedName("password")
    val password: String
)
