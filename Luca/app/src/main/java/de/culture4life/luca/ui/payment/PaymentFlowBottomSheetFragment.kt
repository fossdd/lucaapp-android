package de.culture4life.luca.ui.payment

import android.content.DialogInterface
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import de.culture4life.luca.payment.PaymentLocationData
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowBottomSheetDialogFragment
import de.culture4life.luca.ui.payment.children.*

class PaymentFlowBottomSheetFragment : BaseFlowBottomSheetDialogFragment<PaymentFlowPage, PaymentFlowViewModel>() {

    override fun lastPageHasBackButton() = false

    override fun getViewModelClass() = PaymentFlowViewModel::class.java

    override fun mapPageToFragment(page: PaymentFlowPage) = when (page) {
        is PaymentFlowPage.AmountPage -> PaymentAmountFragment.newInstance()
        is PaymentFlowPage.PartialAmountPage -> PaymentPartialAmountFragment.newInstance()
        is PaymentFlowPage.PaymentTipPage -> PaymentTipFragment.newInstance()
        is PaymentFlowPage.WebAppPage -> PaymentWebAppFragment.newInstance()
        is PaymentFlowPage.PaymentResultPage -> PaymentResultFragment.newInstance()
    }

    override val sharedViewModelStoreOwner = this

    override fun initializeViews() {
        super.initializeViews()

        viewModel.paymentResult.observe(viewLifecycleOwner) { paymentData ->
            setFragmentResult(
                NEW_PAYMENT_REQUEST_KEY,
                bundleOf(Pair(NEW_PAYMENT_RESULT_KEY, paymentData.isSuccessful))
            )
        }

        observe(viewModel.activatePartialPaymentPage) { isEnabled ->
            if (isEnabled) {
                pagerAdapter.addPageAfter(PaymentFlowPage.AmountPage, PaymentFlowPage.PartialAmountPage)
            } else {
                pagerAdapter.removePage(PaymentFlowPage.PartialAmountPage)
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

        // If ViewModel is dismissed before reaching the end, the result listeners are notified as unsuccessful
        if (viewModel.paymentResult.value == null) {
            setFragmentResult(NEW_PAYMENT_REQUEST_KEY, bundleOf(Pair(NEW_PAYMENT_RESULT_KEY, false)))
        }
    }

    companion object {
        const val TAG = "NewPaymentFlowBottomSheetFragment"
        const val NEW_PAYMENT_REQUEST_KEY = "newPaymentRequest"
        const val NEW_PAYMENT_RESULT_KEY = "newPaymentResult"

        fun newInstance(paymentData: PaymentLocationData): PaymentFlowBottomSheetFragment {
            return PaymentFlowBottomSheetFragment().also {
                it.arguments = bundleOf(PaymentFlowViewModel.ARGUMENT_PAYMENT_LOCATION_DATA_KEY to paymentData)
            }
        }
    }
}
