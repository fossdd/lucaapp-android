package de.culture4life.luca.payment

import de.culture4life.luca.network.pojo.payment.CheckoutResponseData
import de.culture4life.luca.network.pojo.payment.ConsumerPaymentsResponseData
import de.culture4life.luca.util.AmountUtil
import de.culture4life.luca.util.fromUnixTimestamp
import de.culture4life.luca.util.toCurrencyAmountNumber
import java.io.Serializable

data class PaymentData(
    val id: String,
    val locationName: String,
    val locationId: String,
    val invoiceAmount: Int, // in cents
    val originalInvoiceAmount: Int = invoiceAmount, // in cents
    val discountAmount: Int = 0, // in cents
    val tipAmount: Int = 0, // in cents
    val totalAmount: Int, // in cents
    val timestamp: Long,
    val table: String? = null,
    val status: Status,
    val refunded: Boolean = false,
    val refundedAmount: Int = 0, // in cents
    val paymentVerifier: String
) : Serializable {

    val isSuccessful: Boolean = status == Status.CLOSED && !refunded
    val isDiscounted: Boolean = discountAmount > 0 && originalInvoiceAmount > invoiceAmount
    val tipPercent: Int
        get() = AmountUtil.calculateTipInPercent(originalInvoiceAmount, tipAmount)

    enum class Status(val value: String) {
        CLOSED("CLOSED"), ERROR("ERROR"), UNKNOWN("UNKNOWN")
    }

    companion object {

        fun fromPayment(payment: ConsumerPaymentsResponseData.Payment) = PaymentData(
            id = payment.id,
            locationName = payment.locationName,
            locationId = payment.locationId,
            originalInvoiceAmount = payment.discountAmounts?.originalInvoiceAmount?.toCurrencyAmountNumber()
                ?: payment.invoiceAmount.toCurrencyAmountNumber(),
            discountAmount = payment.discountAmounts?.discountAmount?.toCurrencyAmountNumber() ?: 0,
            invoiceAmount = payment.invoiceAmount.toCurrencyAmountNumber(),
            tipAmount = payment.tipAmount.toCurrencyAmountNumber(),
            totalAmount = payment.totalAmount.toCurrencyAmountNumber(),
            timestamp = payment.timestamp.fromUnixTimestamp(),
            table = payment.table,
            status = Status.valueOf(payment.status.value),
            refunded = payment.refunded,
            refundedAmount = payment.refundedAmount.toCurrencyAmountNumber(),
            paymentVerifier = payment.paymentVerifier
        )

        fun fromPayment(payment: CheckoutResponseData.Payment, locationId: String) = PaymentData(
            id = payment.id ?: "",
            locationName = payment.locationName,
            locationId = locationId,
            originalInvoiceAmount = payment.discountAmounts?.originalInvoiceAmount?.toCurrencyAmountNumber()
                ?: payment.invoiceAmount?.toCurrencyAmountNumber() ?: 0,
            discountAmount = payment.discountAmounts?.discountAmount?.toCurrencyAmountNumber() ?: 0,
            invoiceAmount = payment.invoiceAmount?.toCurrencyAmountNumber() ?: 0,
            tipAmount = payment.tipAmount.toCurrencyAmountNumber(),
            totalAmount = payment.totalAmount.toCurrencyAmountNumber(),
            timestamp = payment.timestamp.fromUnixTimestamp(),
            table = payment.table,
            status = Status.valueOf(payment.status.value),
            paymentVerifier = payment.paymentVerifier ?: ""
        )
    }
}
