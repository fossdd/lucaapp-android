package de.culture4life.luca.util

import android.os.Build
import android.text.Html
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.annotation.StringRes

fun TextView.setHtmlStringResource(
    @StringRes
    res: Int
) {
    movementMethod = LinkMovementMethod.getInstance()
    text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(context.getString(res), Html.FROM_HTML_MODE_COMPACT)
    } else {
        Html.fromHtml(context.getString(res))
    }
}
