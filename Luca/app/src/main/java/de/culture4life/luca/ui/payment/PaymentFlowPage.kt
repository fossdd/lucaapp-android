package de.culture4life.luca.ui.payment

import android.os.Bundle
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowPage

sealed class PaymentFlowPage(override val id: Long, override val arguments: Bundle? = null) : BaseFlowPage {
    object AmountPage : PaymentFlowPage(0)
    object PartialAmountPage : PaymentFlowPage(1)
    object PaymentTipPage : PaymentFlowPage(2)
    object WebAppPage : PaymentFlowPage(3)
    object PaymentResultPage : PaymentFlowPage(4)
}
