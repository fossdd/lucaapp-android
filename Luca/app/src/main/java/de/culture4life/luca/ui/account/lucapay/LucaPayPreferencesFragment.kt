package de.culture4life.luca.ui.account.lucapay

import androidx.core.view.isVisible
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.culture4life.luca.R
import de.culture4life.luca.databinding.FragmentLucaPayPreferencesBinding
import de.culture4life.luca.ui.BaseFragment
import de.culture4life.luca.util.setCheckedImmediately

class LucaPayPreferencesFragment : BaseFragment<LucaPayPreferencesViewModel>() {
    private lateinit var binding: FragmentLucaPayPreferencesBinding

    override fun getViewBinding() = FragmentLucaPayPreferencesBinding.inflate(layoutInflater).also { binding = it }

    override fun getViewModelClass() = LucaPayPreferencesViewModel::class.java

    override fun initializeViews() {
        super.initializeViews()
        initializeClickListeners()
        initializeObservers()
    }

    private fun showRevocationCode() {
        safeNavigateFromNavController(R.id.action_lucaPayFragment_to_paymentRevocationCodeFragment)
    }

    private fun initializeClickListeners() {
        binding.activationToggle.setOnClickListener {
            if (binding.activationToggle.isChecked) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(R.string.pay_delete_alert_title)
                    .setMessage(R.string.pay_delete_alert_text)
                    .setPositiveButton(R.string.action_ok) { _, _ -> viewModel.onSwitchToggled(false) }
                    .setNegativeButton(R.string.action_cancel) { _, _ -> }
                    .show()
            } else {
                viewModel.onSwitchToggled(true)
            }
        }
        binding.revocationCodeButton.setOnClickListener { showRevocationCode() }
    }

    private fun initializeObservers() {
        showRevocationCodeViews(viewModel.lucaPayStatus.value == true)
        binding.activationToggle.setCheckedImmediately(viewModel.lucaPayStatus.value)

        observe(viewModel.isLoading) {
            binding.loadingIndicator.isVisible = it
            binding.activationToggle.isEnabled = !it
        }

        observe(viewModel.lucaPayStatus) {
            binding.activationToggle.isChecked = it
            showRevocationCodeViews(it)
        }
    }

    private fun showRevocationCodeViews(payEnabled: Boolean) {
        binding.revocationCodeButton.isVisible = payEnabled
        binding.revocationCodeDescriptionTextView.isVisible = payEnabled
    }
}
