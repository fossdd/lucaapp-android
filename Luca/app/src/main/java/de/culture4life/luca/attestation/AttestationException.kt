package de.culture4life.luca.attestation

open class AttestationException(message: String, cause: Throwable) : Exception(message, cause)
