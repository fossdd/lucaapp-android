package de.culture4life.luca.network.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import de.culture4life.luca.registration.RegistrationData

/**
 * Contact data entered during initial registration (see [RegistrationData]), will be encrypted before leaving the device.
 *
 * Example:
 * <pre>
 * {
 *   "v": 3, // version
 *   "fn": "Donald", // firstName
 *   "ln": "Duck", // lastName
 *   "pn": "123 456 789", // phoneNumber (E.164/FQTN)
 *   "e": "donald.duck@entenhausen.de", // email
 *   "st": "Quackstraße", // street
 *   "hn": "42", // house number
 *   "pc": "12345", // postal code
 *   "c": "Entenhausen", // city
 * }
 * </pre>
 *
 * @see [Security Overview: Guest Registration](https://www.luca-app.de/securityoverview/processes/guest_registration.html)
 */
data class ContactData(
    @Expose
    @SerializedName("v")
    val version: Int = 3,

    @Expose
    @SerializedName("fn")
    val firstName: String? = null,

    @Expose
    @SerializedName("ln")
    val lastName: String? = null,

    @Expose
    @SerializedName("pn")
    val phoneNumber: String? = null,

    @Expose
    @SerializedName("e")
    val email: String? = null,

    @Expose
    @SerializedName("st")
    val street: String? = null,

    @Expose
    @SerializedName("hn")
    val houseNumber: String? = null,

    @Expose
    @SerializedName("c")
    val city: String? = null,

    @Expose
    @SerializedName("pc")
    val postalCode: String? = null

) {
    companion object {

        fun fromRegistrationData(registrationData: RegistrationData) = ContactData(
            firstName = registrationData.firstName,
            lastName = registrationData.lastName,
            phoneNumber = registrationData.phoneNumber,
            // avoid sending empty string, backend wants null if value is not provided
            email = if (registrationData.email.isNullOrEmpty()) null else registrationData.email,
            street = registrationData.street,
            houseNumber = registrationData.houseNumber,
            city = registrationData.city,
            postalCode = registrationData.postalCode
        )
    }
}
