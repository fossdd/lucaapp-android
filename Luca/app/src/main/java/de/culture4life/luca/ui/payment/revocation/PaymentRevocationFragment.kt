package de.culture4life.luca.ui.payment.revocation

import android.content.Intent
import android.content.Intent.EXTRA_STREAM
import android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION
import androidx.core.content.FileProvider
import de.culture4life.luca.R
import de.culture4life.luca.databinding.FragmentPaymentRevocationBinding
import de.culture4life.luca.ui.BaseFragment
import java.io.File
import java.io.OutputStreamWriter

class PaymentRevocationFragment : BaseFragment<PaymentRevocationViewModel>() {

    private lateinit var binding: FragmentPaymentRevocationBinding

    override fun getViewModelClass() = PaymentRevocationViewModel::class.java
    override fun getViewBinding() = FragmentPaymentRevocationBinding.inflate(layoutInflater).also { binding = it }

    override fun initializeViews() {
        super.initializeViews()

        observe(viewModel.revocationCode) { binding.copyTokenView.setToken(it) }

        binding.actionButton.setOnClickListener { exportCode() }
    }

    private fun exportCode() {
        val context = requireContext()

        // Create file
        val filePath = File(context.cacheDir, "revocation")
        filePath.mkdir()
        val textFile = File(filePath, "luca_pay_revocation_code.txt")

        // Write to file
        OutputStreamWriter(textFile.outputStream()).use {
            it.write(binding.copyTokenView.getToken())
        }

        // Share file
        val uri = FileProvider.getUriForFile(context, "${context.packageName}.fileprovider", textFile)
        val shareIntent = Intent.createChooser(
            Intent().apply {
                type = "text/*"
                action = Intent.ACTION_SEND
                putExtra(EXTRA_STREAM, uri)
                addFlags(FLAG_GRANT_READ_URI_PERMISSION)
            },
            context.getString(R.string.pay_revocation_notification_title)
        )
        startActivity(shareIntent)
    }
}
