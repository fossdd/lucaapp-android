package de.culture4life.luca.ui.base.bottomsheetflow

import androidx.lifecycle.ViewModelStoreOwner
import de.culture4life.luca.ui.BaseFragment
import de.culture4life.luca.ui.SharedViewModelScopeProvider
import io.reactivex.rxjava3.core.Completable

abstract class BaseFlowChildFragment<ChildViewModel : BaseFlowChildViewModel<ParentViewModel>, ParentViewModel : BaseFlowViewModel> :
    BaseFragment<ChildViewModel>() {

    abstract fun getSharedViewModelClass(): Class<ParentViewModel>

    override fun preInitializeViewModel(viewModel: ChildViewModel): Completable {
        return super.preInitializeViewModel(viewModel)
            .doOnComplete { viewModel.setupSharedViewModelReference(getSharedViewModelStoreOwner(), getSharedViewModelClass()) }
    }

    /**
     * Gets the [ViewModelStoreOwner] for the shared ViewModels from the parent which has to be a [SharedViewModelScopeProvider].
     */
    private fun getSharedViewModelStoreOwner(): ViewModelStoreOwner = (parentFragment as SharedViewModelScopeProvider).sharedViewModelStoreOwner
}
