package de.culture4life.luca.ui.payment.raffle

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import de.culture4life.luca.R
import de.culture4life.luca.ui.ViewError
import de.culture4life.luca.ui.ViewEvent
import de.culture4life.luca.ui.base.BaseBottomSheetViewModel
import de.culture4life.luca.ui.payment.raffle.PaymentRaffleBottomSheetFragment.Companion.ARGUMENT_PAYMENT_ID
import de.culture4life.luca.ui.registration.RegistrationViewModel
import io.reactivex.rxjava3.core.Completable

class PaymentRaffleViewModel(application: Application, savedStateHandle: SavedStateHandle) : BaseBottomSheetViewModel(application) {

    private lateinit var paymentId: String
    private val paymentManager = this.application.paymentManager
    val checkboxChecked: MutableLiveData<Boolean> = savedStateHandle.getLiveData(SAVED_STATE_CHECKBOX, false)
    val email: MutableLiveData<String> = savedStateHandle.getLiveData(SAVED_STATE_EMAIL, "")
    val success: MutableLiveData<ViewEvent<Unit>> = MutableLiveData()
    val confirmButtonEnabled: LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        addSource(isLoading) { value = validateInput() }
        addSource(checkboxChecked) { value = validateInput() }
        addSource(email) { value = validateInput() }
    }

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(paymentManager.initialize(application))
    }

    override fun processArguments(arguments: Bundle?): Completable {
        return super.processArguments(arguments)
            .andThen(Completable.fromAction { paymentId = arguments?.getString(ARGUMENT_PAYMENT_ID) ?: error("No payment ID provided!") })
    }

    private fun validateInput(): Boolean {
        return checkboxChecked.value == true && RegistrationViewModel.isValidEMailAddress(email.value.orEmpty().trim()) && isLoading.value != true
    }

    fun onConfirmButtonClicked() {
        invoke(
            paymentManager.submitRaffleCampaignParticipation(paymentId, email.value!!.trim())
                .doOnError {
                    addError(
                        ViewError.Builder(application)
                            .withCause(it)
                            .withResolveLabel(R.string.action_retry)
                            .withResolveAction(Completable.fromAction { onConfirmButtonClicked() })
                            .removeWhenShown()
                            .build()
                    )
                }
                .andThen(update(success, ViewEvent(Unit)))
                .doFinally { updateAsSideEffect(isLoading, false) }
                .doOnSubscribe { updateAsSideEffect(isLoading, true) }
        ).subscribe()
    }

    companion object {
        private const val SAVED_STATE_CHECKBOX = "saved_state_raffle_checkbox"
        private const val SAVED_STATE_EMAIL = "saved_state_raffle_email"
    }
}
