package de.culture4life.luca.history

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MeetingEndedItem(
    @SerializedName("guests")
    @Expose
    val guests: List<String> = ArrayList()
) : HistoryItem() {
    init {
        setType(TYPE_MEETING_ENDED)
    }
}
