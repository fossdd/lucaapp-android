package de.culture4life.luca.util

import de.culture4life.luca.network.NetworkManager.Companion.MAXIMUM_REQUEST_RETRY_COUNT
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.CompletableTransformer
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

object CompletableUtil {
    @JvmStatic
    @JvmOverloads
    fun retryWhenWithDelay(
        delay: Long,
        maximumRetries: Int = MAXIMUM_REQUEST_RETRY_COUNT,
        scheduler: Scheduler = Schedulers.io(),
        predicate: (Throwable) -> Boolean = { true }
    ): CompletableTransformer {
        return CompletableTransformer { completable ->
            completable.retryWhen { errors ->
                errors.zipWith(Flowable.range(0, maximumRetries + 1)) { errors, attempts ->
                    Pair(errors, attempts)
                }.flatMap { errorAndAttempt ->
                    if (predicate(errorAndAttempt.first) && errorAndAttempt.second < MAXIMUM_REQUEST_RETRY_COUNT) {
                        Flowable.just(errorAndAttempt.first).delay(delay, TimeUnit.MILLISECONDS, scheduler)
                    } else {
                        Flowable.error(errorAndAttempt.first)
                    }
                }
            }
        }
    }
}

fun Completable.retryWhenWithDelay(
    delay: Long,
    maximumRetries: Int = MAXIMUM_REQUEST_RETRY_COUNT,
    scheduler: Scheduler = Schedulers.io(),
    predicate: (Throwable) -> Boolean = { true }
): Completable {
    return this.compose(CompletableUtil.retryWhenWithDelay(delay, maximumRetries, scheduler, predicate))
}
