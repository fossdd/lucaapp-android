package de.culture4life.luca.ui.lucaconnect

import android.os.Bundle
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowPage

sealed class LucaConnectFlowPage(override val id: Long, override val arguments: Bundle? = null) : BaseFlowPage {
    object ExplanationPage : LucaConnectFlowPage(0)
    object ProvideProofPage : LucaConnectFlowPage(1)
    object LucaConnectSharedDataPage : LucaConnectFlowPage(2)
    object KritisPage : LucaConnectFlowPage(3)
    object LucaConnectConsentPage : LucaConnectFlowPage(4)
    object ConnectSuccessPage : LucaConnectFlowPage(5)
}
