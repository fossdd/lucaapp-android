package de.culture4life.luca.ui.myluca.listitems;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.zxing.EncodeHintType;

import net.glxn.qrgen.android.QRCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import de.culture4life.luca.R;
import de.culture4life.luca.document.CovidDocument;
import de.culture4life.luca.ui.myluca.DynamicContent;
import io.reactivex.rxjava3.core.Single;

public abstract class CovidDocumentItem extends MyLucaListItem implements Serializable, ExpandableListItem, DeletableDocumentListItem {

    @Nonnull
    protected final CovidDocument document;

    protected final List<DynamicContent> topContent = new ArrayList<>();
    protected final List<DynamicContent> collapsedContent = new ArrayList<>();

    @Nullable
    protected String sectionHeader;

    @Nullable
    protected String title;

    @Nullable
    protected String provider;

    @Nullable
    protected Bitmap barcode;

    @ColorInt
    protected int color;

    private boolean isExpanded;

    @DrawableRes
    protected int imageResource;

    public CovidDocumentItem(@Nonnull CovidDocument document) {
        this.document = document;
    }

    @Nonnull
    protected static Single<Bitmap> generateQrCode(@NonNull String data) {
        return Single.fromCallable(() -> QRCode.from(data)
                .withSize(500, 500)
                .withHint(EncodeHintType.MARGIN, 0)
                .bitmap());
    }

    @Nonnull
    protected static String getReadableProvider(@NonNull Context context, @Nullable String provider) {
        if (provider == null || TextUtils.isEmpty(provider)) {
            return context.getString(R.string.unknown);
        } else {
            return provider;
        }
    }

    @Nonnull
    @Override
    public CovidDocument getDocument() {
        return document;
    }

    @Nullable
    public String getSectionHeader() {
        return sectionHeader;
    }

    public void setSectionHeader(@Nonnull String sectionHeader) {
        this.sectionHeader = sectionHeader;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    @Nullable
    public String getProvider() {
        return provider;
    }

    @Nullable
    public Bitmap getBarcode() {
        return barcode;
    }

    @Nonnull
    public List<DynamicContent> getTopContent() {
        return topContent;
    }

    @Nonnull
    public List<DynamicContent> getCollapsedContent() {
        return collapsedContent;
    }

    protected void addTopContent(@NonNull DynamicContent dynamicContent) {
        topContent.add(dynamicContent);
    }

    protected void addCollapsedContent(@NonNull DynamicContent dynamicContent) {
        collapsedContent.add(dynamicContent);
    }

    @ColorInt
    public int getColor() {
        return color;
    }

    @DrawableRes
    public int getImageResource() {
        return imageResource;
    }

    public abstract long getTimestamp();

    @Override
    public boolean isExpanded() {
        return isExpanded;
    }

    @Override
    public void setExpanded(boolean value) {
        this.isExpanded = value;
    }
}
