package de.culture4life.luca.ui.base.bottomsheetflow

import android.annotation.SuppressLint
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class FlowPageAdapter<in PageType : BaseFlowPage>(
    fragment: Fragment,
    pages: List<PageType>,
    private val mapper: (PageType) -> Fragment
) : FragmentStateAdapter(fragment) {

    private var internalPages: MutableList<PageType> = pages.toMutableList()

    @SuppressLint("NotifyDataSetChanged")
    fun updatePages(pages: List<PageType>) {
        internalPages.clear()
        internalPages.addAll(pages)
        notifyDataSetChanged()
    }

    fun removePageAtIndex(index: Int) {
        internalPages.removeAt(index)
        // With [notifyItemRemoved] we got strange effects.
        //  When you remove a page and immediately navigate to followup page it will jump further ..
        //  Following sample scenario:
        //   1. start page
        //   2. page to remove
        //   3. target page
        //   4. final page
        //  Result: page 4. is displayed instead of page 3.
        //noinspection NotifyDataSetChanged
        notifyDataSetChanged()
    }

    fun addPageAfter(previousPage: PageType, page: PageType) {
        if (internalPages.none { it.id == page.id }) {
            val index = internalPages.indexOf(previousPage) + 1
            internalPages.add(index, page)
            notifyItemInserted(index)
        }
    }

    fun removePage(page: PageType) {
        val existingPage = internalPages.find { it.id == page.id }
        if (existingPage != null) {
            val index = internalPages.indexOf(existingPage)
            removePageAtIndex(index)
        }
    }

    fun hasItemAt(position: Int) = position >= 0 && position <= internalPages.lastIndex

    override fun getItemCount(): Int = internalPages.size

    override fun createFragment(position: Int): Fragment = mapper.invoke(internalPages[position])

    override fun getItemId(position: Int): Long = internalPages[position].id

    override fun containsItem(itemId: Long): Boolean = internalPages.any { it.id == itemId }
}
