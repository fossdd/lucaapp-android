package de.culture4life.luca.util

import java.math.BigDecimal
import java.math.RoundingMode

object AmountUtil {

    private val hundred = BigDecimal.valueOf(100)

    /**
     * Calculates the tip of a cent amount and rounds up
     *
     * For example a 10% tip for the amount "13,28€" will be "1,33€" eg. 133 returned
     */
    fun calculateTip(amountInCents: Int, tipPercentage: Int): Int {
        val percent = BigDecimal(tipPercentage).movePointLeft(2)
        val amount = BigDecimal(amountInCents)
        val result = percent.multiply(amount)
        return result.setScale(0, RoundingMode.HALF_UP).toInt()
    }

    fun calculateTipInPercent(amountInCents: Int, tipAmount: Int): Int {
        val cents = amountInCents.toCurrencyAmountBigDecimal()
        val tip = tipAmount.toCurrencyAmountBigDecimal()
        val percent = tip.multiply(hundred).divide(cents, RoundingMode.HALF_UP)
        return percent.setScale(0, RoundingMode.HALF_UP).toInt()
    }
}
