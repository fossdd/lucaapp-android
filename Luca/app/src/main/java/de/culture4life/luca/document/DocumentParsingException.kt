package de.culture4life.luca.document

class DocumentParsingException : Exception {
    constructor() : super("The document could not be parsed")
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable) : super(cause)
}
