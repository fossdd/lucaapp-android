package de.culture4life.luca.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewTreeObserver
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Checkable
import androidx.constraintlayout.widget.Group
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import timber.log.Timber

fun Group.setOnClickListeners(listener: (view: View) -> Unit) {
    referencedIds.forEach {
        val v = rootView.findViewById<View>(it)
        v.setOnClickListener { listener(v) }
    }
}

fun <T> T.setCheckedImmediately(checked: Boolean?) where T : View, T : Checkable {
    isChecked = checked == true
    jumpDrawablesToCurrentState()
}

fun View.showKeyboard(flags: Int = InputMethodManager.SHOW_IMPLICIT) {

    fun View.showKeyboardNow() {
        // post is required because the input may not be ready instantly after a focus change
        post {
            val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(this, flags)
        }
    }

    fun View.showKeyboardAfterFocusChange() {
        viewTreeObserver.addOnWindowFocusChangeListener(object : ViewTreeObserver.OnWindowFocusChangeListener {
            override fun onWindowFocusChanged(hasFocus: Boolean) {
                if (hasFocus) {
                    showKeyboardNow()
                    viewTreeObserver.removeOnWindowFocusChangeListener(this)
                }
            }
        })
    }

    requestFocus()
    if (hasWindowFocus()) {
        showKeyboardNow()
    } else {
        showKeyboardAfterFocusChange()
    }
}

fun View.hideKeyboard(window: Window? = (context as? Activity)?.window) {
    // Get compat inset controller which backports the inset api to before level 30
    if (window == null) {
        Timber.w("Window was null, keyboard can't be hidden")
        return
    }
    val windowInsetsController = WindowCompat.getInsetsController(window, this)

    // Use compat inset controller to hide keyboard (IME). Counterpart would be windowInsetsController.show(...);
    windowInsetsController.hide(WindowInsetsCompat.Type.ime())
}

var View.isInvisibleAndDisabled: Boolean
    get() = visibility == View.INVISIBLE && !isEnabled
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
        isEnabled = !value
    }
