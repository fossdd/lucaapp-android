package de.culture4life.luca.document.provider.appointment

import de.culture4life.luca.document.DocumentParsingException
import de.culture4life.luca.document.provider.CovidDocumentProvider
import de.culture4life.luca.registration.Person
import de.culture4life.luca.util.LucaUrlUtil.isAppointment
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

class AppointmentProvider : CovidDocumentProvider<Appointment>() {

    override fun canParse(encodedData: String): Single<Boolean> {
        return Single.fromCallable { isAppointment(encodedData) }
            .onErrorReturnItem(false)
    }

    override fun parse(encodedData: String): Single<Appointment> {
        return Single.fromCallable { Appointment(encodedData) }
            .onErrorResumeNext { throwable -> Single.error(DocumentParsingException(throwable)) }
    }

    override fun validateName(document: Appointment, person: Person): Completable = Completable.complete()

    override fun validateChildAge(document: Appointment): Completable = Completable.complete()
}
