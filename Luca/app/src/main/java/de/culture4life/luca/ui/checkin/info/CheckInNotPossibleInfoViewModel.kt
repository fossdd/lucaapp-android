package de.culture4life.luca.ui.checkin.info

import android.app.Application
import de.culture4life.luca.ui.BaseViewModel

class CheckInNotPossibleInfoViewModel(application: Application) : BaseViewModel(application)
