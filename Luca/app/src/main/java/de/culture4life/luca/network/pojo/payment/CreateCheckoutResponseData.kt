package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName

data class CreateCheckoutResponseData(

    @SerializedName("uuid")
    val id: String,

    @SerializedName("checkoutId")
    val providerCheckoutId: String,

    @SerializedName("checkoutUrl")
    val providerCheckoutUrl: String

)
