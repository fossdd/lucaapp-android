package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class PaymentDiscountResponseData(
    @SerializedName("discountAmount")
    val discountAmount: BigDecimal,

    @SerializedName("originalInvoiceAmount")
    val originalInvoiceAmount: BigDecimal
)
