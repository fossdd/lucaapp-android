package de.culture4life.luca.util

import java.math.BigDecimal
import java.util.*

object NumberUtil {

    val NON_DIGITS = Regex("[^\\d]")

    /**
     * Parses a given amount (in cents).
     * Defaults to 0 on error.
     *
     * Examples:
     * ```
     * "" -> 0
     * "0012345" -> 12345
     * ```
     */
    @JvmStatic
    fun toCurrencyAmountNumber(amountInCents: String): Int {
        return try {
            val digits = amountInCents.removeNonDigits()
            digits.toInt()
        } catch (exception: Exception) {
            0
        }
    }

    /**
     * Formats a given amount (in cents) to the default representation in Germany.
     * Rounds to event cents.
     *
     * Examples:
     * ```
     * 0 -> "0,00"
     * 1234.56789 -> "1234,57"
     * ```
     */
    @JvmStatic
    fun toCurrencyAmountString(amount: Int): String {
        val euro = amount / 100.0
        return "%.2f".format(Locale.GERMANY, euro)
    }
}

fun String.removeNonDigits(): String {
    return this.replace(NumberUtil.NON_DIGITS, "")
}

fun String.toCurrencyAmountNumber(): Int {
    return NumberUtil.toCurrencyAmountNumber(this)
}

fun Int.toCurrencyAmountString(): String {
    return NumberUtil.toCurrencyAmountString(this)
}

fun Int.toCurrencyAmountBigDecimal(): BigDecimal {
    return toBigDecimal().movePointLeft(2)
}

fun BigDecimal.toCurrencyAmountNumber(): Int = movePointRight(2).intValueExact()
