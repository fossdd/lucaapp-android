package de.culture4life.luca.ui.myluca.listitems

import de.culture4life.luca.R
import de.culture4life.luca.util.TimeUtil

class IdentityRequestedItem(val token: String) : MyLucaListItem(), DeletableListItem {

    override val deleteButtonText: Int
        get() = R.string.action_delete

    override val timestamp = TimeUtil.getCurrentMillis()
}
