package de.culture4life.luca.ui.checkin.info

import de.culture4life.luca.databinding.FragmentCheckInNotPossibleInfoBinding
import de.culture4life.luca.ui.BaseFragment

class CheckInNotPossibleInfoFragment : BaseFragment<CheckInNotPossibleInfoViewModel>() {
    override fun getViewModelClass() = CheckInNotPossibleInfoViewModel::class.java
    override fun getViewBinding() = FragmentCheckInNotPossibleInfoBinding.inflate(layoutInflater)
}
