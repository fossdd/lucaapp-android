package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName

data class CampaignSubmissionRequestData(

    @SerializedName("campaign")
    val campaign: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("language")
    val language: String,
)
