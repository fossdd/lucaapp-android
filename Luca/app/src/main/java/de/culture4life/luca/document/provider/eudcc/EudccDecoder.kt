package de.culture4life.luca.document.provider.eudcc

import COSE.HeaderKeys
import android.text.TextUtils
import com.upokecenter.cbor.CBORObject
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.util.TimeUtil
import de.culture4life.luca.util.fromUnixTimestamp
import dgca.verifier.app.decoder.CertificateDecodingResult
import dgca.verifier.app.decoder.DefaultCertificateDecoder
import dgca.verifier.app.decoder.base45.Base45Decoder
import dgca.verifier.app.decoder.cwt.CwtHeaderKeys
import dgca.verifier.app.decoder.model.CoseData
import java.util.zip.InflaterInputStream

class EudccDecoder(private val base45Decoder: Base45Decoder) {

    private val decoder = DefaultCertificateDecoder(base45Decoder)

    fun decodeCertificate(encodedCertificate: String): CertificateDecodingResult {
        val certificate = decoder.decodeCertificate(encodedCertificate)
        val coseData = decodeCoseData(encodedCertificate)
        sanityCheckCertificate(coseData.cbor)
        return certificate
    }

    private fun sanityCheckCertificate(cbor: ByteArray) {
        val map = CBORObject.DecodeFromBytes(cbor)

        val issuingCountry = map[CwtHeaderKeys.ISSUING_COUNTRY.asCBOR()].AsString()
        if (TextUtils.isEmpty(issuingCountry)) throw IllegalArgumentException("Issuing country not correct: $issuingCountry")

        // TODO include check when running tests
        //  regenerate all testdata with valid issuer date
        if (!LucaApplication.isRunningTests()) {
            val issuedAt = map[CwtHeaderKeys.ISSUED_AT.asCBOR()].AsInt64().fromUnixTimestamp()
            if (issuedAt > TimeUtil.getCurrentMillis()) throw IllegalArgumentException("IssuedAt not correct: $issuedAt")
        }

        // no expiration check
        //  we accept expired, could be important to proof vaccination history
    }

    fun getEncodedCoseData(encodedCertificate: String): ByteArray {
        val encodedCertificateWithoutPrefix: String = if (encodedCertificate.startsWith(DefaultCertificateDecoder.PREFIX)) {
            encodedCertificate.drop(DefaultCertificateDecoder.PREFIX.length)
        } else {
            encodedCertificate
        }
        val base45Decoded = base45Decoder.decode(encodedCertificateWithoutPrefix)
        return decompressBase45DecodedData(base45Decoded)
    }

    fun decodeCoseData(encodedCertificate: String): CoseData {
        val encodedCoseData = getEncodedCoseData(encodedCertificate)
        return decodeCoseData(encodedCoseData)
    }

    companion object {

        @JvmStatic
        fun decompressBase45DecodedData(compressedData: ByteArray): ByteArray {
            // ZLIB magic headers
            val hasExpectedHeaders = compressedData.size >= 2 &&
                compressedData[0] == 0x78.toByte() &&
                (
                    compressedData[1] == 0x01.toByte() || // Level 1
                        compressedData[1] == 0x5E.toByte() || // Level 2 - 5
                        compressedData[1] == 0x9C.toByte() || // Level 6
                        compressedData[1] == 0xDA.toByte()
                    )

            return if (hasExpectedHeaders) {
                InflaterInputStream(compressedData.inputStream()).readBytes()
            } else {
                compressedData
            }
        }

        @JvmStatic
        fun decodeCoseData(encodedData: ByteArray): CoseData {
            val messageObject = CBORObject.DecodeFromBytes(encodedData)
            val content = messageObject[2].GetByteString()
            val protectedRgb = messageObject[0].GetByteString()
            val unprotectedRgb = messageObject[1]
            val key = HeaderKeys.KID.AsCBOR()

            if (!CBORObject.DecodeFromBytes(protectedRgb).keys.contains(key)) {
                val unprotectedObject = unprotectedRgb.get(key).GetByteString()
                return CoseData(content, unprotectedObject)
            }
            val objProtected = CBORObject.DecodeFromBytes(protectedRgb).get(key).GetByteString()
            return CoseData(content, objProtected)
        }
    }
}
