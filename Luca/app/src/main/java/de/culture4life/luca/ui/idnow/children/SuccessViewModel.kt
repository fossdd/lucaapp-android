package de.culture4life.luca.ui.idnow.children

import android.app.Application
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildViewModel
import de.culture4life.luca.ui.idnow.IdNowEnrollFlowViewModel

class SuccessViewModel(app: Application) : BaseFlowChildViewModel<IdNowEnrollFlowViewModel>(app) {
    fun onActionButtonClicked() {
        sharedViewModel!!.navigateToNext()
    }
}
