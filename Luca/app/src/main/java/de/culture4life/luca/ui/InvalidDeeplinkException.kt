package de.culture4life.luca.ui

class InvalidDeeplinkException(val url: String) : Exception()
