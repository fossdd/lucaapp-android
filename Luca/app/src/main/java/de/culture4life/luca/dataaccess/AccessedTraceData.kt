package de.culture4life.luca.dataaccess

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import de.culture4life.luca.network.pojo.NotifyingHealthDepartment

/**
 * Model storing recent trace IDs and their computed [hashedTraceId], allowing the Guest app
 * to match them to hashed trace IDs accessed by a health department. This allows users to be
 * notified in case data has been accessed by a health department.
 *
 * @see DataAccessManager.fetchRecentlyAccessedTraceData
 * @see [Security Overview: Notifying Guests about Data Access](https://www.luca-app.de/securityoverview/processes/tracing_find_contacts.html.notifying-guests-about-data-access)
 */
data class AccessedTraceData(
    @Expose
    @SerializedName("hashedTracingId")
    val hashedTraceId: String,

    @Expose
    @SerializedName("tracingId")
    val traceId: String,

    @Expose
    @SerializedName("warningLevel")
    val warningLevel: Int,

    @Expose
    @SerializedName("locationName")
    val locationName: String,

    @Expose
    @SerializedName("healthDepartment")
    val healthDepartment: NotifyingHealthDepartment,

    @Expose
    @SerializedName("accessTimestamp")
    val accessTimestamp: Long,

    @Expose
    @SerializedName("checkInTimestamp")
    val checkInTimestamp: Long,

    @Expose
    @SerializedName("checkOutTimestamp")
    val checkOutTimestamp: Long,

    @Expose
    @SerializedName("isNew")
    var isNew: Boolean
) {

    companion object {
        /**
         * The number of available warning levels, starting from 1
         */
        const val NUMBER_OF_WARNING_LEVELS = 4
    }
}
