package de.culture4life.luca.ui.discovery

import android.app.Application
import de.culture4life.luca.R
import de.culture4life.luca.ui.BaseViewModel

class DiscoveryViewModel(application: Application) : BaseViewModel(application) {

    fun discoverButtonClicked() {
        val discoveryUrl = "${application.getString(R.string.API_BASE_URL)}/discovery"
        application.openUrl(discoveryUrl, true)
    }

}
