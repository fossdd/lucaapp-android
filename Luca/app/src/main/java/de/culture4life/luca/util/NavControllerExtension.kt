package de.culture4life.luca.util

import androidx.annotation.IdRes
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.navOptions

object NavControllerExtension {
    /**
     * Returns true if the current destination is the start destination of the current nested graph.
     */
    @JvmStatic
    fun isCurrentDestinationNestedGraphStart(navController: NavController): Boolean {
        return navController.isCurrentDestinationNestedGraphStart()
    }
}

/**
 * Pops up the back stack to the start destination and then navigates to [destinationId].
 */
fun NavController.navigateFromStartDestination(
    @IdRes
    destinationId: Int
) {
    navigate(
        destinationId,
        null,
        navOptions { // Sets same settings as in [NavigationUI.onNavDestinationSelected] to pop the back stack to the start destination.
            launchSingleTop = true
            restoreState = true
            popUpTo(graph.findStartDestination().id) {
                inclusive = false
                saveState = true
            }
        }
    )
}

fun NavController.isCurrentDestinationNestedGraphStart(): Boolean {
    return currentDestination?.let {
        it.id == it.parent?.startDestinationId
    } ?: false
}
