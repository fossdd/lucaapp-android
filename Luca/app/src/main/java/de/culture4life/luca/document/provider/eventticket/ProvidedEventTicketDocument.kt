package de.culture4life.luca.document.provider.eventticket

import de.culture4life.luca.document.Document.Companion.TYPE_EVENT_TICKET
import de.culture4life.luca.document.provider.ProvidedDocument
import de.culture4life.luca.util.JwtUtil
import de.culture4life.luca.util.TimeUtil
import de.culture4life.luca.util.fromUnixTimestamp

class ProvidedEventTicketDocument(encodedJwt: String) :
    ProvidedDocument<EventTicketDocument>(generateEventTicket(encodedJwt)) {
    companion object {
        private fun generateEventTicket(encodedJwt: String): EventTicketDocument {
            return with(JwtUtil.parseSignedJwt(encodedJwt).body) {
                val issuer = get("iss", String::class.java)
                val id = get("sub", String::class.java)
                val locationName = get("l", String::class.java)
                val eventName = get("e", String::class.java)
                val category = get("c", Integer::class.java)
                val startTimestamp = get("st", Integer::class.java).toLong().fromUnixTimestamp()
                val endTimestamp = get("et", Integer::class.java).toLong().fromUnixTimestamp()
                val creationTimestamp = get("iat", Integer::class.java).toLong().fromUnixTimestamp()
                val verificationId = get("v", String::class.java)
                val firstName = get("fn", String::class.java)
                val lastName = get("ln", String::class.java)

                EventTicketDocument(
                    id = id,
                    importTimestamp = TimeUtil.getCurrentMillis(),
                    encodedData = encodedJwt,
                    hashableEncodedData = JwtUtil.getHeaderAndBody(encodedJwt),
                    isVerified = false,
                    type = TYPE_EVENT_TICKET,
                    issuer = issuer,
                    locationName = locationName,
                    eventName = eventName,
                    category = EventTicketDocument.Category.fromInt(category?.toInt()),
                    startTimestamp = startTimestamp,
                    endTimestamp = endTimestamp,
                    creationTimestamp = creationTimestamp,
                    verificationId = verificationId,
                    firstName = firstName,
                    lastName = lastName
                )
            }
        }
    }
}
