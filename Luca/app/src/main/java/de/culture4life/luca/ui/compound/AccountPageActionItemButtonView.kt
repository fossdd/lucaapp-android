package de.culture4life.luca.ui.compound

import android.content.Context
import android.util.AttributeSet
import androidx.core.content.withStyledAttributes
import de.culture4life.luca.R
import de.culture4life.luca.databinding.ViewAccountPageButtonBinding

class AccountPageActionItemButtonView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defaultStyle: Int = 0
) : AccountPageActionItemBaseView(context, attributeSet, defaultStyle) {

    private val binding: ViewAccountPageButtonBinding = ViewAccountPageButtonBinding.inflate(inflater, this)

    var text: String
        get() = binding.textView.text.toString()
        set(value) {
            binding.textView.text = value
        }

    init {
        context.withStyledAttributes(attributeSet, R.styleable.AccountPageButtonView) {
            text = getString(R.styleable.AccountPageButtonView_text).orEmpty()
        }
    }
}
