package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.math.BigDecimal

data class ConsumerPaymentsResponseData(

    @SerializedName("cursor")
    val cursor: String?,

    @SerializedName("payments")
    val payments: List<Payment>

) : Serializable {

    data class Payment(

        @SerializedName("paymentId")
        val id: String,

        @SerializedName("locationName")
        val locationName: String,

        @SerializedName("locationId")
        val locationId: String,

        @SerializedName("lucaDiscount")
        val discountAmounts: PaymentDiscountResponseData?,

        @SerializedName("invoiceAmount")
        val invoiceAmount: BigDecimal,

        @SerializedName("tipAmount")
        val tipAmount: BigDecimal,

        @SerializedName("amount")
        val totalAmount: BigDecimal,

        @SerializedName("dateTime")
        val timestamp: Long,

        @SerializedName("table")
        val table: String?,

        @SerializedName("status")
        val status: Status,

        @SerializedName("refunded")
        val refunded: Boolean,

        @SerializedName("refundedAmount")
        val refundedAmount: BigDecimal,

        @SerializedName("paymentVerifier")
        val paymentVerifier: String

    ) : Serializable {
        enum class Status(val value: String) {
            CLOSED("CLOSED"), ERROR("ERROR"), UNKNOWN("UNKNOWN")
        }
    }
}
