package de.culture4life.luca.checkin

import com.google.gson.JsonObject
import de.culture4life.luca.util.deserializeFromJson

class AdditionalCheckInData(val jsonObject: JsonObject) {

    constructor(json: String) : this(json.deserializeFromJson(JsonObject::class.java))

    val table: String?
        get() = tableId ?: tableNumber

    val tableNumber: String?
        get() = jsonObject.get("table")?.asString

    val tableId: String?
        get() = jsonObject.get("tableId")?.asString

    override fun toString(): String {
        return """
            AdditionalCheckInData{
                tableNumber=$tableNumber,
                tableId=$tableId
        """.trimIndent()
    }
}
