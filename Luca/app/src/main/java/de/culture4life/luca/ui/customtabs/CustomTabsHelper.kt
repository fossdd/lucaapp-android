package de.culture4life.luca.ui.customtabs

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import androidx.browser.customtabs.CustomTabsCallback
import androidx.browser.customtabs.CustomTabsClient
import androidx.browser.customtabs.CustomTabsIntent
import androidx.browser.customtabs.CustomTabsService.ACTION_CUSTOM_TABS_CONNECTION
import androidx.browser.customtabs.CustomTabsServiceConnection
import de.culture4life.luca.LucaApplication
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber

class CustomTabsHelper {

    var isSessionReady = false
    private val builder = CustomTabsIntent.Builder()
    private val customTabsIntent = builder.build()

    private val serviceConnection = object : CustomTabsServiceConnection() {

        override fun onCustomTabsServiceConnected(name: ComponentName, client: CustomTabsClient) {
            Timber.d("Service connected: $name")

            try {
                // enable deeplink back to app
                val session = client.newSession(CustomTabsCallback())
                require(session != null) { "Custom tab service didn't respond" }
                builder.setSession(session)
                isSessionReady = true
            } catch (exception: Exception) {
                Timber.e("Unable to create session: $exception")
            }

            // initialize browser to speed up page load
            Completable.fromAction { client.warmup(0L) }
                .subscribeOn(Schedulers.io())
                .subscribe()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Timber.d("Service disconnected: $name")
            isSessionReady = false
        }
    }

    fun bind(context: Context) {
        // Binding with robolectric has to be mocked to work [ShadowInstrumentation.setComponentNameAndServiceForBindService].
        //  But custom tabs service binding isn't necessary in tests yet.
        if (!LucaApplication.isRunningUnitTests()) {
            val appsWithCustomTabsSupport = getCustomTabsPackages(context).map { it.resolvePackageName }.toMutableList()
            var packageName = CustomTabsClient.getPackageName(context, appsWithCustomTabsSupport)
            if (packageName == null) {
                packageName = "com.android.chrome"
            }
            Timber.d("Binding custom tab service: $packageName")
            CustomTabsClient.bindCustomTabsService(context, packageName, serviceConnection)
        }
    }

    fun launch(url: String, context: Context) {
        require(isSessionReady) { "Session is not ready" }
        customTabsIntent.launchUrl(context, Uri.parse(url))
    }

    /**
     * Returns a list of packages that support Custom Tabs.
     *
     * https://developer.chrome.com/docs/android/custom-tabs/best-practices/#preparing-for-other-browsers
     */
    private fun getCustomTabsPackages(context: Context): ArrayList<ResolveInfo> {
        val packageManager = context.packageManager
        // Get default VIEW intent handler.
        val activityIntent = Intent()
            .setAction(Intent.ACTION_VIEW)
            .addCategory(Intent.CATEGORY_BROWSABLE)
            .setData(Uri.fromParts("https", "www.google.de", null))

        // Get all apps that can handle VIEW intents.
        val resolvedActivityList = packageManager.queryIntentActivities(activityIntent, 0)
        val packagesSupportingCustomTabs: ArrayList<ResolveInfo> = ArrayList()
        for (info in resolvedActivityList) {
            val serviceIntent = Intent()
            serviceIntent.action = ACTION_CUSTOM_TABS_CONNECTION
            serviceIntent.setPackage(info.activityInfo.packageName)
            // Check if this package also resolves the Custom Tabs service.
            if (packageManager.resolveService(serviceIntent, 0) != null) {
                packagesSupportingCustomTabs.add(info)
            }
        }
        return packagesSupportingCustomTabs
    }
}
