package de.culture4life.luca.ui.payment.history

import android.app.Application
import android.content.ActivityNotFoundException
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import de.culture4life.luca.R
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.ui.BaseViewModel
import de.culture4life.luca.ui.payment.history.PaymentHistoryDetailFragment.Companion.ARGUMENT_PAYMENT_DATA_KEY
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single

class PaymentHistoryDetailViewModel(application: Application) : BaseViewModel(application) {

    private val paymentManager: PaymentManager = this.application.paymentManager
    val paymentData = MutableLiveData<PaymentData>()
    val payRaffleParticipation: MutableLiveData<Boolean> = MutableLiveData()
    val tableName = MutableLiveData<String>()

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(paymentManager.initialize(application))
    }

    override fun processArguments(arguments: Bundle?): Completable {
        return super.processArguments(arguments)
            .andThen(updatePaymentDetails(arguments))
            .andThen(
                Completable.mergeArray(
                    invoke(updatePayRaffleParticipation()),
                    invoke(updateTableName()),
                )
            )
    }

    private fun updatePaymentDetails(arguments: Bundle?): Completable {
        return Single.fromCallable {
            val paymentData = arguments?.getSerializable(ARGUMENT_PAYMENT_DATA_KEY) as? PaymentData
            requireNotNull(paymentData)
            paymentData
        }
            .observeOn(AndroidSchedulers.mainThread())
            .flatMapCompletable { updateInstantly(paymentData, it) }
    }

    private fun updateTableName(): Completable {
        return updateIfRequired(tableName, application.getString(R.string.unknown))
            .andThen(Maybe.fromCallable<String> { paymentData.value?.table })
            .flatMap(paymentManager::fetchTableName)
            .flatMapCompletable { update(tableName, it) }
    }

    private fun updatePayRaffleParticipation(): Completable {
        return Completable.defer {
            val paymentData = paymentData.value ?: error("Payment data was null")
            paymentManager.restorePaymentCampaignRevocationSecretIfAvailable(paymentData.id)
                .map { true }
                .defaultIfEmpty(false)
                .flatMapCompletable { update(payRaffleParticipation, it) }
        }
    }

    fun requestSupportMail() {
        try {
            paymentManager.openLucaPaySupportMailIntent(paymentData.value)
        } catch (exception: ActivityNotFoundException) {
            val viewError = createErrorBuilder(exception)
                .withTitle(R.string.menu_support_error_title)
                .withDescription(R.string.menu_support_error_description)
                .removeWhenShown()
                .build()
            addError(viewError)
        }
    }
}
