package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName

data class CampaignSubmissionResponseData(
    @SerializedName("uuid")
    val id: String,

    @SerializedName("consumerId")
    val consumerId: String,

    @SerializedName("paymentId")
    val paymentId: String,

    @SerializedName("campaign")
    val campaign: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("language")
    val language: String,

    @SerializedName("revocationSecret")
    val revocationSecret: String,

    @SerializedName("createdAt")
    val creationTimestamp: Long,
)
