package de.culture4life.luca.ui.base.bottomsheetflow

import android.content.DialogInterface
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import androidx.viewpager2.widget.ViewPager2
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.databinding.BottomSheetFlowBinding
import de.culture4life.luca.ui.base.BaseBottomSheetDialogFragment
import de.culture4life.luca.util.isInvisibleAndDisabled

abstract class BaseFlowBottomSheetDialogFragment<PageType : BaseFlowPage, ViewModelType : BaseFlowViewModel> :
    BaseBottomSheetDialogFragment<ViewModelType>() {

    protected lateinit var binding: BottomSheetFlowBinding
    protected lateinit var pagerAdapter: FlowPageAdapter<PageType>
    override var fixedHeight = true

    // Disable animations for automatic tests to avoid flakiness. Espresso does not always wait until all animations are done.
    private val useSmoothScrollAnimation = !LucaApplication.isRunningInstrumentationTests()

    protected abstract fun mapPageToFragment(page: PageType): Fragment

    abstract fun lastPageHasBackButton(): Boolean

    override fun getViewBinding(): ViewBinding {
        binding = BottomSheetFlowBinding.inflate(layoutInflater)
        return binding
    }

    override fun initializeViews() {
        super.initializeViews()
        initializeObservers()
        initializeViewPager()
    }

    private fun initializeViewPager() {
        binding.flowViewPager.apply {
            isUserInputEnabled = false
            offscreenPageLimit = 2

            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    val isFirstPage = position == 0
                    val isLastPage = pagerAdapter.itemCount - 1 == position
                    val backButtonIsHidden = isFirstPage || (isLastPage && !lastPageHasBackButton())
                    binding.backButton.isInvisibleAndDisabled = backButtonIsHidden
                    updateCancelButtonForPageChange(isLastPage)
                }
            })
        }
    }

    protected open fun updateCancelButtonForPageChange(isLastPage: Boolean) {
        binding.cancelButton.text = if (isLastPage) getString(R.string.action_close) else getString(R.string.action_cancel)
    }

    private fun initializeObservers() {
        binding.backButton.setOnClickListener { navigateToPrevious() }
        binding.cancelButton.setOnClickListener { dismiss() }

        viewModel.onPagesUpdated.observe(viewLifecycleOwner) {
            if (it.isNotHandled) {
                pagerAdapter = FlowPageAdapter(this, it.valueAndMarkAsHandled as List<PageType>, ::mapPageToFragment)
                binding.flowViewPager.adapter = pagerAdapter
            }
        }

        viewModel.pagerNavigation.observe(viewLifecycleOwner) {
            if (it.isNotHandled) {
                when (it.valueAndMarkAsHandled) {
                    BaseFlowViewModel.PagerNavigate.NEXT -> navigateToNext()
                    BaseFlowViewModel.PagerNavigate.PREVIOUS -> navigateToPrevious()
                }
            }
        }

        viewModel.pageNavigation.observe(viewLifecycleOwner) {
            if (it.isNotHandled) {
                navigateToPage(it.valueAndMarkAsHandled)
            }
        }
    }

    private fun navigateToPrevious() {
        val current = binding.flowViewPager.currentItem

        if (current > 0) {
            binding.flowViewPager.setCurrentItem(current - 1, useSmoothScrollAnimation)
        }
    }

    protected fun navigateToNext() {
        val current = binding.flowViewPager.currentItem

        when {
            pagerAdapter.hasItemAt(current + 1) ->
                binding.flowViewPager.setCurrentItem(current + 1, useSmoothScrollAnimation)

            current == (pagerAdapter.itemCount - 1) -> viewModel.onFinishFlow()
        }
    }

    private fun navigateToPage(page: BaseFlowPage) {
        val current = binding.flowViewPager.currentItem
        val target = viewModel.pages.indexOf(page)
        if (current != target && pagerAdapter.hasItemAt(target)) {
            binding.flowViewPager.setCurrentItem(target, useSmoothScrollAnimation)
        }
    }

    fun disableVerticalViewPagerScrolling() {
        binding.flowViewPager.apply {
            // access underlying RecyclerView and disable scrolling so BottomSheet can scroll vertically
            getChildAt(0).apply {
                isNestedScrollingEnabled = false
                overScrollMode = View.OVER_SCROLL_NEVER
            }
        }
    }

    fun enableVerticalViewPagerScrolling() {
        binding.flowViewPager.apply {
            // access underlying RecyclerView and disable scrolling so BottomSheet can scroll vertically
            getChildAt(0).apply {
                isNestedScrollingEnabled = true
                overScrollMode = View.OVER_SCROLL_IF_CONTENT_SCROLLS
            }
        }
    }

    fun showsLastPage(): Boolean = binding.flowViewPager.currentItem == pagerAdapter.itemCount - 1

    override fun onDismiss(dialog: DialogInterface) {
        binding.flowViewPager.adapter = null
        super.onDismiss(dialog)
    }
}
