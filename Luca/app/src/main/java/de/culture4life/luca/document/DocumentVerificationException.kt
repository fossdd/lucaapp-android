package de.culture4life.luca.document

class DocumentVerificationException : DocumentImportException {
    enum class Reason {
        NAME_MISMATCH,
        INVALID_SIGNATURE,
        PROCEDURES_EMPTY,
        MIXED_TYPES_IN_PROCEDURES,
        DATE_OF_BIRTH_TOO_OLD_FOR_CHILD,
        OUTCOME_UNKNOWN,
        TIMESTAMP_IN_FUTURE,
        UNKNOWN
    }

    val reason: Reason

    constructor(reason: Reason) : super(getDefaultMessage(reason)) {
        this.reason = reason
    }

    constructor(reason: Reason, message: String) : super(message) {
        this.reason = reason
    }

    constructor(reason: Reason, message: String, cause: Throwable) : super(message, cause) {
        this.reason = reason
    }

    constructor(reason: Reason, cause: Throwable) : super(getDefaultMessage(reason), cause) {
        this.reason = reason
    }

    companion object {
        private fun getDefaultMessage(reason: Reason): String {
            return when (reason) {
                Reason.NAME_MISMATCH -> "Name mismatch"
                Reason.INVALID_SIGNATURE -> "Invalid signature"
                Reason.PROCEDURES_EMPTY -> "Procedures size in baercode is empty"
                Reason.MIXED_TYPES_IN_PROCEDURES -> "Vaccination and non-vaccination types are mixed in procedures"
                Reason.DATE_OF_BIRTH_TOO_OLD_FOR_CHILD -> "The date of birth is too old for being a child"
                Reason.OUTCOME_UNKNOWN -> "The outcome is unknown"
                Reason.TIMESTAMP_IN_FUTURE -> "The timestamp of this document is in the future"
                Reason.UNKNOWN -> "Unknown verification error"
            }
        }
    }
}
