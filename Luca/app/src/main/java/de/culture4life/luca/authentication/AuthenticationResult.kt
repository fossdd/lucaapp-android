package de.culture4life.luca.authentication

import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.AUTHENTICATION_RESULT_TYPE_UNKNOWN
import de.culture4life.luca.util.TimeUtil

data class AuthenticationResult(
    val success: Boolean,
    val timestamp: Long = TimeUtil.getCurrentMillis(),
    val authenticationType: Int = AUTHENTICATION_RESULT_TYPE_UNKNOWN,
    val cryptoObject: BiometricPrompt.CryptoObject? = null
)
