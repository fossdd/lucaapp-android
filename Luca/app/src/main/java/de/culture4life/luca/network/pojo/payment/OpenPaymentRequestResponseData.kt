package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class OpenPaymentRequestResponseData(

    @SerializedName("uuid")
    val id: String,

    @SerializedName("locationId")
    val locationId: String,

    @SerializedName("invoiceAmount")
    val invoiceAmount: BigDecimal,

    @SerializedName("openAmount")
    val openAmount: BigDecimal?,

    @SerializedName("status")
    val status: Status,

    @SerializedName("allowSplitPayment")
    val partialAmountsSupported: Boolean,

) {
    enum class Status(val value: String) {
        OPEN("OPEN"),
        STARTED("STARTED"),
        CLOSED("CLOSED"),
        ERROR("ERROR"),
        UNKNOWN("UNKNOWN")
    }
}
