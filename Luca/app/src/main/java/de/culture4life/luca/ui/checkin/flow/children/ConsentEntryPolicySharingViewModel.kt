package de.culture4life.luca.ui.checkin.flow.children

import android.app.Application
import androidx.lifecycle.MutableLiveData
import de.culture4life.luca.ui.ViewEvent
import de.culture4life.luca.ui.base.BaseBottomSheetViewModel
import io.reactivex.rxjava3.core.Completable

class ConsentEntryPolicySharingViewModel(app: Application) : BaseBottomSheetViewModel(app) {

    val onEntryPolicySharingConsentResult: MutableLiveData<ViewEvent<Boolean>> = MutableLiveData()

    fun onConsentAccepted(consented: Boolean) {
        invoke(
            persistEntryPolicyStateSharingConsented(consented)
                .andThen(update(onEntryPolicySharingConsentResult, ViewEvent(consented)))
        ).subscribe()
    }

    private fun persistEntryPolicyStateSharingConsented(consented: Boolean): Completable {
        return preferencesManager.persist(KEY_ENTRY_POLICY_STATE_SHARING_CONSENTED, consented)
    }

    companion object {
        const val KEY_ENTRY_POLICY_STATE_SHARING_CONSENTED = "entry_policy_state_sharing_consented"
    }
}
