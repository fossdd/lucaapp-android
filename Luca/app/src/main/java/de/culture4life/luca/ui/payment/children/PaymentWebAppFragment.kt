package de.culture4life.luca.ui.payment.children

import de.culture4life.luca.databinding.BottomSheetPayWebappBinding
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildFragment
import de.culture4life.luca.ui.payment.PaymentFlowViewModel

class PaymentWebAppFragment : BaseFlowChildFragment<PaymentWebAppViewModel, PaymentFlowViewModel>() {

    private lateinit var binding: BottomSheetPayWebappBinding

    override fun getViewModelClass() = PaymentWebAppViewModel::class.java
    override fun getSharedViewModelClass() = PaymentFlowViewModel::class.java
    override fun getViewBinding() = BottomSheetPayWebappBinding.inflate(layoutInflater).also { binding = it }

    override fun onResume() {
        super.onResume()
        viewModel.onViewResumed()
    }

    companion object {
        fun newInstance(): PaymentWebAppFragment = PaymentWebAppFragment()
    }
}
