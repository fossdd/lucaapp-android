package de.culture4life.luca.ui

import android.app.Application
import android.net.Uri
import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import com.tbruyelle.rxpermissions3.Permission
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.notification.LucaNotificationManager
import de.culture4life.luca.preference.PreferencesManager
import de.culture4life.luca.ui.dialog.BaseDialogContent
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.concurrent.TimeUnit

abstract class BaseViewModel(application: Application) : AndroidViewModel(application) {

    val isInitialized: MutableLiveData<Boolean> = MutableLiveData(false)
    val isLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    val errors: MutableLiveData<Set<ViewError>> = MutableLiveData<Set<ViewError>>(setOf())
    val dialogRequest: MutableLiveData<ViewEvent<BaseDialogContent>> = MutableLiveData<ViewEvent<BaseDialogContent>>()
    val requiredPermissions = MutableLiveData<ViewEvent<out Set<String>>>()
    var navigationController: NavController? = null

    protected val application: LucaApplication = application as LucaApplication
    protected val preferencesManager: PreferencesManager = this.application.preferencesManager
    protected val modelDisposable = CompositeDisposable()
    protected val arguments = MutableLiveData<Bundle?>()

    override fun onCleared() {
        super.onCleared()
        dispose()
    }

    open fun dispose() {
        modelDisposable.dispose()
    }

    @CallSuper
    open fun initialize(): Completable {
        return Single.fromCallable { isInitialized.value!! }
            .filter { alreadyInitialized -> !alreadyInitialized }
            .flatMapCompletable { updateRequiredPermissions() }
            .andThen(preferencesManager.initialize(application))
            .andThen(update(isInitialized, true))
    }

    @CallSuper
    open fun keepDataUpdated(): Completable {
        return Completable.never()
    }

    protected fun <ValueType> update(mutableLiveData: MutableLiveData<ValueType>, value: ValueType): Completable {
        return Completable.fromAction { updateAsSideEffect(mutableLiveData, value) }
    }

    protected fun <ValueType> updateIfRequired(mutableLiveData: MutableLiveData<ValueType>, value: ValueType): Completable {
        return Completable.fromAction { updateAsSideEffectIfRequired(mutableLiveData, value) }
    }

    protected fun <ValueType> updateAsSideEffect(mutableLiveData: MutableLiveData<ValueType>, value: ValueType) {
        mutableLiveData.postValue(value)
    }

    protected fun <ValueType> updateAsSideEffectIfRequired(mutableLiveData: MutableLiveData<ValueType>, value: ValueType) {
        if (mutableLiveData.value !== value) {
            updateAsSideEffect(mutableLiveData, value)
        }
    }

    protected fun <ValueType> updateInstantly(mutableLiveData: MutableLiveData<ValueType>, value: ValueType): Completable {
        return Completable.complete()
            .observeOn(AndroidSchedulers.mainThread())
            .andThen(Completable.fromAction { updateInstantlyAsSideEffect(mutableLiveData, value) })
    }

    private fun <ValueType> updateInstantlyAsSideEffect(mutableLiveData: MutableLiveData<ValueType>, value: ValueType) {
        mutableLiveData.value = value
    }

    private fun updateRequiredPermissions(): Completable {
        return createRequiredPermissions()
            .distinct()
            .toList()
            .map(::HashSet)
            .map(::ViewEvent)
            .flatMapCompletable { update(requiredPermissions, it) }
    }

    @CallSuper
    protected fun createRequiredPermissions(): Observable<String> = Observable.empty()

    protected fun addPermissionToRequiredPermissions(vararg newlyRequiredPermissions: String) {
        addPermissionToRequiredPermissions(mutableSetOf(*newlyRequiredPermissions))
    }

    private fun addPermissionToRequiredPermissions(newlyRequiredPermissions: MutableSet<String>) {
        Timber.v("Added permissions to be requested: $newlyRequiredPermissions")
        val permissions = requiredPermissions.value
        if (permissions != null && permissions.isNotHandled) {
            newlyRequiredPermissions.addAll(permissions.valueAndMarkAsHandled)
        }
        updateAsSideEffect(requiredPermissions, ViewEvent<Set<String>>(newlyRequiredPermissions))
    }

    /**
     * Will add the specified error to [.errors] when subscribed and remove it when disposed.
     */
    protected fun addErrorUntilDisposed(viewError: ViewError): Completable {
        return Completable.create { emitter ->
            addError(viewError)
            emitter.setCancellable { removeError(viewError) }
        }
    }

    fun createErrorBuilder(throwable: Throwable): ViewError.Builder {
        return ViewError.Builder(application)
            .withCause(throwable)
    }

    fun addError(viewError: ViewError?) {
        modelDisposable.add(
            Completable.fromAction {
                if (viewError == null) {
                    return@fromAction
                }
                if (!application.isUiCurrentlyVisible && viewError.canBeShownAsNotification) {
                    showErrorAsNotification(viewError)
                } else {
                    synchronized(errors) {
                        val errorSet = errors.value!!.toMutableSet()
                        errorSet.add(viewError)
                        errors.setValue(Collections.unmodifiableSet(errorSet))
                    }
                }
            }
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Timber.d("Added error on $this: $viewError") },
                    { Timber.w("Unable to add error on $this: $viewError: $it") }
                )
        )
    }

    private fun showErrorAsNotification(error: ViewError) {
        val notificationManager = application.notificationManager
        val notificationBuilder = if (error.isExpected) {
            notificationManager.createErrorNotificationBuilder(error.title, error.description)
        } else {
            notificationManager.createErrorNotificationBuilder(
                error.title,
                application.getString(R.string.error_specific_description, error.description)
            )
        }
        notificationManager.showNotification(LucaNotificationManager.NOTIFICATION_ID_EVENT, notificationBuilder.build())
            .subscribe()
    }

    fun removeError(viewError: ViewError?) {
        modelDisposable.add(
            Completable.fromAction {
                if (viewError == null) {
                    return@fromAction
                }
                synchronized(errors) {
                    val errorSet = errors.value!!.toMutableSet()
                    val removed = errorSet.remove(viewError)
                    if (removed) {
                        errors.setValue(Collections.unmodifiableSet(errorSet))
                    } else {
                        throw IllegalStateException("Error was not added before")
                    }
                }
            }
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Timber.d("Removed error on $this: $viewError") },
                    { Timber.w("Unable to remove error on $this: $viewError: $it") }
                )
        )
    }

    fun onErrorShown(viewError: ViewError) {
        Timber.d("onErrorShown() called on $this with: viewError = [$viewError]")
        if (viewError.removeWhenShown) {
            removeError(viewError)
        }
    }

    fun onErrorDismissed(viewError: ViewError) {
        Timber.d("onErrorDismissed() called on $this with: viewError = [$viewError]")
        removeError(viewError)
    }

    @CallSuper
    open fun onPermissionResult(permission: Permission) {
        Timber.i("Permission result: $permission")
    }

    protected fun isCurrentDestinationId(
        @IdRes
        destinationId: Int
    ): Boolean {
        if (navigationController == null) {
            return false
        }
        val targetDestination = navigationController!!.graph.findNode(destinationId)
        val targetDestinationId = if (targetDestination is NavGraph) {
            targetDestination.startDestinationId
        } else {
            destinationId
        }
        val currentDestination = requireNavigationController().currentDestination
        return currentDestination != null && currentDestination.id == targetDestinationId
    }

    @CallSuper
    open fun processArguments(arguments: Bundle?) = updateIfRequired(this.arguments, arguments)

    protected fun export(uriSingle: Single<Uri>, contentSingle: Single<String>) {
        modelDisposable.add(
            Single.zip(uriSingle, contentSingle) { uri, content -> this.export(uri, content) }
                .flatMapCompletable { it }
                .doOnSubscribe { updateAsSideEffect(isLoading, true) }
                .doOnError { throwable ->
                    if (throwable !is UserCancelledException) {
                        Timber.w(throwable, "Unable to export data report: $throwable")
                        addError(
                            createErrorBuilder(throwable).removeWhenShown()
                                .build()
                        )
                    }
                }
                .onErrorComplete()
                .doFinally { updateAsSideEffect(isLoading, false) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        )
    }

    private fun export(uri: Uri, content: String): Completable {
        return Completable.fromAction {
            val stream = application.contentResolver.openOutputStream(uri)
            stream!!.write(content.toByteArray(StandardCharsets.UTF_8))
        }
            .doOnComplete { Timber.d("Exported:\n$content") }
    }

    override fun toString(): String = this.javaClass.simpleName

    operator fun invoke(completable: Completable) = invokeDelayed(completable, 0)

    fun invokeDelayed(completable: Completable, delay: Long): Completable {
        return Completable.fromAction {
            modelDisposable.add(
                completable.doOnError { Timber.w(it, "Invoked completable emitted an error: $it") }
                    .onErrorComplete()
                    .delaySubscription(delay, TimeUnit.MILLISECONDS, Schedulers.io())
                    .subscribe()
            )
        }
    }

    fun showDialog(content: BaseDialogContent) {
        updateAsSideEffect(dialogRequest, ViewEvent(content))
    }

    protected fun requireNavigationController() = checkNotNull(navigationController) { "Navigation controller was null!" }

    companion object {
        const val KEY_CAMERA_CONSENT_GIVEN = "camera_consent_given"
    }
}
