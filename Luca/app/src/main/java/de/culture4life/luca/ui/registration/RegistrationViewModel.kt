package de.culture4life.luca.ui.registration

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.network.NetworkManager.Companion.HTTP_RATE_LIMIT_REACHED
import de.culture4life.luca.network.NetworkManager.Companion.isHttpException
import de.culture4life.luca.registration.RegistrationData
import de.culture4life.luca.registration.RegistrationManager
import de.culture4life.luca.ui.BaseViewModel
import de.culture4life.luca.ui.ViewError
import de.culture4life.luca.util.StringSanitizeUtil
import de.culture4life.luca.util.TimeUtil.getCurrentMillis
import de.culture4life.luca.util.TimeUtil.getReadableDurationWithPlural
import de.culture4life.luca.util.addTo
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import timber.log.Timber
import java.net.HttpURLConnection
import java.util.concurrent.TimeUnit

class RegistrationViewModel(application: Application) : BaseViewModel(application) {

    val progress: MutableLiveData<Double> = MutableLiveData(0.0)
    val firstName: MutableLiveData<String> = MutableLiveData<String>()
    val lastName: MutableLiveData<String> = MutableLiveData<String>()
    val phoneNumber: MutableLiveData<String> = MutableLiveData<String>()
    val email: MutableLiveData<String> = MutableLiveData<String>()
    val street: MutableLiveData<String> = MutableLiveData<String>()
    val houseNumber: MutableLiveData<String> = MutableLiveData<String>()
    val city: MutableLiveData<String> = MutableLiveData<String>()
    val postalCode: MutableLiveData<String> = MutableLiveData<String>()
    val shouldRequestNewVerificationTan: MutableLiveData<Boolean> = MutableLiveData(true)
    val nextPossibleTanRequestTimestamp: MutableLiveData<Long> = MutableLiveData<Long>()
    val completed: MutableLiveData<Boolean> = MutableLiveData(false)
    val isLucaConnectNoticeRequired: Boolean
        get() = isLucaConnectEnrolled && (shouldUnEnrollLucaConnect || isAnyContactPropertyChanged)

    private val registrationManager = this.application.registrationManager
    private val documentManager = this.application.documentManager
    private val healthDepartmentManager = this.application.healthDepartmentManager
    private val connectManager = this.application.connectManager
    private val phoneNumberUtil = PhoneNumberUtil.getInstance()
    private val validationStatuses: MutableMap<LiveData<String>, MutableLiveData<Boolean>> = mutableMapOf()
    private val formValueSubjects: MutableMap<LiveData<String>, BehaviorSubject<String>> = mutableMapOf()
    private val connectEnrollmentStatus: MutableLiveData<Boolean> = MutableLiveData(false)

    private val isLucaConnectEnrolled: Boolean
        get() = connectEnrollmentStatus.value!!

    private val shouldUnEnrollLucaConnect: Boolean
        get() = isLucaConnectEnrolled && (isNameChanged || isPostalCodeChanged)

    private val shouldUpdateLucaConnectSharedData: Boolean
        get() = isLucaConnectEnrolled && !shouldUnEnrollLucaConnect && isAnyContactPropertyChanged

    private var registrationData: RegistrationData? = null
    private var registrationError: ViewError? = null

    var isInEditMode = false
        private set

    var shouldReImportTestData = false
        private set

    var isNameChanged = false
        private set

    var isPostalCodeChanged = false
        private set

    var isAnyContactPropertyChanged = false
        private set

    init {
        listOf(firstName, lastName, phoneNumber, email, street, houseNumber, city, postalCode).forEach { formData ->
            // set empty value
            formData.value = ""

            // create validation status
            val validationStatus = MutableLiveData<Boolean>()
            validationStatus.value = false
            validationStatuses[formData] = validationStatus

            // create value subject
            val formValueSubject = BehaviorSubject.createDefault(formData.value!!)
            formValueSubjects[formData] = formValueSubject
        }
    }

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(
                Completable.mergeArray(
                    registrationManager.initialize(application),
                    documentManager.initialize(application),
                    healthDepartmentManager.initialize(application),
                    connectManager.initialize(application),
                    initializeEditMode(),
                    initializeRegistrationData(),
                    resetTanRequestRateLimits(),
                    updateNextPossibleTanRequestTimestamp(),
                    updateShouldRequestNewTan(),
                    invokeDelayed(updateProgress(), 100)
                )
            )
    }

    private fun initializeRegistrationData(): Completable {
        return registrationManager.getRegistrationData()
            .doOnSuccess { this.registrationData = it }
            .ignoreElement()
    }

    private fun initializeEditMode(): Completable {
        return preferencesManager.restoreOrDefault(RegistrationManager.ONBOARDING_COMPLETED_KEY, false)
            .doOnSuccess { isInEditMode = it }
            .ignoreElement()
    }

    override fun keepDataUpdated(): Completable {
        return Completable.mergeArray(
            super.keepDataUpdated(),
            validateFormValueChanges(),
            observeRegistrationDataChanges(),
            observePhoneNumberVerificationChanges(),
            keepConnectEnrollmentStatusUpdated()
        )
    }

    private fun observeRegistrationDataChanges(): Completable {
        return preferencesManager.restoreIfAvailableAndGetChanges(RegistrationManager.REGISTRATION_DATA_KEY, RegistrationData::class.java)
            .doOnNext { updatedRegistrationData ->
                Timber.d("Restored registration data: $updatedRegistrationData")
                registrationData = updatedRegistrationData
            }
            .flatMapCompletable { updateFormValuesWithRegistrationData() }
    }

    private fun observePhoneNumberVerificationChanges(): Completable {
        return preferencesManager.restoreIfAvailableAndGetChanges(LAST_VERIFIED_PHONE_NUMBER_KEY, String::class.java)
            .flatMapCompletable { lastVerifiedNumber ->
                Completable.fromAction {
                    registrationData!!.phoneNumber = lastVerifiedNumber
                    updateAsSideEffect(phoneNumber, lastVerifiedNumber)
                }
            }
    }

    private fun validateFormValueChanges(): Completable {
        return Completable.mergeArray(
            validateFormValueChanges(firstName, ::isValidName),
            validateFormValueChanges(lastName, ::isValidName),
            validateFormValueChanges(phoneNumber, ::isValidPhoneNumber),
            validateFormValueChanges(email, ::isValidEMailAddress),
            validateFormValueChanges(street, ::isValidStreet),
            validateFormValueChanges(houseNumber, ::isValidHouseNumber),
            validateFormValueChanges(city, ::isValidCity),
            validateFormValueChanges(postalCode, ::isValidPostalCode)
        )
    }

    private fun validateFormValueChanges(data: MutableLiveData<String>, validationMethod: ValidationMethod): Completable {
        return formValueSubjects[data]!!
            .map { StringSanitizeUtil.sanitize(it) }
            .map(String::trim)
            .doOnNext(data::postValue)
            .debounce(DEBOUNCE_DURATION, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .map(validationMethod::isValid)
            .switchMapCompletable { isValid ->
                Completable.defer {
                    val validationStatus = validationStatuses[data]!!
                    val hasValidationChanged = isValid != validationStatus.value
                    if (hasValidationChanged) {
                        update(validationStatus, isValid).andThen(updateProgress().delaySubscription(500, TimeUnit.MILLISECONDS))
                    } else {
                        Completable.complete()
                    }
                }
            }
            .subscribeOn(Schedulers.computation())
    }

    fun onUserDataUpdateRequested() {
        updateRegistrationDataWithFormValues().andThen(persistUserDataUpdateInHistory())
            .andThen(additionalStepsAfterUpdate())
            .doOnSubscribe {
                updateAsSideEffect(isLoading, true)
                removeError(registrationError)
            }
            .doOnComplete { updateAsSideEffect(completed, true) }
            .doOnError { throwable ->
                registrationError = createErrorBuilder(throwable)
                    .withTitle(R.string.error_request_failed_title)
                    .withResolveAction(Completable.fromAction { onUserDataUpdateRequested() })
                    .withResolveLabel(R.string.action_retry)
                    .build()
                addError(registrationError)
            }
            .doFinally { updateAsSideEffect(isLoading, false) }
            .subscribeOn(Schedulers.io())
            .subscribe(
                { Timber.i("User data updated") },
                { throwable -> Timber.w(throwable, "Unable to update user data: $throwable") }
            )
            .addTo(modelDisposable)
    }

    private fun additionalStepsAfterUpdate(): Completable {
        return Completable.mergeArray(
            updateResponsibleHealthDepartmentIfRequired(),
            unEnrollIfRequired(),
            updateSharedDataIfRequired(),
            reImportDocumentsIfRequired()
        )
    }

    private fun updateResponsibleHealthDepartmentIfRequired(): Completable {
        return Single.fromCallable { isPostalCodeChanged }
            .filter { it }
            .flatMapCompletable {
                healthDepartmentManager.deleteResponsibleHealthDepartment()
                    .andThen(healthDepartmentManager.updateResponsibleHealthDepartmentIfRequired())
            }
    }

    private fun unEnrollIfRequired(): Completable {
        return Single.fromCallable { shouldUnEnrollLucaConnect }
            .filter { it }
            .flatMapCompletable { connectManager.unEnroll() }
    }

    private fun updateSharedDataIfRequired(): Completable {
        return Single.fromCallable { shouldUpdateLucaConnectSharedData }
            .filter { it }
            .flatMapCompletable { connectManager.reEnroll() }
    }

    private fun reImportDocumentsIfRequired(): Completable {
        return Single.fromCallable { shouldReImportTestData }
            .filter { it }
            .flatMapCompletable { documentManager.reImportDocuments() }
    }

    fun onRegistrationRequested() {
        invoke(
            updateRegistrationDataWithFormValues().andThen(persistUserDataUpdateInHistory())
                .andThen(registrationManager.persistHasCompletedOnboarding())
                .doOnComplete { updateAsSideEffect(completed, true) }
        ).subscribe(
            { Timber.i("User registered") },
            { Timber.w("Unable to register user") }
        )
    }

    fun updateRegistrationDataWithFormValuesAsSideEffect() {
        updateRegistrationDataWithFormValues()
            .onErrorComplete()
            .subscribeOn(Schedulers.io())
            .subscribe()
            .addTo(modelDisposable)
    }

    private fun persistUserDataUpdateInHistory(): Completable {
        return application.historyManager
            .addContactDataUpdateItem(registrationData!!)
    }

    private fun updateRegistrationDataWithFormValues(): Completable {
        return Completable.mergeArray(
            updatePhoneNumberVerificationStatus(),
            updateShouldReImportingTestData(),
            updateLucaConnectUpdatesRequired()
        )
            .andThen(Single.fromCallable { sanitizeRegistrationData(registrationData!!) })
            .flatMapCompletable { registrationManager.persistRegistrationData(it) }
    }

    private fun sanitizeRegistrationData(registrationData: RegistrationData): RegistrationData {
        registrationData.firstName = getSanitizedString(firstName.value)
        registrationData.lastName = getSanitizedString(lastName.value)
        if (!isInEditMode || SKIP_PHONE_NUMBER_VERIFICATION) {
            registrationData.phoneNumber = getSanitizedString(phoneNumber.value)
        }
        val email = email.value
        if (email != null && (isValidEMailAddress(email.trim { it <= ' ' }) || email.isEmpty())) {
            // email is optional and should only be set when valid or empty (to delete value)
            registrationData.email = email.trim { it <= ' ' }
        }
        registrationData.street = getSanitizedString(street.value)
        registrationData.houseNumber = getSanitizedString(houseNumber.value)
        registrationData.city = getSanitizedString(city.value)
        registrationData.postalCode = getSanitizedString(postalCode.value)
        return registrationData
    }

    private fun updateFormValuesWithRegistrationData(): Completable {
        return Completable.mergeArray(
            updateFormValue(firstName, registrationData!!.firstName),
            updateFormValue(lastName, registrationData!!.lastName),
            updateFormValue(phoneNumber, registrationData!!.phoneNumber),
            updateFormValue(email, registrationData!!.email),
            updateFormValue(street, registrationData!!.street),
            updateFormValue(houseNumber, registrationData!!.houseNumber),
            updateFormValue(city, registrationData!!.city),
            updateFormValue(postalCode, registrationData!!.postalCode)
        )
    }

    fun useDebugRegistrationData(): Completable {
        return Completable.fromAction {
            registrationData!!.firstName = "Erika"
            registrationData!!.lastName = "Mustermann"
            registrationData!!.phoneNumber = "+491711234567"
            registrationData!!.email = "erika.mustermann@example.de"
            registrationData!!.street = "Street"
            registrationData!!.houseNumber = "123"
            registrationData!!.postalCode = "12345"
            registrationData!!.city = "City"
        }.andThen(
            Completable.mergeArray(
                registrationManager.persistRegistrationData(registrationData!!),
                updateFormValuesWithRegistrationData()
            )
        )
    }

    private fun updateFormValue(liveData: MutableLiveData<String>, newValue: String?): Completable {
        return Completable.fromAction {
            if (newValue == null) {
                return@fromAction
            }
            onFormValueChanged(liveData, newValue)
        }
    }

    private fun updateProgress(): Completable {
        return Observable.defer { Observable.fromIterable(validationStatuses.values) }
            .filter { it.value!! }
            .count()
            .map { it / formValueSubjects.size.toDouble() }
            .flatMapCompletable { update(progress, it) }
    }

    fun updateShouldReImportingTestData(): Completable {
        val hasDocuments = documentManager.getOrRestoreDocuments()
            .isEmpty
            .map { isEmpty -> !isEmpty }
        val isSameFirstName = Maybe.fromCallable<String> { firstName.value }
            .map { it == registrationData!!.firstName }
            .defaultIfEmpty(false)
        val isSameLastName = Maybe.fromCallable<String> { lastName.value }
            .map { it == registrationData!!.lastName }
            .defaultIfEmpty(false)
        val hasRelevantDataChanged = Single.zip(isSameFirstName, isSameLastName) { sameFirstName, sameLastName -> !sameFirstName || !sameLastName }
        return Single.zip(
            hasDocuments,
            hasRelevantDataChanged
        ) { testResults, relevantDataChanged -> testResults && relevantDataChanged }
            .doOnSuccess { reImportTestData ->
                Timber.d("Should re-import test data: $reImportTestData")
                shouldReImportTestData = reImportTestData
            }
            .ignoreElement()
    }

    fun onFormValueChanged(liveData: LiveData<String>, newValue: String) {
        formValueSubjects[liveData]!!.onNext(newValue)
    }

    fun updatePhoneNumberVerificationStatus(): Completable {
        return Single.fromCallable<String> { phoneNumber.value }
            .flatMapCompletable { this.updatePhoneNumberVerificationStatus(it) }
    }

    private fun updatePhoneNumberVerificationStatus(currentNumber: String): Completable {
        return preferencesManager.restoreOrDefault(LAST_VERIFIED_PHONE_NUMBER_KEY, "")
            .doOnSuccess { Timber.v("Last verified number: $it - Current number: $currentNumber") }
            .map { isSamePhoneNumber(it, currentNumber) }
            .flatMapCompletable { sameNumber ->
                if (sameNumber) {
                    return@flatMapCompletable preferencesManager.persist(PHONE_VERIFICATION_COMPLETED_KEY, true)
                } else {
                    Timber.d("Resetting verification, phone number changed: $currentNumber")
                    return@flatMapCompletable Completable.mergeArray(
                        preferencesManager.persist(PHONE_VERIFICATION_COMPLETED_KEY, false),
                        updateShouldRequestNewTan()
                    )
                }
            }
    }

    fun getPhoneNumberVerificationStatus(): Single<Boolean> {
        return preferencesManager.restoreOrDefault(PHONE_VERIFICATION_COMPLETED_KEY, false)
    }

    fun requestPhoneNumberVerificationTan(): Completable {
        return assertTanRateLimitNotReached()
            .andThen(Single.fromCallable<String> { getFormattedPhoneNumber() })
            .flatMap { registrationManager.requestPhoneNumberVerificationTan(it) }
            .flatMapCompletable {
                Completable.mergeArray(
                    addToRecentTanChallengeIds(it),
                    preferencesManager.persist(LAST_TAN_REQUEST_TIMESTAMP_KEY, getCurrentMillis())
                )
            }
            .andThen(incrementTanRequestTimeoutDuration())
            .doOnSubscribe {
                removeError(registrationError)
                updateAsSideEffect(isLoading, true)
            }
            .doOnComplete { updateAsSideEffect(shouldRequestNewVerificationTan, false) }
            .doOnError { throwable ->
                updateAsSideEffect(shouldRequestNewVerificationTan, true)
                var builder = createErrorBuilder(throwable).withTitle(R.string.error_request_failed_title)
                if (throwable is VerificationException) {
                    builder = builder.withDescription(throwable.message)
                } else {
                    builder = builder.withResolveAction(Completable.fromAction { requestPhoneNumberVerificationTan() })
                        .withResolveLabel(R.string.action_retry)
                    if (isHttpException(throwable, HTTP_RATE_LIMIT_REACHED)) {
                        builder = builder.withTitle(R.string.verification_rate_limit_title)
                            .withDescription(R.string.verification_rate_limit_description)
                    } else if (isHttpException(throwable, HttpURLConnection.HTTP_BAD_GATEWAY)) {
                        builder = builder.withTitle(R.string.error_http_server_error_title)
                            .withDescription(R.string.error_http_server_error_description)
                    }
                }
                registrationError = builder.build()
                addError(registrationError)
            }
            .doFinally { updateAsSideEffect(isLoading, false) }
    }

    private fun assertTanRateLimitNotReached(): Completable {
        return nextPossibleTanVerificationTimestamp
            .map { nextPossibleTimestamp -> nextPossibleTimestamp - getCurrentMillis() }
            .flatMapCompletable { remainingTimeoutDuration ->
                if (remainingTimeoutDuration <= 0) {
                    Completable.complete()
                } else {
                    val readableDuration = getReadableDurationWithPlural(remainingTimeoutDuration, application).blockingGet()
                    Completable.error(
                        VerificationException(
                            application.getString(
                                R.string.verification_timeout_error_description,
                                readableDuration
                            )
                        )
                    )
                }
            }
    }

    private val nextPossibleTanVerificationTimestamp: Single<Long>
        get() {
            val getLastTimestamp = preferencesManager.restoreOrDefault(LAST_TAN_REQUEST_TIMESTAMP_KEY, 0L)
            val getTimeout = preferencesManager.restoreOrDefault(NEXT_TAN_REQUEST_TIMEOUT_DURATION_KEY, INITIAL_TAN_REQUESTS_TIMEOUT)
            return Single.zip(getLastTimestamp, getTimeout) { lastTimestamp: Long, timeout: Long -> lastTimestamp + timeout }
        }

    private fun incrementTanRequestTimeoutDuration(): Completable {
        return preferencesManager.restoreOrDefault(NEXT_TAN_REQUEST_TIMEOUT_DURATION_KEY, INITIAL_TAN_REQUESTS_TIMEOUT)
            .map { it * 2 }
            .doOnSuccess { Timber.i("Increasing TAN request timeout duration to $it") }
            .flatMapCompletable { preferencesManager.persist(NEXT_TAN_REQUEST_TIMEOUT_DURATION_KEY, it) }
            .andThen(updateNextPossibleTanRequestTimestamp())
    }

    private fun resetTanRequestRateLimits(): Completable {
        return preferencesManager.restoreIfAvailable(LAST_TAN_REQUEST_TIMESTAMP_KEY, Long::class.java)
            .map { getCurrentMillis() - it }
            .filter { it > TimeUnit.DAYS.toMillis(1) }
            .doOnSuccess { Timber.i("Resetting TAN request rate limits") }
            .flatMapCompletable {
                preferencesManager.persist(
                    NEXT_TAN_REQUEST_TIMEOUT_DURATION_KEY,
                    INITIAL_TAN_REQUESTS_TIMEOUT
                )
            }
    }

    private fun updateNextPossibleTanRequestTimestamp(): Completable {
        return nextPossibleTanVerificationTimestamp.flatMapCompletable { update(nextPossibleTanRequestTimestamp, it) }
    }

    private fun updateShouldRequestNewTan(): Completable {
        return preferencesManager.containsKey(RECENT_TAN_CHALLENGE_IDS_KEY)
            .map { !it || shouldRequestNewVerificationTan.value!! }
            .flatMapCompletable { update(shouldRequestNewVerificationTan, it) }
    }

    fun onPhoneNumberVerificationCanceled() {
        updateAsSideEffect(shouldRequestNewVerificationTan, true)
    }

    fun verifyTan(verificationTan: String?): Completable {
        return restoreRecentTanChallengeIds()
            .takeLast(MAXIMUM_TAN_CHALLENGE_IDS)
            .toList()
            .flatMapCompletable { registrationManager.verifyPhoneNumberWithVerificationTan(verificationTan!!, it) }
            .andThen(
                Completable.mergeArray(
                    preferencesManager.persist(LAST_VERIFIED_PHONE_NUMBER_KEY, phoneNumber.value!!),
                    preferencesManager.persist(PHONE_VERIFICATION_COMPLETED_KEY, true),
                    clearRecentTanChallengeIds()
                )
            )
            .doOnSubscribe {
                removeError(registrationError)
                updateAsSideEffect(isLoading, true)
            }
            .doOnError { throwable ->
                if (isHttpException(throwable, HttpURLConnection.HTTP_FORBIDDEN, HttpURLConnection.HTTP_BAD_REQUEST)) {
                    // TAN was incorrect
                    // No need to create an error here, this will be handled in the TAN dialog
                    return@doOnError
                }
                registrationError = createErrorBuilder(throwable).withTitle(R.string.error_request_failed_title)
                    .apply {
                        if (throwable is VerificationException) {
                            withDescription(throwable.message)
                        } else {
                            withResolveAction(verifyTan(verificationTan)).withResolveLabel(R.string.action_retry)
                        }
                    }
                    .build()
                addError(registrationError)
            }
            .doFinally { updateAsSideEffect(isLoading, false) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun addToRecentTanChallengeIds(challengeId: String): Completable {
        return persistRecentTanChallengeIds(restoreRecentTanChallengeIds().mergeWith(Observable.just(challengeId)))
            .doOnComplete { Timber.d("Added challenge ID: $challengeId") }
    }

    private fun restoreRecentTanChallengeIds(): Observable<String> {
        return preferencesManager.restoreOrDefault(RECENT_TAN_CHALLENGE_IDS_KEY, ChallengeIdContainer())
            .flatMapObservable { Observable.fromIterable(it) }
    }

    private fun persistRecentTanChallengeIds(challengeIds: Observable<String>): Completable {
        return challengeIds.toList()
            .map { ChallengeIdContainer(it) }
            .flatMapCompletable { preferencesManager.persist(RECENT_TAN_CHALLENGE_IDS_KEY, it) }
    }

    private fun clearRecentTanChallengeIds(): Completable = preferencesManager.delete(RECENT_TAN_CHALLENGE_IDS_KEY)

    private fun keepConnectEnrollmentStatusUpdated(): Completable {
        return connectManager.getEnrollmentStatusAndChanges()
            .flatMapCompletable { updateIfRequired(connectEnrollmentStatus, it) }
    }

    fun updateLucaConnectUpdatesRequired(): Completable {
        return Completable.fromAction {
            isNameChanged = !registrationData!!.hasSameName(sanitizeRegistrationData(RegistrationData()))
            isPostalCodeChanged = !registrationData!!.hasSamePostalCode(sanitizeRegistrationData(RegistrationData()))
            isAnyContactPropertyChanged = registrationData != sanitizeRegistrationData(RegistrationData())
        }
    }

    fun getFormattedPhoneNumber(): String? {
        return getFormattedPhoneNumber(phoneNumber.value!!, PhoneNumberFormat.E164)
    }

    fun getFormattedPhoneNumber(phoneNumberString: String, format: PhoneNumberFormat): String? {
        return try {
            val phoneNumber = phoneNumberUtil.parse(phoneNumberString, "DE")
            phoneNumberUtil.format(phoneNumber, format)
        } catch (e: NumberParseException) {
            phoneNumber.value
        }
    }

    val isUsingTestingCredentials: Boolean
        get() = (
            firstName.value == "Mickey" &&
                lastName.value == "Mouse" &&
                phoneNumber.value == "+4900000000000" &&
                email.value == "mickey.mouse@gmail.com"
            )

    private fun isSamePhoneNumber(firstNumber: String?, secondNumber: String?): Boolean {
        return if (firstNumber == secondNumber) {
            true
        } else {
            phoneNumberUtil.isNumberMatch(firstNumber, secondNumber) == PhoneNumberUtil.MatchType.EXACT_MATCH
        }
    }

    fun isValidPhoneNumber(phoneNumberString: String): Boolean {
        return if (isUsingTestingCredentials) {
            true
        } else try {
            val phoneNumber = phoneNumberUtil.parse(phoneNumberString, GERMAN_REGION_CODE)
            phoneNumberUtil.isValidNumber(phoneNumber)
        } catch (e: NumberParseException) {
            false
        }
    }

    fun isMobilePhoneNumber(phoneNumberString: String?): Boolean {
        return try {
            val phoneNumber = phoneNumberUtil.parse(phoneNumberString, GERMAN_REGION_CODE)
            phoneNumberUtil.getNumberType(phoneNumber) == PhoneNumberUtil.PhoneNumberType.MOBILE
        } catch (e: NumberParseException) {
            false
        }
    }

    fun getValidationStatus(textLiveData: LiveData<String>): MutableLiveData<Boolean> = validationStatuses[textLiveData]!!

    companion object {
        @JvmField
        val DEBOUNCE_DURATION: Long = if (LucaApplication.isRunningTests()) 0 else 100
        const val GERMAN_REGION_CODE = "DE"
        const val LAST_VERIFIED_PHONE_NUMBER_KEY = "last_verified_phone_number"
        const val PHONE_VERIFICATION_COMPLETED_KEY = "phone_verification_completed"
        const val RECENT_TAN_CHALLENGE_IDS_KEY = "recent_tan_challenge_ids"
        const val LAST_TAN_REQUEST_TIMESTAMP_KEY = "last_tan_request_timestamp"
        const val NEXT_TAN_REQUEST_TIMEOUT_DURATION_KEY = "next_tan_request_timeout_duration"

        private const val MAXIMUM_TAN_CHALLENGE_IDS = 10
        private val INITIAL_TAN_REQUESTS_TIMEOUT = TimeUnit.SECONDS.toMillis(15)

        @JvmField
        val SKIP_PHONE_NUMBER_VERIFICATION = LucaApplication.IS_USING_STAGING_ENVIRONMENT && !LucaApplication.isRunningTests()

        fun isValidName(name: String) = name.isNotEmpty()

        @JvmStatic
        fun isValidEMailAddress(emailAddress: String): Boolean {
            return emailAddress.length > 3 && emailAddress.contains("@") && emailAddress == emailAddress.trim { it <= ' ' }
        }

        fun isValidStreet(address: String) = address.isNotEmpty()

        fun isValidHouseNumber(houseNumber: String) = houseNumber.isNotEmpty()

        @JvmStatic
        fun isValidPostalCode(postalCode: String): Boolean {
            return (postalCode.matches("^[a-zA-Z0-9]*$".toRegex()) && postalCode.length >= 3 && postalCode.length <= 15)
        }

        fun isValidCity(city: String) = city.isNotEmpty()

        fun getSanitizedString(value: String?) = StringSanitizeUtil.sanitize(value!!).trim { it <= ' ' }
    }
}
