package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class CreateCustomerCheckoutRequestData(

    @SerializedName("locationId")
    val locationId: String,

    @SerializedName("table")
    val table: String? = null,

    @SerializedName("invoiceAmount")
    val invoiceAmount: BigDecimal,

    @SerializedName("tipAmount")
    val tipAmount: BigDecimal,

    @SerializedName("waiter")
    val waiter: String? = null,

    @SerializedName("referer")
    val referer: String = "luca/android"

)
