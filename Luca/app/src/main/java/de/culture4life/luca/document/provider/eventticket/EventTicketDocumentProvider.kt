package de.culture4life.luca.document.provider.eventticket

import android.content.Context
import com.nexenio.rxkeystore.RxKeyStore
import de.culture4life.luca.document.DocumentManager
import de.culture4life.luca.document.DocumentParsingException
import de.culture4life.luca.document.provider.DocumentProvider
import de.culture4life.luca.util.CertificateUtil
import de.culture4life.luca.util.LucaUrlUtil.isEventTicket
import de.culture4life.luca.util.verifyJwt
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

class EventTicketDocumentProvider(private val context: Context) : DocumentProvider<ProvidedEventTicketDocument> {

    private val connfairPublicKey by lazy { CertificateUtil.loadPublicPEMKey("connfair_public_key.pem", context, RxKeyStore.KEY_ALGORITHM_EC) }

    override fun canParse(encodedData: String): Single<Boolean> {
        return Single.fromCallable {
            // If it is a valid event ticket url the ticket can be parsed
            if (isEventTicket(encodedData)) return@fromCallable true

            // If it is just the data it needs to be parsed to check for correctnes
            ProvidedEventTicketDocument(encodedData)
            return@fromCallable true
        }.onErrorReturnItem(false)
    }

    override fun parse(encodedData: String): Single<ProvidedEventTicketDocument> {
        return Single.defer {
            if (isEventTicket(encodedData)) {
                DocumentManager.getEncodedDocumentFromDeepLink(encodedData)
            } else {
                Single.just(encodedData)
            }
        }.map { ProvidedEventTicketDocument(it) }
            .onErrorResumeNext { Single.error(DocumentParsingException(it)) }
    }

    override fun verify(encodedData: String): Completable {
        return Completable.fromAction {
            encodedData.verifyJwt(signingKey = connfairPublicKey, pinnedAlgorithm = "ES256")
        }
    }
}
