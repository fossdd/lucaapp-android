package de.culture4life.luca.ui.base.bottomsheetflow

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import de.culture4life.luca.ui.BaseViewModel

abstract class BaseFlowChildViewModel<SharedViewModelType : BaseFlowViewModel>(app: Application) : BaseViewModel(app) {

    protected var sharedViewModel: SharedViewModelType? = null

    fun setupSharedViewModelReference(owner: ViewModelStoreOwner, viewModelClass: Class<SharedViewModelType>) {
        sharedViewModel = ViewModelProvider(owner).get(viewModelClass)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    fun setupSharedViewModelReference(sharedViewModel: SharedViewModelType) {
        this.sharedViewModel = sharedViewModel
    }
}
