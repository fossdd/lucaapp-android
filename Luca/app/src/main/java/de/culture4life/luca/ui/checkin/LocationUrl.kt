package de.culture4life.luca.ui.checkin

import java.io.Serializable

data class LocationUrl(val type: UrlType, val url: String) : Serializable {
    enum class UrlType {
        MENU, PROGRAM, MAP, WEBSITE, GENERAL
    }
}
