package de.culture4life.luca.util

import de.culture4life.luca.network.NetworkManager.Companion.MAXIMUM_REQUEST_RETRY_COUNT
import io.reactivex.rxjava3.core.*

object SingleUtil {

    @JvmStatic
    fun <T : Any> retryWhen(
        throwableClass: Class<out Throwable>,
        maximumRetries: Int = MAXIMUM_REQUEST_RETRY_COUNT
    ): SingleTransformer<T, T> {
        return SingleTransformer<T, T> { single ->
            single.retryWhen { errors ->
                errors.zipWith(
                    Flowable.range(0, maximumRetries + 1),
                    { error, attempt -> Pair(error, attempt) }
                ).flatMap { errorAndAttempt ->
                    val isExpectedError = errorAndAttempt.first.isCause(throwableClass)
                    val hasAttempts = errorAndAttempt.second < maximumRetries
                    if (isExpectedError && hasAttempts) {
                        Flowable.just(errorAndAttempt.first) // retry
                    } else {
                        Flowable.error(errorAndAttempt.first) // don't retry
                    }
                }
            }
        }
    }

    @JvmStatic
    fun <T : Any> assertValue(
        predicate: (T) -> Boolean,
        exception: (T) -> Exception = { IllegalStateException("Assertion failed for value: $it") }
    ): SingleConverter<T, Completable> {
        return SingleConverter<T, Completable> { single ->
            single.flatMapCompletable {
                if (predicate(it)) {
                    Completable.complete()
                } else {
                    Completable.error(exception(it))
                }
            }
        }
    }
}

fun <T : Any> Single<T>.retryWhen(
    throwableClass: Class<out Throwable>,
    maximumRetries: Int = MAXIMUM_REQUEST_RETRY_COUNT
): Single<T> {
    return this.compose(SingleUtil.retryWhen(throwableClass, maximumRetries))
}

fun <T : Any> Single<T>.assertValue(
    predicate: (T) -> Boolean,
    exception: (T) -> Exception = { IllegalStateException("Assertion failed for value: $it") }
): Completable {
    return this.to(SingleUtil.assertValue(predicate, exception))
}

fun Single<Boolean>.assertTrue(
    exception: (Boolean) -> Exception = { IllegalStateException("Assertion failed for value: $it") }
): Completable {
    return this.assertValue({ value -> value }, exception)
}

fun Single<Boolean>.assertFalse(
    exception: (Boolean) -> Exception = { IllegalStateException("Assertion failed for value: $it") }
): Completable {
    return this.assertValue({ value -> !value }, exception)
}
