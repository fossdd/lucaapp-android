package de.culture4life.luca.ui.payment.history.recycler.viewholder

import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import de.culture4life.luca.R
import de.culture4life.luca.databinding.ItemPaymentBinding
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.util.NumberUtil
import de.culture4life.luca.util.getReadableDateTime

class PaymentsViewHolder(
    private val binding: ItemPaymentBinding,
    private val onPaymentClicked: (PaymentData) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: PaymentData) {
        binding.titleTextView.text = item.locationName
        binding.dateTextView.text = binding.root.context.getReadableDateTime(item.timestamp)
        binding.amountTextView.text = binding.root.context.getString(R.string.euro_amount, NumberUtil.toCurrencyAmountString(item.totalAmount))
        binding.cancelledTextView.isVisible = item.refunded
        binding.amountTextView.isVisible = !item.refunded
        binding.root.setOnClickListener { onPaymentClicked(item) }
    }
}
