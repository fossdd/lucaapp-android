package de.culture4life.luca.document.provider

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface DocumentProvider<DocumentType : ProvidedDocument<*>> {
    fun canParse(encodedData: String): Single<Boolean>
    fun parse(encodedData: String): Single<DocumentType>
    fun verify(encodedData: String): Completable
}
