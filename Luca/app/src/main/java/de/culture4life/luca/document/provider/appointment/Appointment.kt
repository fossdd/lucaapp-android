package de.culture4life.luca.document.provider.appointment

import android.net.Uri
import de.culture4life.luca.document.CovidDocument
import de.culture4life.luca.document.Document
import de.culture4life.luca.document.provider.ProvidedDocument
import de.culture4life.luca.util.TimeUtil.getCurrentMillis
import java.util.*

data class Appointment(private val url: String) : ProvidedDocument<CovidDocument>(CovidDocument()) {
    @JvmField
    val type: String

    @JvmField
    val lab: String

    @JvmField
    val address: String

    @JvmField
    val timestamp: String

    @JvmField
    val qrCode: String

    init {
        val uri = Uri.parse(url)

        val type = uri.getQueryParameter("type")
        requireNotNull(type) { "Invalid type parameter" }
        this.type = type

        val lab = uri.getQueryParameter("lab")
        requireNotNull(lab) { "Invalid lab parameter" }
        this.lab = lab

        val address = uri.getQueryParameter("address")
        requireNotNull(address) { "Invalid address parameter" }
        this.address = address

        val timestamp = uri.getQueryParameter("timestamp")
        requireNotNull(timestamp) { "Invalid timestamp parameter" }
        this.timestamp = timestamp

        val qrCode = uri.getQueryParameter("qrCode")
        requireNotNull(qrCode) { "Invalid qrCode parameter" }
        this.qrCode = qrCode

        document.type = Document.TYPE_APPOINTMENT
        document.firstName = type
        document.lastName = address
        document.labName = lab
        document.testingTimestamp = timestamp.toLong()
        document.resultTimestamp = document.testingTimestamp
        document.importTimestamp = getCurrentMillis()
        document.id = UUID.nameUUIDFromBytes(qrCode.toByteArray()).toString()
        document.provider = lab
        document.encodedData = url
        document.hashableEncodedData = qrCode
    }
}
