package de.culture4life.luca.util

import androidx.test.espresso.idling.CountingIdlingResource
import de.culture4life.luca.LucaApplication

/**
 * Idling resource that tracks the initialization process in [BaseFragment] and [BaseActivity]
 */
object InitializationIdlingResource {

    val actualIdlingResource by lazy {
        if (LucaApplication.isRunningTests()) {
            CountingIdlingResource(InitializationIdlingResource::class.java.name)
        } else {
            error("Cannot access idling resource in production/non-test code!")
        }
    }

    fun increment() {
        if (LucaApplication.isRunningTests()) {
            actualIdlingResource.increment()
        }
    }

    fun decrement() {
        if (LucaApplication.isRunningTests()) {
            actualIdlingResource.decrement()
        }
    }
}
