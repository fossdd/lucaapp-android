package de.culture4life.luca.ui.registration

class VerificationException(message: String) : Exception(message)
