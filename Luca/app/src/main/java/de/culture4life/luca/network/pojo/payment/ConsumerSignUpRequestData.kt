package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Holds the credentials of the consumer to sign up (register) for payment.
 *
 * @see [Sign up consumer](https://app.luca-app.de/pay/api/v1/swagger/#/Consumers/post_consumers_signup)
 */
data class ConsumerSignUpRequestData(

    @Expose
    @SerializedName("username")
    val username: String,

    @Expose
    @SerializedName("name")
    val name: String,

    @Expose
    @SerializedName("password")
    val password: String
)
