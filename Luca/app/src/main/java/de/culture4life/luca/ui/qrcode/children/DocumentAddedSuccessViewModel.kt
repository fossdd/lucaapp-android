package de.culture4life.luca.ui.qrcode.children

import android.app.Application
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildViewModel
import de.culture4life.luca.ui.qrcode.AddCertificateFlowViewModel

class DocumentAddedSuccessViewModel(app: Application) : BaseFlowChildViewModel<AddCertificateFlowViewModel>(app) {
    fun onActionButtonPressed() {
        sharedViewModel!!.navigateToNext()
    }
}
