package de.culture4life.luca.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import java.util.concurrent.TimeUnit;

import de.culture4life.luca.R;
import de.culture4life.luca.authentication.AuthenticationUiExtension;
import de.culture4life.luca.ui.consent.ConsentUiExtension;
import de.culture4life.luca.ui.registration.RegistrationActivity;
import de.culture4life.luca.util.AccessibilityServiceUtil;
import de.culture4life.luca.util.CompletableUtil;
import de.culture4life.luca.util.InitializationIdlingResource;
import de.culture4life.luca.util.NavControllerExtension;
import five.star.me.FiveStarMe;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import timber.log.Timber;

public class MainActivity extends BaseActivity implements NavigationBarView.OnItemSelectedListener {

    private NavController navigationController;
    private BottomNavigationView bottomNavigationView;
    private MainViewModel viewModel;
    // Destination IDs that can be navigated to on messages tab and account tab

    protected boolean initialized;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Timber.d("onCreate() called with: savedInstanceState = [%s]", savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeNavigation();
        hideActionBar();
        setupKeyboardListener();
        initializeConsentRequests();
        initializeAuthenticationRequests();

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.setNavigationController(navigationController);
        viewModel.initialize()
                .doOnComplete(() -> {
                    viewModel.onNewIntent(getIntent());
                    this.initialized = true;
                })
                .subscribe();

        Completable.fromAction(() -> FiveStarMe.with(this)
                .setInstallDays(2)
                .setLaunchTimes(7)
                .monitor())
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    private void initializeNavigation() {
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.checkInFragment, R.id.myLucaFragment, R.id.paymentFragment, R.id.accountFragment, R.id.messagesFragment
        ).build();
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.navigationHostFragment);
        navigationController = navHostFragment.getNavController();
        navigationController.setGraph(R.navigation.mobile_navigation);
        NavigationUI.setupActionBarWithNavController(this, navigationController, appBarConfiguration);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setItemIconTintList(null); // don't tint icons, we'll control states changes by ourselves
        NavigationUI.setupWithNavController(bottomNavigationView, navigationController);
        if (AccessibilityServiceUtil.getFontScale(getApplicationContext()) > 1.3) {
            bottomNavigationView.setItemTextAppearanceActive(R.style.ThemeOverlay_Luca_BottomNavigationView_StaticFontSize);
            bottomNavigationView.setItemTextAppearanceInactive(R.style.ThemeOverlay_Luca_BottomNavigationView_StaticFontSize);
        }
        // Get IDs of destinations that can be opened on messages tab and account tab
        bottomNavigationView.setOnItemSelectedListener(this);

        MenuItem accountMenuItem = bottomNavigationView.getMenu().findItem(R.id.nav_graph_account);
        MenuItemCompat.setContentDescription(accountMenuItem, getString(R.string.account_tab_title_content_description));

        if (application.isInDarkMode()) {
            // workaround for removing the elevation color overlay
            // https://github.com/material-components/material-components-android/issues/1148
            bottomNavigationView.setElevation(0);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return NavigationUI.onNavDestinationSelected(item, navigationController);
    }

    private void setupKeyboardListener() {
        KeyboardVisibilityEvent.setEventListener(this, isOpen -> {
            bottomNavigationView.setVisibility(isOpen ? View.GONE : View.VISIBLE);
        });
    }

    private void initializeConsentRequests() {
        new ConsentUiExtension(getSupportFragmentManager(), application.getConsentManager(), activityDisposable);
    }

    private void initializeAuthenticationRequests() {
        new AuthenticationUiExtension(getSupportFragmentManager(), application.getAuthenticationManager(), activityDisposable);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Timber.d("onNewIntent() called with: intent = [%s]", intent);
        super.onNewIntent(intent);
        if (viewModel.isInitialized().getValue()) {
            viewModel.onNewIntent(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        activityDisposable.add(waitUntilInitializationCompleted()
                .andThen(viewModel.keepDataUpdated())
                .doOnSubscribe(disposable -> Timber.d("Keeping data updated for %s", this))
                .doOnError(throwable -> Timber.w(throwable, "Unable to keep data updated for %s", this))
                .retryWhen(errors -> errors.delay(1, TimeUnit.SECONDS))
                .doFinally(() -> Timber.d("Stopping to keep data updated for %s", this))
                .subscribeOn(Schedulers.io())
                .subscribe());
    }

    @Override
    protected void onResume() {
        super.onResume();
        showRegistrationIfRequired();
    }

    private void showRegistrationIfRequired() {
        activityDisposable.add(application.getRegistrationManager().hasCompletedOnboarding()
                .doOnError(throwable -> Timber.w("Unable to check if registration has been completed: %s", throwable.toString()))
                .onErrorReturnItem(false)
                .subscribe(registrationCompleted -> {
                    if (!registrationCompleted) {
                        Intent intent = new Intent(this, RegistrationActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }));
    }

    @Override
    public void onBackPressed() {
        int currentDestinationId = (navigationController.getCurrentDestination() != null) ? navigationController.getCurrentDestination().getId() : 0;
        if (currentDestinationId != R.id.checkInFragment && NavControllerExtension.isCurrentDestinationNestedGraphStart(navigationController)) {
            // Going back from another tab to start destination via back press clears the back stack, therefore navigate via bottom navigation instead
            MenuItem checkInMenuItem = bottomNavigationView.getMenu().findItem(R.id.nav_graph_check_in);
            NavigationUI.onNavDestinationSelected(checkInMenuItem, navigationController);
        } else {
            super.onBackPressed();
        }
    }

    private Completable waitUntilInitializationCompleted() {
        return Observable.interval(0, 50, TimeUnit.MILLISECONDS)
                .filter(tick -> initialized)
                .firstOrError()
                .ignoreElement()
                .doOnSubscribe(disposable -> InitializationIdlingResource.INSTANCE.increment())
                .doFinally(InitializationIdlingResource.INSTANCE::decrement);
    }
}
