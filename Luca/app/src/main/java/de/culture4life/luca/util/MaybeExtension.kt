package de.culture4life.luca.util

import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single

inline fun <T, R> Maybe<T>.mapNotNull(crossinline block: (T) -> R?): Maybe<R> = flatMap { Maybe.fromCallable { block(it) } }

fun <T> Maybe<T>.isNotEmpty(): Single<Boolean> = isEmpty.map { !it }
