package de.culture4life.luca.crypto

open class DailyKeyUnavailableException : Exception {
    constructor(message: String) : super(message)
    constructor(cause: Throwable) : super(cause)
}
