package de.culture4life.luca.ui

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView

/**
 * RecyclerView that allows having a fading edge and a bottom/top padding, otherwise adding a padding would remove the fading edge effect
 */
class RecyclerViewWithFadingEdgeAndPadding @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defaultStyle: Int = 0
) : RecyclerView(context, attributeSet, defaultStyle) {

    override fun isPaddingOffsetRequired(): Boolean {
        return true
    }

    override fun getTopPaddingOffset(): Int {
        return -paddingTop
    }

    override fun getBottomPaddingOffset(): Int {
        return paddingBottom
    }
}
