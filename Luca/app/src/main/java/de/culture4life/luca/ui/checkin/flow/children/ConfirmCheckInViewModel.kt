package de.culture4life.luca.ui.checkin.flow.children

import android.app.Application
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildViewModel
import de.culture4life.luca.ui.checkin.flow.CheckInFlowViewModel
import io.reactivex.rxjava3.core.Completable

class ConfirmCheckInViewModel(app: Application) : BaseFlowChildViewModel<CheckInFlowViewModel>(app) {

    fun onActionButtonClicked(skipCheckInConfirmation: Boolean) {
        invoke(
            persistSkipCheckInConfirmation(skipCheckInConfirmation)
                .doOnComplete { sharedViewModel!!.navigateToNext() }
        ).subscribe()
    }

    private fun persistSkipCheckInConfirmation(skipCheckInConfirmation: Boolean): Completable {
        return preferencesManager.persist(KEY_SKIP_CHECK_IN_CONFIRMATION, skipCheckInConfirmation)
    }

    companion object {
        const val KEY_SKIP_CHECK_IN_CONFIRMATION = "dont_ask_confirmation"
        const val KEY_LOCATION_NAME = "locationName"
    }
}
