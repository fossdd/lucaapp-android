package de.culture4life.luca.ui.payment.children

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildViewModel
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import de.culture4life.luca.util.ThrowableUtil
import de.culture4life.luca.util.retryWhenWithDelay
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import java.util.concurrent.TimeUnit

class PaymentResultViewModel(application: Application) : BaseFlowChildViewModel<PaymentFlowViewModel>(application) {

    private val paymentManager: PaymentManager = this.application.paymentManager

    val raffleCampaignActive: MutableLiveData<Boolean> = MutableLiveData()

    val paymentResult: LiveData<PaymentData>
        get() = sharedViewModel?.paymentResult ?: error("Shared ViewModel was not present!")

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(paymentManager.initialize(application))
            .andThen(invoke(updateRaffle()))
    }

    private fun updateRaffle(): Completable {
        return Single.defer { sharedViewModel!!.getPaymentLocationData() }
            .map { it.locationId }
            .flatMap { paymentManager.isRaffleCampaignActive(it) }
            .flatMapCompletable { updateIfRequired(raffleCampaignActive, it) }
            .retryWhenWithDelay(TimeUnit.SECONDS.toMillis(1)) { ThrowableUtil.isNetworkError(it) }
    }

    fun onNextButtonClicked() {
        sharedViewModel!!.navigateToNext()
    }

    fun onRetryButtonClicked() {
        sharedViewModel!!.navigateToPrevious()
    }
}
