package de.culture4life.luca.ui.discovery

import de.culture4life.luca.databinding.FragmentDiscoveryBinding
import de.culture4life.luca.ui.BaseFragment

class DiscoveryFragment : BaseFragment<DiscoveryViewModel>() {

    private lateinit var binding: FragmentDiscoveryBinding

    override fun getViewBinding() = FragmentDiscoveryBinding.inflate(layoutInflater).also { binding = it }
    override fun getViewModelClass() = DiscoveryViewModel::class.java

    override fun initializeViews() {
        super.initializeViews()
        binding.discoverButton.setOnClickListener { viewModel.discoverButtonClicked() }
    }
}
