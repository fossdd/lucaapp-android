package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Holds the data of the for payment signed in consumer containing the [accessToken] to use for further authentication.
 *
 * @see [Sign in consumer](https://app.luca-app.de/pay/api/v1/swagger/#/Consumers/post_consumers_signin)
 */
data class ConsumerSignInResponseData(

    @Expose
    @SerializedName("accessToken")
    val accessToken: String,

    @Expose
    @SerializedName("expiresIn")
    val expirationTimestamp: Long
)
