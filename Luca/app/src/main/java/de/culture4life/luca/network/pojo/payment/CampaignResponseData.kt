package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName
import de.culture4life.luca.util.TimeUtil
import de.culture4life.luca.util.fromUnixTimestamp
import java.math.BigDecimal

data class CampaignResponseData(

    @SerializedName("id")
    val campaignId: String,

    @SerializedName("createdAt")
    val creationTimestamp: Long,

    @SerializedName("paymentCampaign")
    val paymentCampaign: PaymentCampaign

) {

    data class PaymentCampaign(

        @SerializedName("name")
        val name: String,

        @SerializedName("startsAt")
        val startTimestamp: Long?,

        @SerializedName("endsAt")
        val endTimestamp: Long?,

        @SerializedName("discountPercentage")
        val discountPercentage: Int? = null,

        @SerializedName("discountMaxAmount")
        val maximumDiscountAmount: BigDecimal? = null

    ) {

        val isCurrentlyActive: Boolean
            get() = wasActiveAt(TimeUtil.getCurrentMillis())

        fun wasActiveAt(timestamp: Long): Boolean {
            val started = startTimestamp != null && startTimestamp.fromUnixTimestamp() < timestamp
            val ended = endTimestamp != null && endTimestamp.fromUnixTimestamp() <= timestamp
            return started && !ended
        }

        val isDiscountCampaign: Boolean
            get() {
                return name.startsWith(DISCOUNT_PREFIX) &&
                    discountPercentage != null &&
                    maximumDiscountAmount != null
            }
    }

    companion object {
        private const val DISCOUNT_PREFIX = "luca-discount"
    }
}
