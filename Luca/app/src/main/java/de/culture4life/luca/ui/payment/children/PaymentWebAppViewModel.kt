package de.culture4life.luca.ui.payment.children

import android.app.Application
import androidx.lifecycle.SavedStateHandle
import de.culture4life.luca.R
import de.culture4life.luca.network.NetworkManager
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildViewModel
import de.culture4life.luca.ui.payment.PaymentFlowPage
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import de.culture4life.luca.util.*
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import java.net.HttpURLConnection.HTTP_NOT_ACCEPTABLE
import java.net.HttpURLConnection.HTTP_NOT_FOUND
import java.util.concurrent.TimeUnit

class PaymentWebAppViewModel(application: Application, private val savedStateHandle: SavedStateHandle) :
    BaseFlowChildViewModel<PaymentFlowViewModel>(application) {

    private val paymentManager: PaymentManager = this.application.paymentManager
    private var paymentStartTimestamp: Long = TimeUtil.getCurrentMillis()
    private var paymentWebAppOpened: Boolean
        get() = savedStateHandle[SAVED_STATE_WEB_APP_OPENED] ?: false
        set(value) {
            savedStateHandle[SAVED_STATE_WEB_APP_OPENED] = value
        }

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(paymentManager.initialize(application))
            .doOnComplete {
                paymentStartTimestamp = TimeUtil.getCurrentMillis()
            }
    }

    fun onViewResumed() {
        invoke(
            Completable.defer {
                if (!paymentWebAppOpened) {
                    openPaymentWebApp()
                } else {
                    updatePaymentStatus()
                }
            }.retryWhenWithDelay(TimeUnit.SECONDS.toMillis(3), NetworkManager.MAXIMUM_REQUEST_RETRY_COUNT, Schedulers.io(), ThrowableUtil::isNetworkError)
                .doOnError {
                    if (it.isHttpException(HTTP_NOT_FOUND)) {
                        // the payment has already been closed
                        sharedViewModel!!.onPaymentRequestExpired()
                    } else {
                        val viewError = createErrorBuilder(it)
                            .removeWhenShown()

                        if (it.isHttpException(HTTP_NOT_ACCEPTABLE)) {
                            // the partial payment amount either exceeded the open amount,
                            // or is lower than the minimum amount but doesn't match the open amount
                            viewError.withDescription(R.string.error_pay_split_amount)
                                .withResolveLabel(R.string.action_update)
                                .withResolveAction(
                                    Completable.fromAction {
                                        sharedViewModel!!.navigateToPage(PaymentFlowPage.PartialAmountPage)
                                    }
                                )
                        } else {
                            viewError.withResolveLabel(R.string.action_retry)
                                .withResolveAction(Completable.fromAction { onViewResumed() })
                        }

                        addError(viewError.build())
                    }
                }
        ).subscribe()
    }

    /*
        Initiate checkout using web-app
     */

    private fun openPaymentWebApp(): Completable {
        return getPaymentWebAppUrl()
            .observeOn(AndroidSchedulers.mainThread())
            .flatMapCompletable { url ->
                Completable.fromAction {
                    paymentWebAppOpened = true
                    paymentManager.openPaymentWebApp(url)
                }
            }
            .doOnSubscribe { Timber.d("Opening payment web-app") }
    }

    private fun getPaymentWebAppUrl(): Single<String> {
        return sharedViewModel!!.getPaymentLocationData()
            .flatMap {
                val paymentData = sharedViewModel!!.paymentAmounts.value!!
                val paymentRequestId = sharedViewModel!!.paymentRequestId.value
                paymentManager.createCheckout(
                    locationId = it.locationId,
                    paymentRequestId = paymentRequestId,
                    table = it.table,
                    invoiceAmount = paymentData.invoiceAmount,
                    partialAmount = if (paymentData.isPartialPayment) paymentData.partialPaymentAmount else null,
                    tipAmount = paymentData.tipAmount
                )
            }
            .map { it.providerCheckoutUrl }
    }

    /*
        Get checkout status after returning from web-app
     */

    private fun updatePaymentStatus(): Completable {
        return paymentManager.restoreLastCheckoutIdIfAvailable()
            .flatMapSingle(paymentManager::fetchCheckout)
            .map { PaymentData.fromPayment(it.payment, it.locationId) }
            .toSingle()
            .doOnSuccess(::onPaymentStatusUpdated)
            .ignoreElement()
            .andThen(paymentManager.deleteLastCheckoutId())
            .onErrorResumeNext { error ->
                Timber.w("Unable to update payment status: $error")
                if (error.isCause(NoSuchElementException::class.java) || error.isHttpException(HTTP_NOT_FOUND)) {
                    // unrecoverable, attempt to create a new checkout on retry
                    paymentWebAppOpened = false
                }
                Completable.error(error)
            }
            .doOnSubscribe { Timber.d("Updating payment status") }
    }

    private fun onPaymentStatusUpdated(paymentData: PaymentData) {
        Timber.d("Payment status updated: $paymentData")
        paymentManager.onPaymentResultReceived(paymentData)
        updateAsSideEffectIfRequired(sharedViewModel!!.paymentResult, paymentData)
        sharedViewModel!!.navigateToNext()
        paymentWebAppOpened = false
    }

    companion object {
        private const val SAVED_STATE_WEB_APP_OPENED = "web_app_opened"
    }
}
