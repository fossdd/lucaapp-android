package de.culture4life.luca.ui.payment.children

import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import de.culture4life.luca.R
import de.culture4life.luca.databinding.BottomSheetPayAmountBinding
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildFragment
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import de.culture4life.luca.util.*
import io.reactivex.rxjava3.schedulers.Schedulers

class PaymentAmountFragment : BaseFlowChildFragment<PaymentAmountViewModel, PaymentFlowViewModel>() {

    private lateinit var binding: BottomSheetPayAmountBinding

    override fun getViewModelClass() = PaymentAmountViewModel::class.java
    override fun getSharedViewModelClass() = PaymentFlowViewModel::class.java
    override fun getViewBinding() = BottomSheetPayAmountBinding.inflate(layoutInflater).also { binding = it }

    override fun initializeViews() {
        super.initializeViews()

        initializeAmountInput()
        initializePartialPayment()
        binding.amountTextView.setOnClickListener { setKeyboardVisibility(true) }
        binding.continueWithAmountButton.setOnClickListener { viewModel.onAmountConfirmed() }

        viewModel.locationName.observe(viewLifecycleOwner) {
            binding.descriptionTextView.text = getPlaceholderString(R.string.pay_amount_text, "locationName" to it)
        }
        viewModel.paymentAmounts.observe(viewLifecycleOwner, ::updateAmountViews)
        viewModel.warningText.observe(viewLifecycleOwner) {
            binding.errorTextView.text = it
            binding.errorTextView.isInvisible = it.isNullOrBlank()
        }
        viewModel.showKeyboard.observe(viewLifecycleOwner, ::setKeyboardVisibility)
    }

    private fun initializeAmountInput() {
        binding.amountEditText.addTextChangedListener(PaymentAmountTextWatcher(viewModel::onAmountChanged))
        // Trigger text listener manually in the case that the EditText had its state restored after process death
        binding.amountEditText.text = binding.amountEditText.text
        binding.amountEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                viewModel.onAmountConfirmed()
            }
            false
        }
    }

    private fun initializePartialPayment() {
        binding.continueWithPartialAmountButton.setOnClickListener { viewModel.onPartialAmount() }
        observe(viewModel.isPartialAmountSupported) {
            binding.continueWithPartialAmountButton.isVisible = it
        }
    }

    private fun updateAmountViews(paymentAmounts: PaymentAmounts) {
        val openAmountString = (paymentAmounts.openAmount ?: paymentAmounts.invoiceAmount).toCurrencyAmountString()
        var finalAmountString = openAmountString

        val showDiscount = paymentAmounts.discountPercentage > 0 && viewModel.isFixedAmount.value == true
        with(binding) {
            if (showDiscount) {
                amountTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.payment_promotion, application.theme))

                originAmountTextView.text = getString(R.string.euro_amount, openAmountString)
                originAmountTextView.isVisible = true

                campaignInfoTextView.text = getPlaceholderString(
                    R.string.pay_details_discount_hint,
                    "discountPercentage" to "${paymentAmounts.discountPercentage}",
                    "maximumDiscountAmount" to paymentAmounts.maximumDiscountAmount.toCurrencyAmountString()
                )
                campaignInfoTextView.isVisible = true
                campaignInfoImageView.isVisible = true

                val remainingDiscountedOpenAmount = paymentAmounts.remainingOpenAmount ?: paymentAmounts.remainingInvoiceAmount
                finalAmountString = remainingDiscountedOpenAmount.toCurrencyAmountString()
            } else {
                amountTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.white, application.theme))
                originAmountTextView.visibility = View.GONE
                campaignInfoTextView.visibility = View.GONE
                campaignInfoImageView.visibility = View.GONE
            }

            amountTextView.text = finalAmountString
            continueWithAmountButton.text = getPlaceholderString(R.string.pay_continue_button, "amount" to finalAmountString)
        }
    }

    override fun onResume() {
        super.onResume()
        waitUntilInitializationCompleted()
            .doOnComplete { viewModel.onResume() }
            .subscribeOn(Schedulers.io())
            .subscribe()
            .addTo(viewDisposable)

        setKeyboardVisibility(viewModel.showKeyboard.value == true)
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
    }

    private fun setKeyboardVisibility(visible: Boolean) {
        if (visible) {
            binding.amountEditText.showKeyboard()
        } else {
            binding.amountEditText.hideKeyboard((parentFragment as? DialogFragment)?.dialog?.window)
        }
    }

    companion object {
        fun newInstance() = PaymentAmountFragment()
    }
}
