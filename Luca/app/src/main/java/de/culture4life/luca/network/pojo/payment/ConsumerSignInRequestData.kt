package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Holds the credentials of the consumer to sign in for payment.
 *
 * @see [Sign in consumer](https://app.luca-app.de/pay/api/v1/swagger/#/Consumers/post_consumers_signin)
 */
data class ConsumerSignInRequestData(

    @Expose
    @SerializedName("username")
    val username: String,

    @Expose
    @SerializedName("password")
    val password: String
)
