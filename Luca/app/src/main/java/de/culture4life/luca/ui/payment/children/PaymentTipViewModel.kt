package de.culture4life.luca.ui.payment.children

import android.app.Application
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildViewModel
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import io.reactivex.rxjava3.core.Completable
import timber.log.Timber

class PaymentTipViewModel(app: Application, private val savedStateHandle: SavedStateHandle) : BaseFlowChildViewModel<PaymentFlowViewModel>(app) {

    private val paymentManager = application.paymentManager
    private val tipPercentage: MutableLiveData<Int> = savedStateHandle.getLiveData(SAVED_STATE_TIP_PERCENTAGE)

    val paymentAmount by lazy {
        MediatorLiveData<PaymentAmounts>().apply {
            value = PaymentAmounts(0)
            addSource(tipPercentage) { value = value!!.copy(tipPercentage = it) }
            addSource(sharedViewModel!!.paymentAmounts) { value = it.copy(tipPercentage = value!!.tipPercentage) }
        }
    }

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(paymentManager.initialize(application))
            .andThen(invoke(preselectDefaultTipPercentage()))
    }

    private fun preselectDefaultTipPercentage(): Completable {
        return Completable.defer {
            if (tipPercentage.value != null) {
                Completable.complete()
            } else {
                paymentManager.restoreDefaultTipPercentage()
                    .doOnSuccess { onTipChanged(it) }
                    .ignoreElement()
            }
        }
    }

    fun onTipChanged(percentage: Int) {
        Timber.d("Tip changed: $percentage%")
        updateAsSideEffectIfRequired(tipPercentage, percentage)
    }

    fun onTipConfirmed() {
        val percentage = tipPercentage.value!!
        Timber.i("Confirmed tip: $percentage%")
        invoke(paymentManager.persistLastTipPercentage(percentage)).subscribe()
        updateAsSideEffectIfRequired(sharedViewModel!!.paymentAmounts, paymentAmount.value!!)
        sharedViewModel!!.navigateToNext()
    }

    companion object {
        private const val SAVED_STATE_TIP_PERCENTAGE = "tip_percentage"
    }
}
