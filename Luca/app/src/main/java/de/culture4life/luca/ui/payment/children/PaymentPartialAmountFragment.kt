package de.culture4life.luca.ui.payment.children

import android.view.inputmethod.EditorInfo
import androidx.core.view.isInvisible
import de.culture4life.luca.R
import de.culture4life.luca.databinding.BottomSheetPayPartialAmountBinding
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildFragment
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import de.culture4life.luca.util.getPlaceholderString
import de.culture4life.luca.util.hideKeyboard
import de.culture4life.luca.util.showKeyboard
import de.culture4life.luca.util.toCurrencyAmountString

class PaymentPartialAmountFragment : BaseFlowChildFragment<PaymentPartialAmountViewModel, PaymentFlowViewModel>() {

    private lateinit var binding: BottomSheetPayPartialAmountBinding

    override fun getViewModelClass() = PaymentPartialAmountViewModel::class.java
    override fun getSharedViewModelClass() = PaymentFlowViewModel::class.java
    override fun getViewBinding() = BottomSheetPayPartialAmountBinding.inflate(layoutInflater).also { binding = it }

    override fun initializeViews() {
        super.initializeViews()

        initializeAmountInput()
        viewModel.paymentAmounts.observe(viewLifecycleOwner, ::updateAmountViews)
        binding.amountTextView.setOnClickListener { setKeyboardVisibility(true) }
        binding.continueWithAmountButton.setOnClickListener { viewModel.onAmountConfirmed() }
        viewModel.warningText.observe(viewLifecycleOwner) {
            binding.errorTextView.text = it
            binding.errorTextView.isInvisible = it.isNullOrBlank()
        }
        viewModel.showKeyboard.observe(viewLifecycleOwner, ::setKeyboardVisibility)
    }

    private fun initializeAmountInput() {
        binding.amountEditText.addTextChangedListener(PaymentAmountTextWatcher(viewModel::onAmountChanged))
        // Trigger text listener manually in the case that the EditText had its state restored after process death
        binding.amountEditText.text = binding.amountEditText.text
        binding.amountEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                viewModel.onAmountConfirmed()
            }
            false
        }
    }

    private fun updateAmountViews(paymentAmounts: PaymentAmounts) {
        val openAmountString = (paymentAmounts.openAmount ?: paymentAmounts.invoiceAmount).toCurrencyAmountString()
        val partialAmountString = paymentAmounts.partialPaymentAmount.toCurrencyAmountString()

        with(binding) {
            openAmountTextView.text = getString(R.string.euro_amount, openAmountString)
            amountTextView.text = partialAmountString
            continueWithAmountButton.text = getPlaceholderString(R.string.pay_continue_button, "amount" to partialAmountString)
        }
    }

    override fun onResume() {
        super.onResume()
        setKeyboardVisibility(true)
    }

    override fun onPause() {
        super.onPause()
        setKeyboardVisibility(false)
    }

    private fun setKeyboardVisibility(visible: Boolean) {
        if (visible) {
            binding.amountEditText.showKeyboard()
        } else {
            binding.amountEditText.hideKeyboard()
        }
    }

    companion object {
        fun newInstance() = PaymentPartialAmountFragment()
    }
}
