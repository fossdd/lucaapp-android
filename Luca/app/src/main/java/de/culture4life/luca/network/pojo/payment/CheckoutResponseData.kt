package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class CheckoutResponseData(

    @SerializedName("locationId")
    val locationId: String,

    @SerializedName("checkoutId")
    val checkoutId: String,

    @SerializedName("table")
    val table: String?,

    @SerializedName("payment")
    val payment: Payment

) {
    data class Payment(

        @SerializedName("paymentId")
        val id: String?,

        @SerializedName("locationName")
        val locationName: String,

        @SerializedName("lucaDiscount")
        val discountAmounts: PaymentDiscountResponseData?,

        @SerializedName("invoiceAmount")
        val invoiceAmount: BigDecimal?,

        @SerializedName("tipAmount")
        val tipAmount: BigDecimal,

        @SerializedName("amount")
        val totalAmount: BigDecimal,

        @SerializedName("dateTime")
        val timestamp: Long,

        @SerializedName("table")
        val table: String?,

        @SerializedName("status")
        val status: Status,

        @SerializedName("paymentVerifier")
        val paymentVerifier: String?

    ) {
        enum class Status(val value: String) {
            CLOSED("CLOSED"), ERROR("ERROR"), UNKNOWN("UNKNOWN")
        }
    }
}
