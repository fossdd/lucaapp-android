package de.culture4life.luca.ui.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.ContextWrapper
import android.content.DialogInterface
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class BaseDialogFragment : DialogFragment {

    var onDismissListener: DialogInterface.OnDismissListener? = null

    private val builder: AlertDialog.Builder

    constructor(builder: MaterialAlertDialogBuilder) {
        this.builder = builder
    }

    constructor(context: Context, content: BaseDialogContent) {
        builder = MaterialAlertDialogBuilder(context).apply {
            setTitle(content.title)
            setMessage(content.message)
            if (content.positiveText != null) {
                setPositiveButton(content.positiveText, content.positiveCallback)
            }
            if (content.neutralText != null) {
                setNeutralButton(content.neutralText, content.neutralCallback)
            }
            if (content.negativeText != null) {
                setNegativeButton(content.negativeText, content.negativeCallback)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val messageTextView = dialog!!.findViewById<TextView>(android.R.id.message)
        messageTextView.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = builder.create()

    fun show() {
        val fragmentManager = getFragmentManager(builder.context)
        if (fragmentManager != null) {
            super.show(fragmentManager, null)
        } else {
            throw IllegalStateException("No fragment manager available")
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        onDismissListener?.onDismiss(dialog)
        super.onDismiss(dialog)
    }

    private fun getFragmentManager(context: Context): FragmentManager? {
        val activity = getActivity(context)
        return if (activity is FragmentActivity) activity.supportFragmentManager else null
    }

    private fun getActivity(context: Context?): Activity? {
        if (context == null || context !is ContextWrapper)
            return null
        return if (context is Activity) context else getActivity(context.baseContext)
    }
}
