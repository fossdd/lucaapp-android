package de.culture4life.luca.ui.myluca.listitems

interface ExpandableListItem {
    var isExpanded: Boolean
    fun toggleExpanded() {
        isExpanded = !isExpanded
    }
}
