package de.culture4life.luca.payment

import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.consent.Consent
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.consent.MissingConsentException
import de.culture4life.luca.crypto.CryptoManager
import de.culture4life.luca.network.endpoints.LucaPayEndpoints
import de.culture4life.luca.network.pojo.payment.*
import de.culture4life.luca.preference.PreferencesManager
import de.culture4life.luca.testtools.preconditions.MockServerPreconditions.Companion.REVOCATION_CODE
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.testtools.samples.LucaPaySamples
import de.culture4life.luca.testtools.samples.PaymentSamples
import de.culture4life.luca.util.TimeUtil
import de.culture4life.luca.util.toCurrencyAmountBigDecimal
import de.culture4life.luca.util.toUnixTimestamp
import de.culture4life.luca.whatisnew.WhatIsNewManager
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.HttpException
import java.net.HttpURLConnection
import java.util.*

class PaymentManagerTest : LucaUnitTest() {

    private lateinit var paymentManager: PaymentManager
    private lateinit var preferencesManager: PreferencesManager
    private lateinit var cryptoManager: CryptoManager
    private lateinit var whatIsNewManager: WhatIsNewManager
    private lateinit var consentManager: ConsentManager
    private lateinit var lucaPayEndpoints: LucaPayEndpoints

    @Before
    fun before() {
        lucaPayEndpoints = mock()
        cryptoManager = spy(application.cryptoManager)
        whatIsNewManager = spy(application.whatIsNewManager)
        consentManager = spy(application.consentManager)
        val networkManagerSpy = spy(application.networkManager)
        whenever(networkManagerSpy.getLucaPayEndpoints()).thenReturn(Single.just(lucaPayEndpoints))

        paymentManager = spy(
            application.getInitializedManager(
                PaymentManager(
                    application.preferencesManager,
                    networkManagerSpy,
                    cryptoManager,
                    whatIsNewManager,
                    consentManager
                )
            ).blockingGet()
        )

        preferencesManager = application.preferencesManager
    }

    @Test
    fun `Not signed up by default`() {
        // When
        val testObserver = paymentManager.isSignedUp().test()

        // Then
        testObserver.await().assertValue(false)
    }

    @Test
    fun `Is signed up after SignInData was persisted`() {
        // given
        givenSignInData()

        // When
        val testObserver = paymentManager.isSignedUp().test()

        // Then
        testObserver.await().assertValue(true)
    }

    @Test
    fun `Is not signed up after SignInData was deleted`() {
        // given
        givenSignInData()
        paymentManager.deleteSignInData().blockingAwait()

        // When
        val testObserver = paymentManager.isSignedUp().test()

        // Then
        testObserver.await().assertValue(false)
    }

    @Test
    fun `Deletion removes persisted data`() {
        // given
        givenConsumerData()
        givenSignInData()
        whenever(lucaPayEndpoints.deleteConsumer(any())).thenReturn(Completable.complete())

        // When
        val testObserver = paymentManager.deleteConsumer().test()

        // Then
        testObserver.await().assertComplete()
        paymentManager.isSignedUp()
            .test()
            .assertValue(false)
    }

    @Test
    fun `SignUp completes successfully with given consumerData`() {
        // given
        givenAssertConsent()
        givenConsumerData()
        givenSignUpSuccessResponse()
        givenSignInSuccessResponse()

        // When
        val testObserver = paymentManager.signUp().test()

        // Then
        testObserver.await().assertNoErrors()
    }

    @Test
    fun `SignUp completes successfully after creating consumerData`() {
        // given
        givenAssertConsent()
        givenSignUpSuccessResponse()
        givenSignInSuccessResponse()

        // When
        val testObserver = paymentManager.signUp().test()

        // Then
        testObserver.await().assertNoErrors()
    }

    @Test
    fun `On successful sign up revocation code is stored and revocation notification is shown`() {
        // given
        givenAssertConsent()
        givenSignUpSuccessResponse()
        givenSignInSuccessResponse()

        // When
        val testObserver = paymentManager.signUp().test()

        // Then
        testObserver.await().assertNoErrors()
        val message = whatIsNewManager.getMessage(WhatIsNewManager.ID_LUCA_PAY_REVOCATION_CODE_MESSAGE).blockingGet()
        assertTrue(message.enabled)
        val revocationCode = paymentManager.getRevocationCode().blockingGet()
        assertEquals(REVOCATION_CODE, revocationCode)
    }

    @Test
    fun `SignUp fails when request for signing up fails`() {
        // given
        givenAssertConsent()
        givenConsumerData()
        val exception = Mockito.mock(HttpException::class.java)
        givenSignUpErrorResponse(exception)

        // When
        val testObserver = paymentManager.signUp().test()

        // Then
        testObserver.await().assertError(exception)
    }

    @Test
    fun `SignUp fails when request for signing in fails`() {
        // given
        givenAssertConsent()
        givenConsumerData()
        givenSignUpSuccessResponse()
        val exception = Mockito.mock(HttpException::class.java)
        givenSignInErrorResponse(exception)

        // When
        val testObserver = paymentManager.signUp().test()

        // Then
        testObserver.await().assertError(exception)
    }

    @Test
    fun `SignUp completes after one HTTP_CONFLICT`() {
        // given
        givenAssertConsent()
        givenConsumerData()
        val exception = Mockito.mock(HttpException::class.java)
        whenever(exception.code()).thenReturn(HttpURLConnection.HTTP_CONFLICT)
        val consumer = LucaPaySamples.ErikaMustermann()
        val requestData = ConsumerSignUpRequestData(consumer.username, consumer.username, consumer.password)
        givenSignUpErrorResponse(exception, requestData)
        whenever(lucaPayEndpoints.deleteConsumer(any())).thenReturn(Completable.complete())
        val retryConsumerData = ConsumerData("new${consumer.username}", consumer.username, "new${consumer.password}")
        doReturn(Single.just(retryConsumerData)).`when`(paymentManager).createConsumerData()
        val retrySignUpRequestData = ConsumerSignUpRequestData(retryConsumerData.username, retryConsumerData.name, retryConsumerData.password)
        givenSignUpSuccessResponse(retrySignUpRequestData)
        givenSignInSuccessResponse()

        // When
        val testObserver = paymentManager.signUp().test()

        // Then
        testObserver.await().assertNoErrors()
        verify(paymentManager, Mockito.times(2)).signUp()
    }

    @Test
    fun `RequestConsentAndSignUp fails when no consent was given`() {
        // given
        val consent = mock<Consent> { whenever(it.approved).then { false } }
        doReturn(Single.just(consent)).`when`(consentManager).getConsent(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_PAY)
        val exception = Mockito.mock(MissingConsentException::class.java)
        doReturn(Completable.error(exception)).`when`(consentManager)
            .requestConsentBundleIfRequiredAndAssertApproved(*ConsentManager.lucaPayConsentBundle)
        givenConsumerData()
        givenSignUpSuccessResponse()

        // When
        val testObserver = paymentManager.requestConsentIfRequiredAndSignUp().test()

        // Then
        testObserver.await().assertError(exception)
    }

    @Test
    fun `RequestConsentAndSignUp completes successfully with consent`() {
        // given
        givenRequestConsent()
        givenAssertConsent()
        givenConsumerData()
        givenSignUpSuccessResponse()
        givenSignInSuccessResponse()

        // When
        val testObserver = paymentManager.requestConsentIfRequiredAndSignUp().test()

        // Then
        testObserver.await().assertNoErrors()
    }

    @Test
    fun `SignIn completes successfully with given consumerData`() {
        // given
        givenConsumerData()
        givenSignInSuccessResponse()

        // When
        val testObserver = paymentManager.signIn().test()

        // Then
        testObserver.await().assertNoErrors()
    }

    @Test
    fun `SignIn fails when request for signing in fails`() {
        // given
        givenConsumerData()
        val exception = Mockito.mock(HttpException::class.java)
        givenSignInErrorResponse(exception)

        // When
        val testObserver = paymentManager.signIn().test()

        // Then
        testObserver.await().assertError(exception)
    }

    @Test
    fun `FetchAllPaymentHistory returns the whole payment history`() {
        // Given
        givenConsumerData()
        givenSignInData()
        val customer = LucaPaySamples.ErikaMustermann()
        val payment1 = customer.payments[0]
        val payment2 = payment1.copy(id = UUID.randomUUID().toString())
        val payment3 = payment1.copy(id = UUID.randomUUID().toString())
        givenPaymentHistoryPageResponse(null, CURSOR_PAGE_1, listOf(payment1.toPayment()))
        givenPaymentHistoryPageResponse(CURSOR_PAGE_1, CURSOR_PAGE_2, listOf(payment2.toPayment()))
        givenPaymentHistoryPageResponse(CURSOR_PAGE_2, null, listOf(payment3.toPayment()))

        // When
        val observer = paymentManager.fetchCompletePaymentHistory().test()
        triggerScheduler()

        // Then
        observer.await().assertValueCount(3)
        observer.assertValueAt(0, payment1)
        observer.assertValueAt(1, payment2)
        observer.assertValueAt(2, payment3)
    }

    @Test
    fun `GetConsumerInformation returns the right consumer information`() {
        // Given
        givenSignInData()
        val consumer = LucaPaySamples.ErikaMustermann()
        val signIn = LucaPaySamples.SignIn()
        val response = ConsumerInformationResponseData(
            uuid = UUID.randomUUID().toString(),
            username = consumer.username,
            payCards = consumer.payCards
        )
        whenever(lucaPayEndpoints.getConsumerInformation(signIn.accessToken)).thenReturn(Single.just(response))

        // When
        val observer = paymentManager.fetchConsumerInformation().test()
        triggerScheduler()

        // Then
        observer.await().assertValue(response)
    }

    @Test
    fun `CreateCheckout calls checkout for operator-initiated payment`() {
        // Given
        givenSignInData()
        val signIn = LucaPaySamples.SignIn()
        val location = LocationSamples.Restaurant()
        val payment = PaymentSamples.LocationPayment(location)
        val checkoutRequest = payment.operatorCheckoutRequestData
        val response = payment.createCheckoutResponseData
        whenever(lucaPayEndpoints.createCheckoutForPaymentRequest(signIn.accessToken, location.locationId, payment.id, checkoutRequest))
            .thenReturn(Single.just(response))

        // When
        val observer = paymentManager.createCheckout(
            locationId = location.locationId,
            paymentRequestId = payment.id,
            table = payment.table,
            invoiceAmount = 0,
            partialAmount = null,
            tipAmount = payment.tipAmount
        ).test()
        triggerScheduler()

        // Then
        observer.await().assertValue(response)
    }

    @Test
    fun `CreateCheckout calls checkout for consumer-initiated payment`() {
        // Given
        givenSignInData()
        val signIn = LucaPaySamples.SignIn()
        val location = LocationSamples.Restaurant()
        val payment = PaymentSamples.LocationPayment(location)
        val checkoutRequest = payment.customerCheckoutRequestData
        val checkoutResponse = payment.createCheckoutResponseData

        whenever(lucaPayEndpoints.createCheckout(signIn.accessToken, checkoutRequest)).thenReturn(Single.just(checkoutResponse))

        // When
        val observer = paymentManager.createCheckout(
            locationId = location.locationId,
            paymentRequestId = null,
            table = payment.table,
            invoiceAmount = payment.invoiceAmount,
            partialAmount = null,
            tipAmount = payment.tipAmount
        ).test()
        triggerScheduler()

        // Then
        observer.await().assertValue(checkoutResponse)
    }

    @Test
    fun `Submitting campaign successfully stores revocation secret`() {
        // Given
        givenSignInData()
        givenSignInSuccessResponse()
        val consumer = LucaPaySamples.ErikaMustermann()
        val paymentId = consumer.payments.first().id
        val submissionRequestData = CampaignSubmissionRequestData(
            campaign = PaymentManager.CAMPAIGN_PAY_RAFFLE,
            email = CovidDocumentSamples.ErikaMustermann().registrationData().email!!,
            language = "de"
        )
        val submissionResponseData = CampaignSubmissionResponseData(
            id = UUID.randomUUID().toString(),
            consumerId = consumer.id,
            paymentId = paymentId,
            campaign = submissionRequestData.campaign,
            email = submissionRequestData.email,
            language = "de",
            revocationSecret = REVOCATION_CODE,
            creationTimestamp = TimeUtil.getCurrentMillis()
        )
        whenever(lucaPayEndpoints.submitCampaignParticipation(LucaPaySamples.SignIn().accessToken, paymentId, submissionRequestData)).thenReturn(
            Single.just(submissionResponseData)
        )

        // When
        paymentManager.submitRaffleCampaignParticipation(paymentId, submissionRequestData.email)
            .test()
            .await()
            .assertNoErrors()

        // Then
        assertEquals(
            REVOCATION_CODE,
            paymentManager.restorePaymentCampaignRevocationSecretIfAvailable(paymentId).blockingGet()
        )
    }

    private fun givenSignInData() {
        val signIn = LucaPaySamples.SignIn()
        paymentManager.persistSignInData(SignInData(signIn.accessToken, signIn.expirationTimestamp)).blockingAwait()
    }

    private fun givenConsumerData() {
        val consumer = LucaPaySamples.ErikaMustermann()
        paymentManager.persistConsumerData(ConsumerData(consumer.username, consumer.username, consumer.password)).blockingAwait()
    }

    private fun givenSignUpSuccessResponse(requestData: ConsumerSignUpRequestData? = null) {
        whenever(lucaPayEndpoints.signUpConsumer(requestData ?: any())).thenReturn(
            Single.just(
                ConsumerSignUpResponseData(
                    "uuid",
                    requestData?.username ?: "username",
                    REVOCATION_CODE
                )
            )
        )
    }

    private fun givenSignUpErrorResponse(exception: Exception, requestData: ConsumerSignUpRequestData? = null) {
        whenever(lucaPayEndpoints.signUpConsumer(requestData ?: any())).thenReturn(Single.error(exception))
    }

    private fun givenSignInSuccessResponse(requestData: ConsumerSignInRequestData? = null) {
        val signIn = LucaPaySamples.SignIn()
        whenever(lucaPayEndpoints.signInConsumer(requestData ?: any())).thenReturn(
            Single.just(
                ConsumerSignInResponseData(signIn.accessToken, signIn.expirationTimestamp)
            )
        )
    }

    private fun givenSignInErrorResponse(exception: Exception, requestData: ConsumerSignInRequestData? = null) {
        whenever(lucaPayEndpoints.signInConsumer(requestData ?: any())).thenReturn(Single.error(exception))
    }

    private fun givenPaymentHistoryPageResponse(
        cursor: String? = null,
        nextCursor: String? = null,
        payments: List<ConsumerPaymentsResponseData.Payment>
    ) {
        val signIn = LucaPaySamples.SignIn()
        whenever(lucaPayEndpoints.getConsumerPayments(signIn.accessToken, cursor)).thenReturn(
            Single.just(
                ConsumerPaymentsResponseData(
                    nextCursor,
                    payments
                )
            )
        )
    }

    private fun givenRequestConsent() {
        val consent = mock<Consent> { whenever(it.approved).then { true } }
        doReturn(Single.just(consent)).`when`(consentManager).getConsent(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_PAY)
        doReturn(Completable.complete()).`when`(consentManager).requestConsentIfRequiredAndAssertApproved(ConsentManager.ID_ACTIVATE_LUCA_PAY)
    }

    private fun givenAssertConsent() {
        doReturn(Completable.complete()).`when`(consentManager).assertConsentApproved(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_PAY)
        doReturn(Completable.complete()).`when`(consentManager).assertConsentApproved(ConsentManager.ID_ACTIVATE_LUCA_PAY)
    }

    private fun PaymentData.toPayment(): ConsumerPaymentsResponseData.Payment = ConsumerPaymentsResponseData.Payment(
        id = this.id,
        locationName = this.locationName,
        locationId = this.locationId,
        totalAmount = this.totalAmount.toCurrencyAmountBigDecimal(),
        invoiceAmount = this.invoiceAmount.toCurrencyAmountBigDecimal(),
        discountAmounts = null,
        tipAmount = this.tipAmount.toCurrencyAmountBigDecimal(),
        timestamp = this.timestamp.toUnixTimestamp(),
        table = this.table,
        status = ConsumerPaymentsResponseData.Payment.Status.valueOf(this.status.value),
        refunded = this.refunded,
        refundedAmount = this.refundedAmount.toCurrencyAmountBigDecimal(),
        paymentVerifier = this.paymentVerifier
    )

    companion object {
        private const val CURSOR_PAGE_1 = "cursor1"
        private const val CURSOR_PAGE_2 = "cursor2"
    }
}
