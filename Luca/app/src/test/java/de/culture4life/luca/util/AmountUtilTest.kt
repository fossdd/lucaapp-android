package de.culture4life.luca.util

import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class AmountUtilTest {

    @Test
    fun calculateTip() {
        val scenarios = listOf(

            TipScenario(10010, 10, 1001),
            TipScenario(10010, 15, 1502), // case: has to be round up to get right tip amount
            TipScenario(10010, 20, 2002),

            // case: math operations are 0 safe
            TipScenario(10010, 0, 0),
            TipScenario(0, 20, 0),

            // small amounts
            TipScenario(1, 10, 0),
            TipScenario(4, 10, 0),
            TipScenario(5, 10, 1),
            TipScenario(10, 10, 1),
            TipScenario(14, 10, 1),
            TipScenario(15, 10, 2),
        )

        scenarios.forEach {
            val calculatedTip = AmountUtil.calculateTip(amountInCents = it.amount, tipPercentage = it.percentage)
            Assertions.assertThat(calculatedTip)
                .withFailMessage("got $calculatedTip for $it")
                .isEqualTo(it.tip)
        }
    }

    @Test
    fun calculateTipInPercent() {
        val scenarios = listOf(
            TipScenario(100, 5, 5),
            TipScenario(150, 10, 15),
            TipScenario(170, 15, 25),

            TipScenario(500, 5, 25),
            TipScenario(587, 15, 88), // case: has to be round down

            TipScenario(10010, 10, 1001),
            TipScenario(10010, 15, 1502), // case: has to be round up to get right tip amount
            TipScenario(10010, 20, 2002),
        )

        scenarios.forEach {
            val calculatedTipInPercent = AmountUtil.calculateTipInPercent(it.amount, it.tip)
            assertThat(calculatedTipInPercent)
                .withFailMessage("got $calculatedTipInPercent for $it")
                .isEqualTo(it.percentage)
        }
    }
}

data class TipScenario(val amount: Int, val percentage: Int, val tip: Int)
