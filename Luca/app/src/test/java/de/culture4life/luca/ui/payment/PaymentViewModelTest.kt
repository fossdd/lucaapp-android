package de.culture4life.luca.ui.payment

import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.consent.Consent
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.consent.MissingConsentException
import de.culture4life.luca.crypto.CryptoManager
import de.culture4life.luca.network.endpoints.LucaPayEndpoints
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.preference.PreferencesManager
import de.culture4life.luca.whatisnew.WhatIsNewManager
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class PaymentViewModelTest : LucaUnitTest() {

    private lateinit var viewModel: PaymentViewModel
    private lateinit var preferencesManager: PreferencesManager
    private lateinit var cryptoManager: CryptoManager
    private lateinit var paymentManager: PaymentManager
    private lateinit var lucaPayEndpoints: LucaPayEndpoints
    private lateinit var whatIsNewManager: WhatIsNewManager
    private lateinit var consentManager: ConsentManager

    @Before
    fun before() {
        val applicationSpy = spy(application)
        cryptoManager = spy(application.cryptoManager)
        whatIsNewManager = spy(application.whatIsNewManager)
        consentManager = spy(application.consentManager)
        doReturn(cryptoManager).`when`(applicationSpy).cryptoManager
        doReturn(whatIsNewManager).`when`(applicationSpy).whatIsNewManager
        doReturn(consentManager).`when`(applicationSpy).consentManager

        lucaPayEndpoints = mock()
        val networkManagerSpy = spy(application.networkManager)
        whenever(networkManagerSpy.getLucaPayEndpoints()).thenReturn(Single.just(lucaPayEndpoints))

        preferencesManager = application.preferencesManager
        paymentManager = spy(
            getInitializedManager(
                PaymentManager(
                    preferencesManager,
                    networkManagerSpy,
                    cryptoManager,
                    whatIsNewManager,
                    consentManager
                )
            )
        )
        doReturn(paymentManager).`when`(applicationSpy).paymentManager

        viewModel = spy(PaymentViewModel(applicationSpy))
    }

    @Test
    fun `Calling initialize to check updateSignedUpStatus with consumer being signed up`() {
        // Given
        doReturn(Single.just(true)).`when`(paymentManager).isSignedUp()

        // When
        val observer = viewModel.initialize().test()
        triggerScheduler()

        // Then
        observer.await().assertNoErrors()
        assertTrue(viewModel.isSignedUpForPayment.value!!)
    }

    @Test
    fun `Calling initialize to check updateSignedUpStatus with no consumer being signed up`() {
        // Given
        doReturn(Single.just(false)).`when`(paymentManager).isSignedUp()

        // When
        val observer = viewModel.initialize().test()
        triggerScheduler()

        // Then
        observer.await().assertNoErrors()
        assertFalse(viewModel.isSignedUpForPayment.value!!)
    }

    @Test
    fun `Calling onPaymentSignUpRequested with success when consent is given`() {
        // Given
        givenConsent()
        doReturn(Completable.complete()).`when`(paymentManager).signUp()

        // When
        viewModel.onPaymentSignUpRequested()
        triggerScheduler()

        // Then
        assertTrue(viewModel.errors.value.isNullOrEmpty())
        assertTrue(viewModel.isSignedUpForPayment.value!!)
    }

    @Test
    fun `Calling onPaymentSignUpRequested doesn't sign up without consent`() {
        // Given
        givenConsent(false)

        // When
        viewModel.onPaymentSignUpRequested()
        triggerScheduler()

        // Then
        assertTrue(viewModel.errors.value!!.isNotEmpty())
        assertFalse(viewModel.isSignedUpForPayment.value!!)
    }

    @Test
    fun `Calling onPaymentSignUpRequested with error`() {
        // Given
        givenConsent()
        doReturn(Completable.error(Exception())).`when`(paymentManager).signUp()

        // When
        viewModel.onPaymentSignUpRequested()
        triggerScheduler()

        // Then
        assertTrue(viewModel.errors.value!!.isNotEmpty())
    }

    private fun givenConsent(isGiven: Boolean = true) {
        val consent = mock<Consent> { whenever(it.approved).then { isGiven } }
        if (isGiven) {
            doReturn(Single.just(consent)).`when`(consentManager).getConsent(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_PAY)
            doReturn(Completable.complete()).`when`(consentManager).requestConsentIfRequiredAndAssertApproved(ConsentManager.ID_ACTIVATE_LUCA_PAY)
            doReturn(Completable.complete()).`when`(consentManager).assertConsentApproved(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_PAY)
            doReturn(Completable.complete()).`when`(consentManager).assertConsentApproved(ConsentManager.ID_ACTIVATE_LUCA_PAY)
        } else {
            Mockito.doReturn(Single.just(consent)).`when`(consentManager).getConsent(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_PAY)
            val exception = Mockito.mock(MissingConsentException::class.java)
            Mockito.doReturn(Completable.error(exception)).`when`(consentManager)
                .requestConsentBundleIfRequiredAndAssertApproved(*ConsentManager.lucaPayConsentBundle)
        }
    }
}
