package de.culture4life.luca.ui.payment.history

import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.testtools.samples.LucaPaySamples
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class PaymentHistoryDetailViewModelTest : LucaUnitTest() {

    private lateinit var viewModel: PaymentHistoryDetailViewModel

    @Before
    fun before() {
        val applicationSpy = spy(application)
        viewModel = spy(PaymentHistoryDetailViewModel(applicationSpy))
    }

    @Test
    fun `Calling processArguments to check that payment data is read`() {
        // Given
        val samplePayment = LucaPaySamples.ErikaMustermann().payments[0]
        val bundle = PaymentHistoryDetailFragment.createArguments(samplePayment)

        // When
        val observer = viewModel.processArguments(bundle).test()
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        observer.await().assertNoErrors()
        assertThat(viewModel.paymentData.value).isNotNull
        assertThat(viewModel.paymentData.value?.id).isEqualTo(samplePayment.id)
    }
}
