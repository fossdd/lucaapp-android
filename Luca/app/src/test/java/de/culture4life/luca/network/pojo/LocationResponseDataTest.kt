package de.culture4life.luca.network.pojo

import de.culture4life.luca.LucaUnitTest
import org.junit.Assert.assertEquals
import org.junit.Test

class LocationResponseDataTest : LucaUnitTest() {

    @Test
    fun getLocationDisplayName() {
        var data = LocationResponseData(locationId = "", groupName = "Group", areaName = "Area")
        assertEquals("Group - Area", data.locationDisplayName)

        data = LocationResponseData(locationId = "", groupName = "Group")
        assertEquals("Group", data.locationDisplayName)

        data = LocationResponseData(locationId = "", areaName = "Area")
        assertEquals("Area", data.locationDisplayName)

        data = LocationResponseData(locationId = "")
        assertEquals(null, data.locationDisplayName)
    }
}
