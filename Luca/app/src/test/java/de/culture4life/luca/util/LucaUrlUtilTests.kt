package de.culture4life.luca.util

import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.testtools.samples.UrlSamples
import junit.framework.Assert.assertEquals
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.Test

class LucaUrlUtilTests : LucaUnitTest() {

    @Test
    fun `isLucaUrl returns correct value for given Url string`() {
        val testData = mapOf(
            "luca://luca-app.de/webapp/something" to true,
            "luca://luca-app.de/webapp" to true,
            "${UrlSamples.webApp}something" to true,
            "http://luca-app.de/webapp" to true,
            "http://luca-app.de/webapp/something" to true,
            "http://luca-app.de/webapp" to true,
            "http://luc-app.de/webapp" to false,
            "http://luca-app.de" to false,
            "luca-app.de/webapp" to false,
        )

        testData.forEach {
            assertEquals("${it.value} is wrong result for ${it.key}", it.value, LucaUrlUtil.isLucaUrl(it.key))
        }
    }

    @Test
    fun `isAppointment returns correct value for given Url string`() {
        val testData = mapOf(
            CovidDocumentSamples.ErikaMustermann.TestAppointment().asDeepLink().toString() to true,
            UrlSamples.webApp to false,
            "https://www.google.de" to false
        )

        testData.forEach {
            assertEquals("${it.value} is wrong result for ${it.key}", it.value, LucaUrlUtil.isAppointment(it.key))
        }
    }

    @Test
    fun `isPrivateMeeting returns correct value for given Url string`() {
        val testData = mapOf(
            LocationSamples.Meeting().qrCodeContent to true,
            UrlSamples.webApp to false,
            "https://www.google.de" to false
        )

        testData.forEach {
            assertEquals("${it.value} is wrong result for ${it.key}", it.value, LucaUrlUtil.isPrivateMeeting(it.key))
        }
    }

    @Test
    fun `isTestResult returns correct value for given Url string`() {
        val testData = mapOf(
            CovidDocumentSamples.ErikaMustermann.EudccPcrPositive().asDeepLink().toString() to true,
            "${UrlSamples.webApp}testresult/eyJ0eXAi..." to false,
            LocationSamples.Meeting().qrCodeContent to false,
            UrlSamples.webApp to false,
            "https://www.google.com" to false,
            "https://www.google.com/webapp/testresult/#eyJ0eXAi..." to false,
            "" to false
        )

        testData.forEach {
            assertEquals("${it.value} is wrong result for ${it.key}", it.value, LucaUrlUtil.isTestResult(it.key))
        }
    }

    @Test
    fun `isSelfCheckIn returns correct value for given Url string`() {
        val restaurant = LocationSamples.Restaurant()
        val testData = mapOf(
            restaurant.qrCodeContent to true,
            restaurant.qrCodeContentWithLucaData to true,
            restaurant.qrCodeContentWithLucaDataAndCWA to true,
            UrlSamples.webApp to false,
            "https://app.luca-app.de/webapp/setup" to false,
            CovidDocumentSamples.ErikaMustermann.EudccPcrPositive().asDeepLink().toString() to false,
        )

        testData.forEach {
            assertEquals("${it.value} is wrong result for ${it.key}", it.value, LucaUrlUtil.isSelfCheckIn(it.key))
        }
    }

    @Test
    fun `getEncodedAdditionalDataFromVenueUrlIfAvailable returns empty value for valid url without additional data`() {
        val location = LocationSamples.Restaurant()
        LucaUrlUtil.getEncodedAdditionalDataFromVenueUrlIfAvailable(location.scannerId)
            .test()
            .assertNoValues()
            .assertComplete()
    }

    @Test
    fun `getEncodedAdditionalDataFromVenueUrlIfAvailable returns luca data for valid url with additional data`() {
        val location = LocationSamples.Restaurant()
        LucaUrlUtil.getEncodedAdditionalDataFromVenueUrlIfAvailable(location.qrCodeContentWithLucaData)
            .test()
            .assertValue(location.qrCodeLucaData)
    }

    @Test
    fun `getEncodedAdditionalDataFromVenueUrlIfAvailable returns luca data for valid url with additional data and CWA`() {
        val location = LocationSamples.Restaurant()
        LucaUrlUtil.getEncodedAdditionalDataFromVenueUrlIfAvailable(location.qrCodeContentWithLucaDataAndCWA)
            .test()
            .assertValue(location.qrCodeLucaData)
    }

    @Test
    fun `getEncodedAdditionalDataFromVenueUrlIfAvailable does not crash for url with empty CWA`() {
        val location = LocationSamples.Restaurant()
        LucaUrlUtil.getEncodedAdditionalDataFromVenueUrlIfAvailable(location.qrCodeContentWithEmptyCWA)
            .test()
            .await()
            .assertNoErrors()
    }

    @Test
    fun `isWebAppPaymentResult returns correct value for given URL string`() {
        val testData = mapOf(
            UrlSamples.checkoutResult("AnyId") to true,
            UrlSamples.checkoutResult("AnyId", PaymentData.Status.CLOSED) to true,
            UrlSamples.checkoutResult("AnyId", PaymentData.Status.UNKNOWN) to true,
            UrlSamples.checkoutResult("AnyId", PaymentData.Status.ERROR) to true,
            UrlSamples.payment to false,
            UrlSamples.webApp to false
        )

        testData.forEach {
            assertEquals("${it.value} is wrong result for ${it.key}", it.value, LucaUrlUtil.isWebAppPaymentResult(it.key))
        }
    }

    @Test
    fun `getCheckoutId returns checkoutId`() {
        val expectedCheckoutId = "AnyId"
        val url = UrlSamples.checkoutResult(expectedCheckoutId)
        val parsedCheckoutId = LucaUrlUtil.getCheckoutId(url)
        assertThat(parsedCheckoutId).isEqualTo(expectedCheckoutId)
    }

    @Test
    fun `getCheckoutId fails for non paymentResult URL`() {
        assertThatThrownBy { LucaUrlUtil.getCheckoutId(UrlSamples.webApp) }
            .isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Failed requirement.")
    }
}
