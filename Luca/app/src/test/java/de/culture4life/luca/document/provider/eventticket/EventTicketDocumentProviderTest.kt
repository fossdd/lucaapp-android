package de.culture4life.luca.document.provider.eventticket

import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.document.Document.Companion.TYPE_EVENT_TICKET
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.testtools.samples.TicketSampleEvents
import de.culture4life.luca.util.JwtUtil
import de.culture4life.luca.util.TimeUtil
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class EventTicketDocumentProviderTest : LucaUnitTest() {

    @get:Rule
    val fixedTimeRule = FixedTimeRule()

    private lateinit var eventTicketDocumentProvider: EventTicketDocumentProvider

    @Before
    fun before() {
        eventTicketDocumentProvider = EventTicketDocumentProvider(application)
    }

    @Test
    fun `Provider can only parse valid url`() {
        eventTicketDocumentProvider.canParse(TicketSampleEvents.Exhibition().qrCodeContent).test().await().assertValue(true)
        eventTicketDocumentProvider.canParse(LocationSamples.Meeting().qrCodeContent).test().await().assertValue(false)
        eventTicketDocumentProvider.canParse(CovidDocumentSamples.ErikaMustermann.EudccFullyVaccinated().asDeepLink().toString()).test().await()
            .assertValue(false)
    }

    @Test
    fun `Parsing extracts JWT from url and returns EventTicketDocument`() {
        // Given
        val event = TicketSampleEvents.Exhibition()
        val encodedJWT = event.jwt
        val expectedEventTicket = EventTicketDocument(
            "935183eb-3d52-4ef8-8ca1-c2ca184a3776",
            importTimestamp = TimeUtil.getCurrentMillis(),
            encodedData = encodedJWT,
            hashableEncodedData = JwtUtil.getHeaderAndBody(encodedJWT),
            isVerified = false,
            type = TYPE_EVENT_TICKET,
            issuer = "Connfair",
            locationName = "Hannover Congress Centrum (HCC)",
            eventName = "13. Deutscher Seniorentag 2021",
            category = EventTicketDocument.Category.EXHIBITION,
            startTimestamp = TimeUtil.getCurrentMillis() - 100000,
            endTimestamp = TimeUtil.getCurrentMillis() + 100000,
            creationTimestamp = TimeUtil.getCurrentMillis() - 200000,
            verificationId = "e320fdf121951c87",
            "Linus",
            "Weiss"
        )

        // When
        val testObserver = eventTicketDocumentProvider.parse(event.qrCodeContent).test()
        val verificationObserver = eventTicketDocumentProvider.verify(encodedJWT).test()

        // Then
        verificationObserver.await().assertNoErrors()
        val parsedDocument = testObserver.await().values().first()
        assertEquals(expectedEventTicket, parsedDocument.document)
    }
}
