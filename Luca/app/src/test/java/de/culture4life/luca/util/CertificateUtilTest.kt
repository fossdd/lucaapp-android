package de.culture4life.luca.util

import de.culture4life.luca.LucaUnitTest
import dgca.verifier.app.decoder.fromBase64
import org.junit.Assert.assertEquals
import org.junit.Test
import java.security.KeyFactory
import java.security.cert.CertPathValidatorException
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec

class CertificateUtilTest : LucaUnitTest() {

    @Test
    fun loadCertificate_validFile_loadsCertificate() {
        val certificate = CertificateUtil.loadCertificate("staging_root_ca.pem", application)
        assertEquals("CN=luca Dev Cluster Root CA, O=luca Dev, L=Berlin, ST=Berlin, C=DE", certificate.issuerDN.name)
    }

    @Test
    fun checkCertificateChain_validChain_completes() {
        val rootCertificate = CertificateUtil.loadCertificate("production_root_ca.pem", application)
        val intermediateCertificate = CertificateUtil.loadCertificate("production_intermediate_ca.pem", application)
        CertificateUtil.checkCertificateChain(rootCertificate, listOf(intermediateCertificate))
    }

    @Test(expected = CertPathValidatorException::class)
    fun checkCertificateChain_invalidChain_throws() {
        val rootCertificate = CertificateUtil.loadCertificate("production_root_ca.pem", application)
        val intermediateCertificate = CertificateUtil.loadCertificate("staging_intermediate_ca.pem", application)
        CertificateUtil.checkCertificateChain(rootCertificate, listOf(intermediateCertificate))
    }

    @Test
    fun checkCertificateChain_revokedRootCertificate_throws() {
        // TODO: 01.07.21 implement
    }

    @Test
    fun `Loading a public key PEM file is successful`() {
        // Given
        val validPublicKeyFile = "test_public_key.pem"
        val expectedKeyEncoded = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvchF99E49oeAIY2Y2TnH\n" +
            "7MBed7HqaFGvkjDEbxNbR7KizPjXoTgkZn1LTsWa0bxRTrobGN/g9swD+cUxPtvq\n" +
            "YyE0YD+CfoP/IgU2rp5dhRNtupv0ntZ0kgEXVp4oj/SiY59dKSOx6Eh+vdb7Ia+6\n" +
            "lBZF5HhO+wJrP4ip+EKBbEOcdX2sA5V7Zzpau8jLO4J22FF69gGfz27GxNRk1E8z\n" +
            "ra00DXXUP8mVF3YfuncynZAbHSFE7kM5feL2KMWSpF5r8pKdEyICI4a99+t39Xzb\n" +
            "pDPxFuf3NY42i2SY/qouIROMqzZYF+rKffNC0MDhVXqcLXrRMUyJWSPic+Bm4fUJ\n" +
            "9wIDAQAB"
        val expectedKey = KeyFactory.getInstance("RSA").generatePublic(X509EncodedKeySpec(expectedKeyEncoded.fromBase64()))

        // When
        val publicKey = CertificateUtil.loadPublicPEMKey(validPublicKeyFile, application)

        // Then
        assertEquals(expectedKey, publicKey)
    }

    @Test
    fun `Loading a private key PKCS8 file is successful`() {
        // Given
        val validPrivateKeyFile = "test_private_key.pem"
        val expectedKeyEncoded = "MIIEowIBAAKCAQEAvchF99E49oeAIY2Y2TnH7MBed7HqaFGvkjDEbxNbR7KizPjX\n" +
            "oTgkZn1LTsWa0bxRTrobGN/g9swD+cUxPtvqYyE0YD+CfoP/IgU2rp5dhRNtupv0\n" +
            "ntZ0kgEXVp4oj/SiY59dKSOx6Eh+vdb7Ia+6lBZF5HhO+wJrP4ip+EKBbEOcdX2s\n" +
            "A5V7Zzpau8jLO4J22FF69gGfz27GxNRk1E8zra00DXXUP8mVF3YfuncynZAbHSFE\n" +
            "7kM5feL2KMWSpF5r8pKdEyICI4a99+t39XzbpDPxFuf3NY42i2SY/qouIROMqzZY\n" +
            "F+rKffNC0MDhVXqcLXrRMUyJWSPic+Bm4fUJ9wIDAQABAoIBAHYnk/gf2FkUL1+0\n" +
            "am8DnEb31Vir+gaMpMRdfE6zPGc8/kKiwozrCS6N3y7hs+vUVMKw92HbMJI2nlsI\n" +
            "vAoonJqu947cu3/M6jDiEhNRIWRSnkRKCFhDqDumVkNlIFfPtLJVa4jLYl39lHrY\n" +
            "t21XnM/JtiXyCSoZbXAWw8t+jFk8Ao9MaPmsGMN6n/9h8jPV+6eETmymXsMk+QF3\n" +
            "O9evnDLh8xRWqDyYpJ38rv7aDhMFQyTXZcQ9RLFTf/4rnUktImDlZUmn6VZ/aOvf\n" +
            "+7oBnbw5BWZiw7JRGBqouHXG5VdLkcUFmTXRQbzeZQfvcF9U0R94E3djCBsgddjz\n" +
            "ZXiHVqECgYEA9y2wOgU9XvsYrjO7QGL/ZpmolqJw9FQOfMZa5lYR0m7l29/eoQpB\n" +
            "xDsuk26SHFo4bqqPw6wvHpH8EGAUQls88+N+V9CrMfIwmCG5FuNAP7/2ba0w1zzF\n" +
            "BReO29NZ0F6yNddLh8AXg8RFFV2eEeRoenUco0To7wdAUlJWT6EYXw8CgYEAxI4x\n" +
            "Z3cD9WyzNrB4vvki+8gNbhoWo+sVbJ/4lrStnQ91LX/IyagXiaTyBkUWSJAXudYG\n" +
            "DPlt6ILFK/ggyNVOa499CTixpYAfjad1+t1TAmzzTe90h613JikUMqgjDzrtUo7B\n" +
            "IGfSU0xLjDaWRCl/89dntdaqJzMngaqZC784JpkCgYEA9Q8QC8YMHXHW+0cAnApK\n" +
            "ap9nPMkaMbJwlpZVos2sZ7RZIc/nOkQgBfwi5jqWk5ODJXzmgbnhEW7suBN5onww\n" +
            "hyCBr3CLej5CnWG5zo5JsaqRGBXAOXTfZHgBw8Hb5rEsACNOl+0DozjgwRkK0XRh\n" +
            "fR9E4D4+rlTDGZrWRaZSslMCgYAylKjwCDeTk7p2q2kXNAOlqXCTUdFbs2ZbAru1\n" +
            "BJ/c2Bp+b/DavA/6kuDZbjPyzmncn9AEzLdU6zoUO5EsPYAlu9Igal+EyHjWOi7p\n" +
            "NMVkzWa7OpDRTXlo749MDeqfw7lI0oWucik02GBkn7OBrPBJbrg4mhpFZiuXebBS\n" +
            "zo2DmQKBgCBZpLdmE5lNLC6QPPmC570yk0oEY+K69R2+h4x0yP7XfcLGOMufXFvQ\n" +
            "Lzt4BYW8L/6H05m3EisFrw1ZnRKLa/X/qP6a4+f8WhlPw1nvWHQBj9YzT8GrUl1i\n" +
            "/RWguDfNMYwxKzGXdCFD3MIj9zdq6CL4dJU2es5hS1fJQZ1FwOG1"
        val expectedKey = KeyFactory.getInstance("RSA").generatePrivate(PKCS8EncodedKeySpec(expectedKeyEncoded.fromBase64()))

        // When
        val privateKey = CertificateUtil.loadPrivatePEMKey(validPrivateKeyFile, application)

        // Then
        assertEquals(expectedKey, privateKey)
    }
}
