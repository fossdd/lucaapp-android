package de.culture4life.luca.ui.payment.history

import de.culture4life.luca.LucaApplication
import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.testtools.samples.LocationSamples
import io.reactivex.rxjava3.core.Single
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.whenever

class PaymentHistoryViewModelTests : LucaUnitTest() {

    private lateinit var viewModel: PaymentHistoryViewModel

    @Mock
    private lateinit var paymentManager: PaymentManager
    private lateinit var applicationSpy: LucaApplication

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        applicationSpy = spy(application)
        doReturn(paymentManager).whenever(applicationSpy).paymentManager
        viewModel = PaymentHistoryViewModel(applicationSpy)
    }

    @After
    fun after() {
        viewModel.dispose()
    }

    @Test
    fun `Starting payment from scanner in location that does not support payment results in error`() {
        // Given
        val expectedPaymentLocationData = LocationSamples.RestaurantWithoutLucaPay().locationData
        doReturn(Single.just(expectedPaymentLocationData)).`when`(paymentManager).parsePaymentUrl(any())

        // When
        viewModel.invokeProcessQrCodeScan("")
        triggerScheduler()

        // Then
        assertThat(viewModel.errors.value, `is`(not(empty())))
    }

    @Test
    fun `Starting payment from scanner in location that supports payment results in emitted payment data`() {
        // Given
        val expectedPaymentLocationData = LocationSamples.RestaurantWithTable().locationData
        doReturn(Single.just(expectedPaymentLocationData)).`when`(paymentManager).parsePaymentUrl(any())

        // When
        viewModel.invokeProcessQrCodeScan("")
        triggerScheduler()

        // Then
        assertEquals(expectedPaymentLocationData, viewModel.paymentLocationData.value!!.value)
    }
}
