package de.culture4life.luca.document.provider.appointment;

import static junit.framework.TestCase.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import de.culture4life.luca.LucaUnitTest;
import de.culture4life.luca.document.Document;
import de.culture4life.luca.testtools.samples.CovidDocumentSamples;

public class AppointmentProviderTest extends LucaUnitTest {

    private AppointmentProvider appointmentProvider;

    @Before
    public void setUp() {
        appointmentProvider = new AppointmentProvider();
    }

    @Test
    public void canParse_validAppointment_emitsTrue() {
        String testAppointmentUrl = new CovidDocumentSamples.ErikaMustermann.TestAppointment().asDeepLink().toString();
        appointmentProvider.canParse(testAppointmentUrl)
                .test()
                .assertValue(true);
    }

    @Test
    public void canParse_validAppointment2_emitsTrue() {
        String testAppointmentBUrl = new CovidDocumentSamples.ErikaMustermann.TestAppointmentB().asDeepLink().toString();
        appointmentProvider.canParse(testAppointmentBUrl)
                .test()
                .assertValue(true);
    }

    @Test
    public void canParse_invalidData_emitsFalse() {
        appointmentProvider.canParse(new CovidDocumentSamples.ErikaMustermann.UnsupportedAppointment().asDeepLink().toString())
                .test()
                .assertValue(false);
    }

    @Test
    public void parse_validAppointment_parsesData() {
        CovidDocumentSamples.ErikaMustermann.TestAppointment testAppointment = new CovidDocumentSamples.ErikaMustermann.TestAppointment();
        appointmentProvider.parse(testAppointment.asDeepLink().toString())
                .test()
                .assertValue(appointment -> {
                    assertEquals(testAppointment.getType(), appointment.type);
                    assertEquals(testAppointment.getLab(), appointment.lab);
                    assertEquals(testAppointment.getAddress(), appointment.address);
                    assertEquals(String.valueOf(testAppointment.getTestingDateTime().getMillis()), appointment.timestamp);
                    assertEquals(testAppointment.getQrCodeContent(), appointment.qrCode);
                    assertEquals(Document.TYPE_APPOINTMENT, appointment.getDocument().getType());
                    assertEquals(testAppointment.getTestingDateTime().getMillis(), appointment.getDocument().getTestingTimestamp());
                    assertEquals(testAppointment.getTestingDateTime().getMillis() + TimeUnit.HOURS.toMillis(2), appointment.getDocument().getExpirationTimestamp());
                    return true;
                });
    }

    @Test
    public void parse_validAppointment2_parsesData() {
        CovidDocumentSamples.ErikaMustermann.TestAppointmentB testAppointmentB = new CovidDocumentSamples.ErikaMustermann.TestAppointmentB();
        appointmentProvider.parse(testAppointmentB.asDeepLink().toString())
                .test()
                .assertValue(appointment -> {
                    assertEquals(testAppointmentB.getType(), appointment.type);
                    assertEquals(testAppointmentB.getLab(), appointment.lab);
                    assertEquals(testAppointmentB.getAddress(), appointment.address);
                    assertEquals(String.valueOf(testAppointmentB.getTestingDateTime().getMillis()), appointment.timestamp);
                    assertEquals(testAppointmentB.getQrCodeContent(), appointment.qrCode);
                    assertEquals(Document.TYPE_APPOINTMENT, appointment.getDocument().getType());
                    return true;
                });
    }

}
